# Hinweise zur Adventuria-API
> Diese API bietet grundlegende Funktionen zur Entwicklung erweiterter Features für das Minecraft-Server Netzwerk *adventuria.eu* und darf nur in Verbindung mit diesem verwendet werden.

---
## Funktionen

### Plugin-Integration

#### AuthMe
Die Api Stellt eine Verbindung zum Plugin AuthMe her, um Logins zu überprüfen. Dies dient der Sicherheit, da sich AuthMe nur auf den aktuellen *Spigot-Server* bezieht und somit *BungeeCord* Befehle frei ausführbar wären.
Außerdem wird eine serverübergreifende Brücke erstellt, welche einen Spieler anhand der *IP-Adresse* und dem letzten *Einlogzeitpunkt* (24 Stunden) identifiziert und den Spieler ohne erneute Eingabe des Passworts einloggt.

> Alle Befehle außer */login* und */register* werden vor dem Login blockiert

> Bei einem Login von mehr als 24 Stunden am Stück muss sich der Spieler für die Benutzung von *BungeeCord-Befehlen* erneut einloggen. Dazu werden die Befehle */logout* und */login* benötigt

#### Vault
Die Api bietet eine Schnittstellt für *Vault*. Diese ermöglicht die Nutzung des *Berechtigungssystems*, **ohne** direkt auf das System bzw. die Api zuzugreifen und stellt somit eine Verbindung zur Api für externe Entwickler dar.

### Eigenständige Systeme

#### Standard-Chat
Die Api verändert das standardgemäße Chatdesign, sodass neben dem Namen des Spielers und dessen Nachricht auch ein im *Berechtigungssystem* eingestellter *Präfix* und *Suffix* angezeigt wird. Außerdem ist es möglich den Spielernamen mithilfe einer *Display*-Einstellung einzufärben.

> Das Chatdesign kann in der *api-messages.properties* verändert werden

#### Standard-Tabliste
Die Api erstellt mithilfe des *Präfixes*, sowie des *Suffixes* und des *Displays*, welche im *Berechtigungssystem* eingestellt werden können eine farbige Anzeige der Spieler in der Tabliste.

> Die Kopf- und Fußzeile der Tabliste wird **nicht** von der Api verändert, sondern von der *CloudNet-API* und ist in der *services.json* einstellbar

#### Berechtigungssystem
Die Api beinhaltet ein Berechtigungssystem, welches *serverweite*, *servergruppenbasierte* und *weltenbasierte* Berechtigungen mithilfe von *serverweiten* und *servergruppenbasierten* Gruppen oder *Spielerdaten* verteilen kann.

> Dieses System löst PermissionsEx ab, da dieses keinen Servergruppen-Support hat.

> Die Api ersetzt die Mechanik hinter den *.hasPermission([String])* Funktionen von *Spigot* und *BungeeCord*

#### Spielerlogging
Die Api erhebt diverse Daten über die Spieler des *Minecraft-Server* Netzwerks. Diese Daten werden in einer Datenbank gespeichert und zur weiteren Verarbeitung, über eine Schnittstelle der Api, zur Verfügung gestellt für Entwickler.

##### Erhobene Daten:
* Spielername / UUID
* IP-Adresse
* Rang- / Berechtigungsdaten
* Spielzeit auf dem Server
* Skindaten
* erste / letzte Verbindung zum Server

---
## Befehle
> Das folgende Abteil ist eine Darstellung aller Befehle mit ihren Berechtigungen und kurzer Beschreibung der Funktion

### Bungeecord
* /perm [perms, permission, permissions] (adventuria.api.command.perm) - *Befehl zur Bearbeitung des Rechtesystems*

### Spigot
* /performance [perf] (adventuria.api.command.performance) - Gibt Performance-Informationen des aktuellen Servers aus
* /log (adventuria.api.command.log) - Erstellt eine Kopie des Serverlogs auf *hastebin.com* und gibt den Link zu diesem im Chat aus

---
## Berechtigungen

### Bungeecord
* adventuria.api.command.perm - Ermöglicht die Nutzung von */perm*

### Spigot
* adventuria.api.command.performance - Ermöglicht die Nutzung von */perf*
* adventuria.api.command.log - Ermöglicht die Nutzung von /log

### Allgemein
* adventuria.api.general.team - Markiert eine Gruppe als dem Team angehörig
* adventuria.api.general.fulljoin - Ermöglicht das Betreten voller Server

---
## Funktionen für Entwickler
> Als Api des bietet das Plugin diverse Möglichkeiten für Entwickler Systeme zu entwickeln, vereinheitlichen und vereinfachen. Die Nutzung der Api ist insofern sinnvoll, dass alle Daten somit gebündelt und leicht zugänglich gespeichert werden, was die weitere Verarbeitung zu Darstellungs- oder Analysezwecken vereinfacht.

### Berechtigungssystem

Abfrage von Gruppen
```java
public PermissionGroup getPermissionGroup(String group) {
    return AdviAPI.getInstance().getPermissionPool().get(group);
}
```

Abfrage von Gruppenrechten
```java
// Abfrage für globale Berechtigungen
// funktioniert nicht für ServerGruppen basierte Gruppen (liefert immer false)
public boolean hasPermission(String group, String permission) {
    PermissionPool pool = AdviAPI.getInstance().getPermissionPool();
    
    PermissionGroup group;
    if((group = pool.get(group)) == null)
        return false;

    return group.hasPermission(permission);
}

// Abfrage für ServerGruppen basierte Berechtigungen
public boolean hasPermission(String group, String servergroup, String permission) {
    PermissionPool pool = AdviAPI.getInstance().getPermissionPool();
    
    PermissionGroup group;
    if((group = pool.get(group)) == null)
        return false;

    return group.hasPermission(permission, servergroup);
}


// Abfrage für Weltenbasierte Berechtigungen
public boolean hasPermission(String group, String permission, String world) {
    PermissionPool pool = AdviAPI.getInstance().getPermissionPool();
    
    PermissionGroup group;
    if((group = pool.get(group)) == null)
        return false;

    return group.hasPermission(permission, servergroup, world);
}
```

Abfrage des PermissionUsers
```java
public PermissionUser getPermissionUser(UUID uniqueId) {
    
    NetworkPlayer player;
    if((player = AdviAPI.getInstance().getPlayerProvider().get(uniqueId)) == null)
        return null;
    
    return player.getPermissionUser();
}

public PermissionUser getPermissionUser(String name) {
    
    NetworkPlayer player;
    if((player = AdviAPI.getInstance().getPlayerProvider().get(name)) == null)
        return null;
    
    return player.getPermissionUser();
}
```

Abfrage von Spielerberechtigungen (auch für Spieler, die nicht online sind)
```java
// Abfrage für globale Berechtigungen
public boolean hasPermission(UUID uniqueId, String permission) {

    NetworkPlayer player;
    if((player = AdviAPI.getInstance().getPlayerProvider().get(name)) == null)
        return null;
    
    return player.getPermissionUser().hasPermission(permission);
}

// Abfrage für ServerGruppen basierte Berechtigungen
public boolean hasPermission(UUID uniqueId, String permission, String servergroup) {

    NetworkPlayer player;
    if((player = AdviAPI.getInstance().getPlayerProvider().get(name)) == null)
        return null;
    
    return player.getPermissionUser().hasPermission(permission, servergroup);
}

// Abfrage für Weltenbasierte Berechtigungen
public boolean hasPermission(UUID uniqueId, String permission, String servergroup, String world) {

    NetworkPlayer player;
    if((player = AdviAPI.getInstance().getPlayerProvider().get(name)) == null)
        return null;
    
    return player.getPermissionUser().hasPermission(permission, servergroup, world);
}
```

### Spielerinformationen

Abfrage allgemeiner Spielerinformationen
```java
public NetworkPlayer getPlayer(UUID uniqueId) {
    return AdviAPI.getInstance().getPlayerProvider().get(uniqueId);
}

public NetworkPlayer getPlayer(String name) {
    return AdviAPI.getInstance().getPlayerProvider().get(name);
}
```