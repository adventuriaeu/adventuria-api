package de.fabtopf.contray.adventuria.api.internal.bukkit.listener;

import de.dytanic.cloudnet.api.CloudAPI;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import fr.xephi.authme.events.LoginEvent;
import fr.xephi.authme.events.LogoutEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.HashMap;
import java.util.Map;

public class AuthMeListener implements Listener {

    @EventHandler
    public void onLogin(LoginEvent event) {
        Player player = event.getPlayer();

        Map<String, Object> authInfo = new HashMap<>();
        authInfo.put("address", CloudAPI.getInstance().getOnlinePlayer(player.getUniqueId()).getPlayerConnection().getHost());
        authInfo.put("timestamp", System.currentTimeMillis());

        AdviAPI.getInstance().saveRedisRequest(redis ->
                redis.set("adviapi-authmebridge-" + player.getUniqueId().toString(),
                        BukkitCore.getInstance().getGson().toJson(authInfo)), null);
    }

    @EventHandler
    public void onLogout(LogoutEvent event) {
        Player player = event.getPlayer();

        AdviAPI.getInstance().saveRedisRequest(redis -> redis.del("adviapi-authmebridge-" + player.getUniqueId().toString()), false);
    }

}
