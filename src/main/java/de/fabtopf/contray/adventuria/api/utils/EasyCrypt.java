package de.fabtopf.contray.adventuria.api.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

@Getter
@Setter
public class EasyCrypt {

    private Key key;
    private String processor;

    public EasyCrypt(Key key, String processor) throws Exception {
        this.key = key;
        this.processor = processor;
    }

    public OutputStream encryptOutputStream(OutputStream os) throws Exception {
        valid();

        // encrypt message using RSA
        Cipher cipher = Cipher.getInstance(processor);
        cipher.init(Cipher.ENCRYPT_MODE, key);

        return new CipherOutputStream(os, cipher);
    }

    public InputStream decryptInputStream(InputStream is) throws Exception {
        valid();

        // decrypt message using AES
        Cipher cipher = Cipher.getInstance(processor);
        cipher.init(Cipher.DECRYPT_MODE, key);

        return new CipherInputStream(is, cipher);
    }

    public String encrypt(String text) throws Exception {
        valid();

        // encrypt
        Cipher cipher = Cipher.getInstance(processor);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encrypted = cipher.doFinal(text.getBytes());

        // convert bytes to base64-string
        BASE64Encoder encoder = new BASE64Encoder();

        return encoder.encode(encrypted);
    }

    public String decrypt(String secret) throws Exception {
        valid();

        // convert base64-string to bytes
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] crypted = decoder.decodeBuffer(secret);

        // decrypt
        Cipher cipher = Cipher.getInstance(processor);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] cipherData = cipher.doFinal(crypted);
        return new String(cipherData);
    }

    private boolean valid() throws Exception {
        if(processor == null){
            throw new NullPointerException("No processor set!");
        }

        if(key == null){
            throw new NullPointerException("No key set!");
        }

        if(processor.isEmpty()){
            throw new NullPointerException("No processor set!");
        }

        return true;
    }

    public static String encrypt(String str, EasyCrypt ec) throws Exception {
        return ec.encrypt(str);
    }

    public static String decrypt(String str, EasyCrypt ec) throws Exception {
        return ec.decrypt(str);
    }

    public static PrivateKey readPrivateKeyRSA(File path) throws Exception {

        FileInputStream fis = new FileInputStream(path);
        byte[] encodedPrivateKey = new byte[(int) path.length()];
        fis.read(encodedPrivateKey);
        fis.close();

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey);
        return keyFactory.generatePrivate(privateKeySpec);

    }

    public static PublicKey readPublicKeyRSA(File path) throws Exception {

        FileInputStream fis = new FileInputStream(path);
        byte[] encodedPublicKey = new byte[(int) path.length()];
        fis.read(encodedPublicKey);
        fis.close();

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedPublicKey);
        return keyFactory.generatePublic(publicKeySpec);

    }

    public static PrivateKey readPrivateKeyRSA(String path) throws Exception {
        return readPrivateKeyRSA(new File(path));
    }

    public static PublicKey readPublicKeyRSA(String path) throws Exception {
        return readPublicKeyRSA(new File(path));
    }

    private static String byteArrayToHexString(byte[] b) {
        StringBuilder result = new StringBuilder();
        for (int i=0; i < b.length; i++) result.append(Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 ));
        return result.toString();
    }

    public static String hash(String str, HashAlgorithm algorithm) {
        byte[] hash;

        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm.processor);
            hash = digest.digest(str.getBytes());
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }

        if(hash == null) return null;

        return byteArrayToHexString(hash);
    }

    @Getter
    @AllArgsConstructor
    public enum HashAlgorithm {
        MD5("MD5"), SHA_1("SHA-1"), SHA_256("SHA-256");

        private String processor;
    }

}
