package de.fabtopf.contray.adventuria.api.utils.bukkit.command.alias;

import de.dytanic.cloudnet.api.CloudAPI;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCallbackSubCommand;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommand;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleSubCommandExecutor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class CommandAlias extends SimpleCommand {

    CommandAlias(String name, String description, String usage, String permission, List<Map<String, Object>> commands) {

        super(name, "advi-alias", description, usage, new ArrayList<>(), permission, BukkitCore.getInstance().getLanguage().getPhraseContent("command-no-perm"), BukkitCore.getInstance(), new SimpleSubCommandExecutor() {

            @Override
            public String getUsage() {
                return "";
            }

            @Override
            public String getPermissionMessage() {
                return "";
            }

            @Override
            public String getDescription() {
                return "";
            }

        });

        for(Map<String, Object> command : commands)
            this.getSubCommandList().add(new SimpleCallbackSubCommand(
                    (String) command.getOrDefault("permission", ""),
                    (String[]) command.getOrDefault("requiredArgs", new String[0]),
                    (String[]) command.getOrDefault("helpMessageParams", new String[0]),
                    (boolean) command.getOrDefault("showInHelp", false),
                    (int) command.getOrDefault("display", Integer.MAX_VALUE),
                    (Class<? extends CommandSender>[]) command.getOrDefault("allowedSenders", new Class[0]),
                    this.getCommandAction((List<Map<String, String>>) command.getOrDefault("commands", new ArrayList<>())))
            );

    }

    SimpleCallbackSubCommand.Action getCommandAction(List<Map<String, String>> commands) {

        return new SimpleCallbackSubCommand.Action() {

            @Override
            public void call(CommandSender sender, SimpleCommand command, Object... args) {
                System.out.println(commands.stream().map(m -> m.get("command")).collect(Collectors.joining(", ")));
                for (Map<String, String> cmd : commands)
                    switch (cmd.get("sender").toLowerCase()) {

                        case "player":
                            ((Player) sender).performCommand(MessageFormat.format(replaceArgs((Player) sender, cmd.get("command")), args));
                            break;

                        case "op":
                            Player player = ((Player) sender);
                            NetworkPlayer np =  AdviAPI.getInstance().getPlayerProvider().get(player.getUniqueId());
                            String world = player.getLocation().getWorld().getName();

                            np.getPermissionUser().addPermission("*", CloudAPI.getInstance().getGroup(), world);
                            player.performCommand(MessageFormat.format(replaceArgs((Player) sender, cmd.get("command")), args));
                            np.getPermissionUser().removePermission("*", CloudAPI.getInstance().getGroup(), world);
                            break;

                        case "console":
                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), MessageFormat.format(cmd.get("command"), args));
                            break;

                    }

            }

            private String replaceArgs(Player player, String cmd) {
                Map<String, String> replacements = new HashMap<>();

                replacements.put("%sender_name%", player.getName());
                replacements.put("%sender_uuid%", player.getUniqueId().toString());
                replacements.put("%world%", player.getLocation().getWorld().getName());
                replacements.put("%pos_x%", Double.toString(player.getLocation().getX()));
                replacements.put("%pos_y%", Double.toString(player.getLocation().getY()));
                replacements.put("%pos_z%", Double.toString(player.getLocation().getZ()));
                replacements.put("%block_x%", Double.toString(player.getLocation().getBlockX()));
                replacements.put("%block_y%", Double.toString(player.getLocation().getBlockY()));
                replacements.put("%block_z%", Double.toString(player.getLocation().getBlockZ()));

                for(Map.Entry<String, String> entry : replacements.entrySet())
                    cmd = cmd.replace(entry.getKey(), entry.getValue());

                return cmd;
            }

        };

    }

}
