package de.fabtopf.contray.adventuria.api.internal.bungee.command;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

public abstract class AdviCommand extends Command implements TabExecutor {

    public AdviCommand(String name)
    {
        super(name);
    }

    public AdviCommand(String name, String permission)
    {
        super(name, permission);
    }

    public AdviCommand(String name, String permission, String... alias)
    {
        super(name, permission, alias);
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings)
    {
        return Collections.EMPTY_LIST;
    }

    protected void simpleTabWithFilter(List<String> destination, Stream<String> source, String filter)
    {
        this.simpleStringTabWithFilter(destination, source, filter != null ? filter.toLowerCase() : null);
    }

    private void simpleStringTabWithFilter(Collection<String> destination, Stream<String> source, final String filter)
    {
        source.forEach(new Consumer<String>() {
            @Override
            public void accept(String temp) {
                if (filter == null || temp.toLowerCase().contains(filter))
                    destination.add(temp);
            }
        });
    }

}
