package de.fabtopf.contray.adventuria.api.utils;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.permission.PermissionGroup;
import de.fabtopf.contray.adventuria.api.permission.PermissionUser;
import net.minecraft.server.v1_13_R2.*;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.*;
import java.util.stream.Collectors;

public class PlayerUtilities {

    /*
     *  Title
     */

    public static void sendTitle(String title, String subtitle) {
        PlayerUtilities.sendPacket(createTitlePacket(title, PacketPlayOutTitle.EnumTitleAction.TITLE));
        PlayerUtilities.sendPacket(createTitlePacket(subtitle, PacketPlayOutTitle.EnumTitleAction.SUBTITLE));
    }

    public static void sendTitle(Player player, String title, String subtitle) {
        PlayerUtilities.sendPacket(player, createTitlePacket(title, PacketPlayOutTitle.EnumTitleAction.TITLE));
        PlayerUtilities.sendPacket(player, createTitlePacket(subtitle, PacketPlayOutTitle.EnumTitleAction.SUBTITLE));
    }

    public static void sendTitle(String title, String subtitle, int stay) {
        PlayerUtilities.sendTitle(title, subtitle, stay);
    }

    public static void sendTitle(Player player, String title, String subtitle, int stay) {
        PlayerUtilities.sendTitle(player, title, subtitle, stay);
    }

    public static void sendTitle(String title, String subtitle, int stay, int fadeIn, int fadeOut) {
        PlayerUtilities.sendPacket(createTitlePacket(title, PacketPlayOutTitle.EnumTitleAction.TITLE));
        PlayerUtilities.sendPacket(createTitlePacket(subtitle, PacketPlayOutTitle.EnumTitleAction.SUBTITLE));
        PlayerUtilities.sendPacket(createTitleDurationPacket(stay, fadeIn, fadeOut));
    }

    public static void sendTitle(Player player, String title, String subtitle, int stay, int fadeIn, int fadeOut) {
        PlayerUtilities.sendPacket(player, createTitlePacket(title, PacketPlayOutTitle.EnumTitleAction.TITLE));
        PlayerUtilities.sendPacket(player, createTitlePacket(subtitle, PacketPlayOutTitle.EnumTitleAction.SUBTITLE));
        PlayerUtilities.sendPacket(player, createTitleDurationPacket(stay, fadeIn, fadeOut));
    }

    private static Packet createTitleDurationPacket(int stay, int fadeIn, int fadeOut) {
        return new PacketPlayOutTitle(fadeIn, stay, fadeOut);
    }

    private static Packet createTitlePacket(String text, PacketPlayOutTitle.EnumTitleAction type) {
        if (text == null)
            text = "";

        return new PacketPlayOutTitle(type, new ChatComponentText(text));
    }

    /*
     * ActionBar
     */

    public static void sendActionbar(String text) {
        PlayerUtilities.sendPacket(PlayerUtilities.createActionBarPacket(text));
    }

    public static void sendActionbar(Player player, String text) {
        PlayerUtilities.sendPacket(player, PlayerUtilities.createActionBarPacket(text));
    }

    private static Packet createActionBarPacket(String text) {
        return new PacketPlayOutChat(new ChatComponentText(text), ChatMessageType.GAME_INFO);
    }

    /*
     * NameTags
     */

    public static void updateNametags() {
        PlayerUtilities.updateNametags(null);
    }

    public static void updateNametags(Player target) {
        PlayerUtilities.$updateNametags(target, Bukkit.getOnlinePlayers().stream()
                .collect(Collectors.toMap(p -> p, p -> AdviAPI.getInstance().getPlayerProvider().get(p.getUniqueId()).getPermissionUser())));
    }

    private static void $updateNametags(Player target, Map<Player, PermissionUser> userMap) {

        Set<Player> toChange = Collections.singleton(target);
        if(target == null)
            toChange = userMap.keySet();

        Map<Player, PermissionGroup> groupMap = new HashMap<>();
        for(Map.Entry<Player, PermissionUser> entry : userMap.entrySet())
            groupMap.put(entry.getKey(), entry.getValue().getCurrentGroups(AdviAPI.getInstance().getCloudUtilities().getGroup()).get(0));

        for(Player player : toChange) {
            PermissionUser user = userMap.get(player);

            Map<PermissionGroup, Team> teamMap = new HashMap<>();
            for(Player online : userMap.keySet()) {
                PermissionGroup group = groupMap.get(online);
                Scoreboard scoreboard = player.getScoreboard();

                String teamName = StringUtils.leftPad(group.getTagId().toString(), 6, "0") + "-" + group.getName();

                Team team;
                if((team = teamMap.get(group)) == null) {
                    if ((team = scoreboard.getTeam(teamName)) == null)
                        team = scoreboard.registerNewTeam(teamName);

                    team.setPrefix(group.getPrefix() + group.getDisplay());
                    team.setSuffix(group.getSuffix());

                    Optional<ChatColor> chatColor;
                    String display = group.getDisplay();
                    if(display != null && !ChatColor.stripColor(display).equals(display))
                        if((chatColor = Arrays.stream(ChatColor.values()).filter(c -> c.getChar() == ChatColor.getLastColors(display).charAt(1)).findFirst()).isPresent())
                            team.setColor(chatColor.get());

                    teamMap.put(group, team);
                }

                team.addEntry(online.getName());
            }

            player.setPlayerListName(ChatColor.translateAlternateColorCodes('&',
                    user.getPrefix(AdviAPI.getInstance().getCloudUtilities().getGroup())) + user.getDisplay(AdviAPI.getInstance().getCloudUtilities().getGroup()) + player.getName() + user.getSuffix(AdviAPI.getInstance().getCloudUtilities().getGroup()));
        }

    }

    /*
     * RankDisplays
     */

    public static void setRankCustomNames() {
        for (Player player : Bukkit.getOnlinePlayers())
            PlayerUtilities.setRankCustomName(player);
    }

    public static void setRankCustomName(Player player) {
        player.setCustomName(AdviAPI.getInstance().getPlayerProvider().get(player.getUniqueId()).getNameFormatted());
    }

    /*
     *  Utils
     */

    private static void sendPacket(Player player, Packet packet) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    private static void sendPacket(Packet packet) {
        for(Player player : Bukkit.getOnlinePlayers())
            sendPacket(player, packet);
    }

}
