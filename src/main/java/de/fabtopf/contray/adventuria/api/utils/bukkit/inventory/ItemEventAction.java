package de.fabtopf.contray.adventuria.api.utils.bukkit.inventory;

import org.bukkit.event.Event;

public interface ItemEventAction<E extends Event> {

    void call(E event, SimpleInventory inventory, SimpleItem item);

}
