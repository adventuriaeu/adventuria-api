package de.fabtopf.contray.adventuria.api.internal.bukkit.command;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommand;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommandHandler;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleFunctionCommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@SimpleCommandHandler(name = "skin")
public class SkinCommand implements SimpleFunctionCommandExecutor {

    @Override
    public void sendHelpMessage(CommandSender executor, SimpleCommand command) {

    }

    @Override
    public String getUsage() {
        return "";
    }

    @Override
    public String getPermissionMessage() {
        return "";
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public void onExecute(CommandSender sender, SimpleCommand command, String label, String[] args) {
        if(!(sender instanceof Player))
            return;

        Player player = (Player) sender;
        NetworkPlayer np = AdviAPI.getInstance().getPlayerProvider().get(player.getUniqueId());

        np.updateSkin(args[0]);
        sender.sendMessage("updated to " + args[0]);
    }
}
