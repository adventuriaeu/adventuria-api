package de.fabtopf.contray.adventuria.api.utils.bukkit.scoreboard;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

@Getter
@Setter
@AllArgsConstructor
public class StaticScoreboardValue implements ScoreboardValue {

    private String name;
    private String defaultValue;

    @Override
    public String getValue(Player player) {
        return defaultValue;
    }

}
