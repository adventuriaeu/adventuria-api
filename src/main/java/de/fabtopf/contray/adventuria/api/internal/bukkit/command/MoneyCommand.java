package de.fabtopf.contray.adventuria.api.internal.bukkit.command;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommandHandler;
import de.fabtopf.contray.adventuria.api.utils.economy.Currency;
import de.fabtopf.contray.adventuria.api.utils.economy.EconomyProvider;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.utils.Language;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommand;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleFunctionCommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.util.UUID;

@SimpleCommandHandler(name = "money", prefix = "advi-economy", permission = "adventuria.api.command.money")
public class MoneyCommand implements SimpleFunctionCommandExecutor {

    @Override
    public void sendHelpMessage(CommandSender executor, SimpleCommand command) {
        executor.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-money-help"));
    }

    @Override
    public String getUsage() {
        return "Nutze /money help für weitere Informationen";
    }

    @Override
    public String getPermissionMessage() {
        return BukkitCore.getInstance().getLanguage().getPhraseContent("command-no-perm");
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public void onExecute(CommandSender sender, SimpleCommand command, String executedAlias, String[] args) {
        Language lang = BukkitCore.getInstance().getLanguage();

        if(AdviAPI.getInstance().bukkit().getEconomyProvider().getEnabledCurrencies().size() > 0) {

            if (args.length == 0) {

                sender.sendMessage(lang.getPhraseContent("command-money-help"));

                return;
            } else {

                UUID uniqueId = null;
                Currency currency = null;
                NetworkPlayer networkPlayer = null;
                switch (args[0].toLowerCase()) {

                    /*
                     *  usage:      /money {bal | balance} [Player] [Currency]
                     *  permission: adventuria.api.command.money.balance.[Currency]
                     *              adventuria.api.command.money.balance (default currency)
                     *
                     *  sub-permissions:
                     *    - adventuria.api.command.money.balance.others.[Currency]
                     *    - adventuria.api.command.money.balance.others (default currency)
                     */

                    case "bal":
                    case "balance":

                        if(!(sender instanceof Player)) {
                            sender.sendMessage(lang.getPhraseContent("command-player-only"));
                            return;
                        }

                        if(!sender.hasPermission("adventuria.api.command.money.balance")) {

                            sender.sendMessage(command.getPermissionMessage());

                            return;
                        }

                        if(args.length > 1) {

                            if(args.length == 2) {

                                if((networkPlayer = AdviAPI.getInstance().getPlayerProvider().get(args[1])) == null) {

                                    if((currency = EconomyProvider.getCurrencyProvider().get(args[1])) == null) {

                                        sender.sendMessage(lang.getPhraseContent("command-money-no-currency-or-player", args[1]));

                                        return;
                                    }

                                } else {

                                    if(!sender.hasPermission("adventuria.api.command.money.balance.others")) {

                                        sender.sendMessage(command.getPermissionMessage());

                                        return;
                                    }

                                    if((uniqueId = AdviAPI.getInstance().bukkit().getEconomyProvider().getMainCurrency()) == null ||
                                            (currency = EconomyProvider.getCurrencyProvider().get(uniqueId)) == null) {

                                        sender.sendMessage(lang.getPhraseContent("command-money-no-default-currency"));

                                        return;
                                    }

                                }

                            } else {

                                if(!sender.hasPermission("adventuria.api.command.money.balance.others")) {

                                    sender.sendMessage(command.getPermissionMessage());

                                    return;
                                }

                                if((networkPlayer = AdviAPI.getInstance().getPlayerProvider().get(args[1])) == null) {

                                    sender.sendMessage(lang.getPhraseContent("player-not-registered", args[1]));

                                    return;
                                }

                                if((currency = EconomyProvider.getCurrencyProvider().get(args[2])) == null) {

                                    sender.sendMessage(lang.getPhraseContent("command-money-no-currency", args[2]));

                                    return;
                                }

                            }

                        } else if((uniqueId = AdviAPI.getInstance().bukkit().getEconomyProvider().getMainCurrency()) == null ||
                                (currency = EconomyProvider.getCurrencyProvider().get(uniqueId)) == null) {
                            sender.sendMessage(lang.getPhraseContent("command-money-no-default-currency"));
                            return;
                        }

                        if(networkPlayer == null)
                            networkPlayer = AdviAPI.getInstance().getPlayerProvider().get(((Player) sender).getUniqueId());

                        sender.sendMessage(lang.getPhraseContent("command-money-balance", networkPlayer.getName(),
                                new DecimalFormat("#,##0.00").format(((Long) networkPlayer.balanceMoney(uniqueId)).doubleValue() / 100), currency.getSign()));

                        return;

                    /*
                     *  usage:      /money set <Player> <Amount> [Currency]
                     *  permission: adventuria.api.command.money.set.[Currency]
                     *              adventuria.api.command.money.set (default currency)
                     */

                    case "set":

                        if(!sender.hasPermission("adventuria.api.command.money.set")) {

                            sender.sendMessage(command.getPermissionMessage());

                            return;
                        }

                        if(args.length > 2) {

                            if((networkPlayer = AdviAPI.getInstance().getPlayerProvider().get(args[1])) == null) {

                                sender.sendMessage(lang.getPhraseContent("player-not-registered", args[1]));

                                return;
                            }

                            long money;
                            try {

                                money = ((Double) (Double.parseDouble(args[2]) * 100)).longValue();

                            } catch (Exception exception) {
                                sender.sendMessage(lang.getPhraseContent("system-command-invalid-datatype", 3, "Dezimalzahl"));
                                return;
                            }

                            if(args.length == 3) {

                                UUID currencyUUID;
                                if((currencyUUID = AdviAPI.getInstance().getEconomyProvider(AdviAPI.getInstance().getServerName()).getMainCurrency()) == null) {

                                    sender.sendMessage(lang.getPhraseContent("command-money-no-currency", args[2]));

                                    return;
                                }


                                if((currency = EconomyProvider.getCurrencyProvider().get(currencyUUID)) == null) {

                                    sender.sendMessage(lang.getPhraseContent("command-money-no-currency", args[2]));

                                    return;
                                }

                            } else if(args.length == 4) {

                                return;

                            } else {

                                sendHelpMessage(sender, command);

                                return;
                            }

                            sender.sendMessage(lang.getPhraseContent("command-money-set", networkPlayer.getName(),
                                    new DecimalFormat("#,##0.00").format(((Long) networkPlayer.setMoney(currency.getKey(), money)).doubleValue() / 100), currency.getSign()));

                        } else {

                            sendHelpMessage(sender, command);

                            return;
                        }

                        return;

                    /*
                     *  usage:      /money {add | give} <Player> <Amount> [Currency]
                     *  permission: adventuria.api.command.money.add.[Currency]
                     *              adventuria.api.command.money.add (default currency)
                     */

                    case "add":
                    case "give":

                        if(!sender.hasPermission("adventuria.api.command.money.add")) {

                            sender.sendMessage(command.getPermissionMessage());

                            return;
                        }

                        if(args.length > 2) {

                            if((networkPlayer = AdviAPI.getInstance().getPlayerProvider().get(args[1])) == null) {

                                sender.sendMessage(lang.getPhraseContent("player-not-registered", args[1]));

                                return;
                            }

                            long money;
                            try {

                                money = ((Double) (Double.parseDouble(args[2]) * 100)).longValue();

                            } catch (Exception exception) {
                                sender.sendMessage(lang.getPhraseContent("system-command-invalid-datatype", 3, "Dezimalzahl"));
                                return;
                            }

                            if(args.length == 3) {

                                UUID currencyUUID;
                                if((currencyUUID = AdviAPI.getInstance().getEconomyProvider(AdviAPI.getInstance().getServerName()).getMainCurrency()) == null) {

                                    sender.sendMessage(lang.getPhraseContent("command-money-no-currency", args[2]));

                                    return;
                                }


                                if((currency = EconomyProvider.getCurrencyProvider().get(currencyUUID)) == null) {

                                    sender.sendMessage(lang.getPhraseContent("command-money-no-currency", args[2]));

                                    return;
                                }

                            } else if(args.length == 4) {

                                return;

                            } else {

                                sendHelpMessage(sender, command);

                                return;
                            }

                            sender.sendMessage(lang.getPhraseContent("command-money-add", networkPlayer.getName(),
                                    new DecimalFormat("#,##0.00").format(((Long) networkPlayer.depositMoney(currency.getKey(), money)).doubleValue() / 100), currency.getSign()));

                        } else {

                            sendHelpMessage(sender, command);

                            return;
                        }

                        return;

                    /*
                     *  usage:      /money {remove | take} <Player> <Amount> [Currency]
                     *  permission: adventuria.api.command.money.remove.[Currency]
                     *              adventuria.api.command.money.remove (default currency)
                     */

                    case "remove":
                    case "take":

                        if(!sender.hasPermission("adventuria.api.command.money.remove")) {

                            sender.sendMessage(command.getPermissionMessage());

                            return;
                        }

                        if(args.length > 2) {

                            if((networkPlayer = AdviAPI.getInstance().getPlayerProvider().get(args[1])) == null) {

                                sender.sendMessage(lang.getPhraseContent("player-not-registered", args[1]));

                                return;
                            }

                            long money;
                            try {

                                money = ((Double) (Double.parseDouble(args[2]) * 100)).longValue();

                            } catch (Exception exception) {
                                sender.sendMessage(lang.getPhraseContent("system-command-invalid-datatype", 3, "Dezimalzahl"));
                                return;
                            }

                            if(args.length == 3) {

                                UUID currencyUUID;
                                if((currencyUUID = AdviAPI.getInstance().getEconomyProvider(AdviAPI.getInstance().getServerName()).getMainCurrency()) == null) {

                                    sender.sendMessage(lang.getPhraseContent("command-money-no-currency", args[2]));

                                    return;
                                }


                                if((currency = EconomyProvider.getCurrencyProvider().get(currencyUUID)) == null) {

                                    sender.sendMessage(lang.getPhraseContent("command-money-no-currency", args[2]));

                                    return;
                                }

                            } else if(args.length == 4) {

                                return;

                            } else {

                                sendHelpMessage(sender, command);

                                return;
                            }

                            sender.sendMessage(lang.getPhraseContent("command-money-remove", networkPlayer.getName(),
                                    new DecimalFormat("#,##0.00").format(((Long) networkPlayer.withdrawMoney(currency.getKey(), money)).doubleValue() / 100), currency.getSign()));

                        } else {

                            sendHelpMessage(sender, command);

                            return;
                        }

                        return;

                    /*
                     *  usage:      /money pay <Player> <Amount> [Currency]
                     *  permission: adventuria.api.command.money.pay.[Currency]
                     *              adventuria.api.command.money.pay (default currency)
                     */

                    case "pay":

                        if(!(sender instanceof Player)) {
                            sender.sendMessage(lang.getPhraseContent("command-player-only"));
                            return;
                        }

                        if(!sender.hasPermission("adventuria.api.command.money.pay")) {

                            sender.sendMessage(command.getPermissionMessage());

                            return;
                        }

                        if(args.length > 2) {

                            if((networkPlayer = AdviAPI.getInstance().getPlayerProvider().get(args[1])) == null) {

                                sender.sendMessage(lang.getPhraseContent("player-not-registered", args[1]));

                                return;
                            }

                            long money;
                            try {

                                money = ((Double) (Double.parseDouble(args[2]) * 100)).longValue();

                            } catch (Exception exception) {
                                sender.sendMessage(lang.getPhraseContent("system-command-invalid-datatype", 3, "Dezimalzahl"));
                                return;
                            }

                            if(args.length == 3) {

                                UUID currencyUUID;
                                if((currencyUUID = AdviAPI.getInstance().getEconomyProvider(AdviAPI.getInstance().getServerName()).getMainCurrency()) == null) {

                                    sender.sendMessage(lang.getPhraseContent("command-money-no-currency", args[2]));

                                    return;
                                }


                                if((currency = EconomyProvider.getCurrencyProvider().get(currencyUUID)) == null) {

                                    sender.sendMessage(lang.getPhraseContent("command-money-no-currency", args[2]));

                                    return;
                                }

                            } else if(args.length == 4) {

                                return;

                            } else {

                                sendHelpMessage(sender, command);

                                return;
                            }

                            NetworkPlayer invoker = AdviAPI.getInstance().getPlayerProvider().get(((Player) sender).getUniqueId());
                            if(money <= 0) {

                                sender.sendMessage(lang.getPhraseContent("command-money-amount-to-little"));

                                return;
                            }

                            if(invoker.balanceMoney(currency.getKey()) < money) {

                                sender.sendMessage(lang.getPhraseContent("command-money-not-enough-money"));

                                return;
                            }

                            networkPlayer.depositMoney(currency.getKey(), money);
                            invoker.withdrawMoney(currency.getKey(), money);

                            sender.sendMessage(lang.getPhraseContent("command-money-pay", networkPlayer.getName(),
                                    new DecimalFormat("#,##0.00").format(((Long) money).doubleValue() / 100), currency.getSign()));

                            if(networkPlayer.isOnline())
                                networkPlayer.sendMessage(lang.getPhraseContent("command-money-pay-receive", sender.getName(),
                                        new DecimalFormat("#,##0.00").format(((Long) money).doubleValue() / 100), currency.getSign()));

                        } else {

                            sendHelpMessage(sender, command);

                            return;
                        }

                        return;

                    /*
                     *  usage:      /money exchange <Amount> [FromCurrency] <ToCurrency>
                     *  permission: adventuria.api.command.money.exchange.[FromCurrency].[ToCurrency]
                     */

                    case "exchange":

                        return;

                    case "enable":

                        return;

                    case "disable":

                        return;

                    case "reload":

                        return;

                    default:

                        sendHelpMessage(sender, command);

                }

            }

        } else {

            sender.sendMessage(lang.getPhraseContent("command-money-disabled"));

        }

    }

}
