package de.fabtopf.contray.adventuria.api.utils.bukkit.command.base;

import org.bukkit.command.CommandSender;

public interface SimpleFunctionCommandExecutor extends ICommandExecutor {

    void sendHelpMessage(CommandSender executor, SimpleCommand command);

}
