package de.fabtopf.contray.adventuria.api.utils.bukkit.command.base;

import lombok.Getter;
import org.bukkit.command.CommandSender;

import java.util.regex.Pattern;

@Getter
public abstract class SimpleSubCommand implements ISimpleSubCommand {

    public static final String REGEX_INT = "([0-9]+)$";
    public static final String REGEX_FLOAT = "([0-9]+)(([.,]([0-9]{1,4}))?)$";
    public static final String REGEX_PLAYER = "[a-zA-Z0-9_]{3,16}$";
    public static final String REGEX_UUID = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$";
    public static final String REGEX_STRING = ".*$";
    public static final String MULTI_STRING = "\\**";

    protected String permission;

    protected String[] requiredArgs;
    protected String[] helpMessageParams;

    protected Pattern[] requiredArgPatterns;

    protected boolean showInHelp;

    protected int display;

    protected Class<? extends CommandSender>[] allowedSenders;

    public boolean checkSender(CommandSender sender) {
        if(sender == null)
            return false;

        for(Class<? extends CommandSender> allowedSender : this.getAllowedSenders())
            if(allowedSender.isAssignableFrom(sender.getClass()))
                return true;

        return true;
    }

    public boolean checkPermission(CommandSender sender) {
        return sender.hasPermission(this.getPermission());
    }

    public boolean checkArguments(String[] args) {

        if(this.getRequiredArgs().length > args.length)
            return false;

        if(this.getRequiredArgs().length > 0)
            if(!this.getRequiredArgs()[this.getRequiredArgs().length - 1].endsWith(MULTI_STRING) &&
                    args.length > this.getRequiredArgs().length)
                return false;

        int count = 0;
        for(String pattern : this.getRequiredArgs()) {

            if(!pattern.endsWith(MULTI_STRING)) {
                if(!this.getRequiredArgPatterns()[count].matcher(args[count]).matches())
                    break;

                count++;
            } else {

                StringBuilder builder = new StringBuilder();
                for(int i = count; i < args.length; i++)
                    builder.append(" ").append(args[i]);

                args[count] = builder.toString().replaceFirst(" ", "");
                count++;

                break;
            }

        }

        return count == this.getRequiredArgs().length;
    }

}
