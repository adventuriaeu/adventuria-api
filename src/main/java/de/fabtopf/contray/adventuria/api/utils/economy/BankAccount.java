package de.fabtopf.contray.adventuria.api.utils.economy;

import com.arangodb.entity.BaseDocument;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.utils.arango.ArangoWritable;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@RequiredArgsConstructor
public class BankAccount implements EconomyObject, ArangoWritable<UUID> {

    @NonNull
    private UUID uniqueId;
    @Setter
    private String name;

    private long balance;

    @Setter
    private UUID currency;

    @Setter
    private Type type;

    private List<UUID> owners;
    private List<UUID> members;

    @Override
    public UUID getKey() {
        return uniqueId;
    }

    @Override
    public void read(BaseDocument document) {
        this.name = (String) document.getAttribute("name");
        this.balance = (long) document.getProperties().getOrDefault("balance", 0L);
        this.currency = document.getProperties().containsKey("type") ?  UUID.fromString((String) document.getAttribute("currency")) : null;
        this.type = document.getProperties().containsKey("type") ? Type.valueOf((String) document.getAttribute("type")) : null;
        this.owners = ((List<String>) document.getProperties().getOrDefault("owners", new ArrayList<>())).stream().map(UUID::fromString).collect(Collectors.toList());
        this.members = ((List<String>) document.getProperties().getOrDefault("members", new ArrayList<>())).stream().map(UUID::fromString).collect(Collectors.toList());
    }

    @Override
    public void save(BaseDocument document) {
        document.addAttribute("name", this.name);
        document.addAttribute("balance", this.balance);
        document.addAttribute("currency", this.currency);
        document.addAttribute("type", this.type);
        document.addAttribute("owners", this.owners);
        document.addAttribute("members", this.members);
    }

    @Override
    public long depositMoney(UUID currency, long value) {
        if(currency != null && !currency.equals(this.currency))
            return this.balance;

        if(value > 0)
            this.balance += value;

        AdviAPI.getInstance().bukkit().getBankProvider().update(this);
        return this.balance;
    }

    @Override
    public long withdrawMoney(UUID currency, long value) {
        if(currency != null && !currency.equals(this.currency))
            return this.balance;

        if(value > 0)
            this.balance -= value;

        AdviAPI.getInstance().bukkit().getBankProvider().update(this);
        return this.balance;
    }

    @Override
    public long setMoney(UUID currency, long value) {
        if(currency != null && !currency.equals(this.currency))
            return this.balance;

        if(value > 0)
            this.balance = value;

        AdviAPI.getInstance().bukkit().getBankProvider().update(this);
        return this.balance;
    }

    @Override
    public long balanceMoney(UUID currency) {
        if(currency != null && !currency.equals(this.currency))
            return 0L;

        return this.balance;
    }

    @Override
    public Map<UUID, Long> transformMoney(UUID fromCurrency, UUID toCurrency, long value) {
        return null;
    }

    public enum Type {
        BANK, EXTERNAL
    }

}
