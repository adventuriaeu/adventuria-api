package de.fabtopf.contray.adventuria.api.utils.bukkit.scoreboard;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RequiredArgsConstructor
public class DynamicScoreboardValue<ContentType> implements ScoreboardValue {

    @Getter
    @Setter
    @NonNull
    private String name;

    @NonNull
    private ContentType defaultValue;

    private final Map<UUID, ContentType> values = new HashMap<>();

    @Override
    public String getDefaultValue() {
        return defaultValue.toString();
    }

    @Override
    public String getValue(Player player) {
        return values.getOrDefault(player.getUniqueId(), defaultValue).toString();
    }

    public void setValue(Player player, ContentType value) {
        setValue(player.getUniqueId(), value);
    }

    public void setValue(UUID uniqueId, ContentType value) {
        values.put(uniqueId, value);
    }

    public void removeValue(Player player) {
        removeValue(player.getUniqueId());
    }

    public void removeValue(UUID uniqueId) {
        values.remove(uniqueId);
    }

}
