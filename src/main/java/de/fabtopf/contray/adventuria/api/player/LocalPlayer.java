package de.fabtopf.contray.adventuria.api.player;

import de.fabtopf.contray.adventuria.api.utils.bukkit.scoreboard.AdviScoreboard;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class LocalPlayer {

    @Setter(AccessLevel.NONE)
    private UUID uniqueId;

    private AdviScoreboard scoreboard;

    private String prefix;
    private String suffix;
    private String display;

    public LocalPlayer(UUID uniqueId) {
        this.uniqueId = uniqueId;

        this.scoreboard = null;
        this.prefix = null;
        this.suffix = null;
        this.display = null;
    }

}
