package de.fabtopf.contray.adventuria.api.utils.bukkit.command.base;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpleSubCommandHandler {

    String permission() default "";

    String[] requiredArgs();
    String[] helpMessageParams() default {};

    boolean showInHelp() default false;

    int display() default Integer.MAX_VALUE;

    Class<? extends CommandSender>[] allowedSenders() default {ConsoleCommandSender.class, Player.class};

}