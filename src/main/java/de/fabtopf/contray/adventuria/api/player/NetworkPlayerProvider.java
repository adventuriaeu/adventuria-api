package de.fabtopf.contray.adventuria.api.player;

import com.arangodb.ArangoCursor;
import com.arangodb.util.MapBuilder;
import com.arangodb.velocypack.VPackSlice;
import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.lib.player.OfflinePlayer;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.permission.GroupData;
import de.fabtopf.contray.adventuria.api.permission.PermissionGroup;
import de.fabtopf.contray.adventuria.api.permission.PermissionUser;
import de.fabtopf.contray.adventuria.api.player.skin.SkinDataProvider;
import de.fabtopf.contray.adventuria.api.utils.arango.CollectionManager;
import de.fabtopf.contray.adventuria.api.utils.arango.WritableGenerator;
import de.fabtopf.contray.adventuria.api.utils.cloud.ChannelMessageDocument;
import lombok.Getter;

import java.util.*;

public abstract class NetworkPlayerProvider extends CollectionManager<NetworkPlayer, UUID> {

    private Map<String, GroupData> groupDataMap = new HashMap<>();
    @Getter
    private Map<UUID, LocalPlayer> localPlayerMap = new HashMap<>();
    private Map<String, UUID> playerUUIDMap = new HashMap<>();

    @Getter
    private SkinDataProvider skinDataProvider = new SkinDataProvider();

    public NetworkPlayerProvider(CacheCondition<NetworkPlayer, UUID> cacheCondition) {
        super("player", new WritableGenerator<NetworkPlayer, UUID>() {

            @Override
            public NetworkPlayer generate(UUID uniqueId) {
                return new NetworkPlayer(uniqueId);
            }

        }, true, cacheCondition);
    }

    public NetworkPlayer get(UUID uniqueId) {
        if(AdviAPI.getInstance().isOnline(uniqueId)) {
            NetworkPlayer np = super.get(uniqueId);

            playerUUIDMap.put(np.getName().toLowerCase(), uniqueId);
            return np;
        }

        return super.getOnce(uniqueId);
    }

    public NetworkPlayer get(String name) {
        UUID uniqueId;
        if((uniqueId = this.getUniqueId(name)) != null)
            return this.get(uniqueId);

        return null;
    }

    public LocalPlayer getLocal(UUID uniqueId) {
        return localPlayerMap.get(uniqueId);
    }

    public LocalPlayer getLocal(String name) {
        UUID uniqueId;
        if((uniqueId = this.getUniqueId(name)) != null)
            return this.getLocal(uniqueId);

        return null;
    }

    public boolean checkPermission(UUID uniqueId, String permission) {
        if(permission == null || uniqueId == null)
            throw new IllegalArgumentException("permission or uniqueId is null");

        OfflinePlayer player = CloudAPI.getInstance().getOfflinePlayer(uniqueId);

        return player != null
                && player.getPermissionEntity().hasPermission(
                        CloudAPI.getInstance().getPermissionPool(),
                permission,
                CloudAPI.getInstance().getGroup());
    }

    public String getName(UUID uniqueId) {
        String name = null;

        if(AdviAPI.getInstance().saveRedisRequest(redis -> redis.exists("advi-player-" + uniqueId.toString()), false))
            name = AdviAPI.getInstance().saveRedisRequest(redis -> redis.get("advi-player-" + uniqueId.toString()), null);

        if(name == null)
            return get(uniqueId).getName();

        return name;
    }

    public UUID getUniqueId(String name) {
        UUID uniqueId = null;

        if(playerUUIDMap.containsKey(name.toLowerCase()))
            uniqueId = playerUUIDMap.get(name.toLowerCase());

        if(uniqueId == null && AdviAPI.getInstance().saveRedisRequest(redis -> redis.exists("advi-player-" + name.toLowerCase()), false))
            try {
                uniqueId = UUID.fromString(AdviAPI.getInstance().saveRedisRequest(redis -> redis.get("advi-player-" + name.toLowerCase()), null));
            } catch (Exception ignored) {}

        if(uniqueId == null) {
            ArangoCursor<VPackSlice> cursor = AdviAPI.getInstance().getArangoDatabase().query(
                    "FOR player IN " + this.collection.name() + " FILTER LIKE(player.name, @name, true) " +
                            "RETURN {uniqueId: player._key}",
                    new MapBuilder().put("name", name).get(),
                    null,
                    VPackSlice.class);

            if(cursor.hasNext())
                uniqueId = UUID.fromString(cursor.next().get("uniqueId").getAsString());

            this.closeCursor(cursor);
        }

        return uniqueId;
    }

    public Collection<NetworkPlayer> getOnlinePlayers() {
        return cache == null ? Collections.EMPTY_LIST : Collections.unmodifiableCollection(cache.values());
    }

    public NetworkPlayer handleJoin(UUID uniqueId) {

        NetworkPlayer player;
        if((player = this.getNoCached(uniqueId)) == null)
            this.register(uniqueId);

        PermissionUser user = player.getPermissionUser();
        LocalPlayer localPlayer = new LocalPlayer(uniqueId);
        this.getLocalPlayerMap().put(uniqueId, localPlayer);
        localPlayer.setDisplay(user.getDisplay());
        localPlayer.setPrefix(user.getPrefix());
        localPlayer.setSuffix(user.getSuffix());

        return player;
    }

    public GroupData getGroupData(String group) {

        GroupData data;
        PermissionGroup permissionGroup;
        if((data = groupDataMap.get(group)) != null) {
            return data;
        } else if((permissionGroup = AdviAPI.getInstance().getPermissionPool().get(group)) != null) {
            groupDataMap.put(permissionGroup.getName(), data = new GroupData(permissionGroup));
            return data;
        }

        return null;
    }

    public boolean isOnline(UUID uniqueId) {
        return AdviAPI.getInstance().isOnline(uniqueId);
    }

    public void updateSkin(UUID uniqueId, String skinName) {
        AdviAPI.getInstance().getCloudUtilities().sendServerChannelMessage(
                "advi-player", "update-skin",
                new ChannelMessageDocument(new Document()
                        .append("uniqueId", uniqueId)
                        .append("skinName", skinName)));
    }

    @Override
    public void uncache(UUID key) {
        super.uncache(key);

        String name = null;
        for(Map.Entry<String, UUID> entry : playerUUIDMap.entrySet())
            if(entry.getValue().equals(key))
                name = entry.getKey();

        if(name != null)
            playerUUIDMap.remove(name);
    }
}
