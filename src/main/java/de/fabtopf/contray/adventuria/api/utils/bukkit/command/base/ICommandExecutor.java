package de.fabtopf.contray.adventuria.api.utils.bukkit.command.base;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;

import java.util.List;
import java.util.stream.Collectors;

public interface ICommandExecutor {

    String getUsage();
    String getPermissionMessage();
    String getDescription();

    void onExecute(CommandSender sender, SimpleCommand command, String label, String[] args);
    void sendHelpMessage(CommandSender sender, SimpleCommand command);

    default List<String> onTabComplete(CommandSender sender, SimpleCommand command, String fallback, String[] args, Location location) {
        return Bukkit.getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList());
    }

}
