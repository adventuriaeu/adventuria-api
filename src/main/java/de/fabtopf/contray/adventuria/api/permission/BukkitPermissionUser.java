package de.fabtopf.contray.adventuria.api.permission;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissibleBase;
import org.bukkit.permissions.PermissionAttachmentInfo;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class BukkitPermissionUser extends PermissionUser {

    public BukkitPermissionUser(NetworkPlayer player, Map<String, Object> data) {
        super(player, data);
    }

    private Map<String, Boolean> getBukkitPlayerPermissions() {
        Map<String, Boolean> perms = new HashMap<>();

        Player player;
        if((player = Bukkit.getPlayer(getPlayer().getUniqueId())) != null)
            try {
                Class<?> craftHumanEntityClass = Class.forName("org.bukkit.craftbukkit." + AdviAPI.getInstance().bukkit().getPackageVersion() + ".entity.CraftHumanEntity");
                Field permissionBaseField = craftHumanEntityClass.getDeclaredField("perm");
                permissionBaseField.setAccessible(true);
                PermissibleBase base = (PermissibleBase) permissionBaseField.get(player);

                Field permissionField = PermissibleBase.class.getDeclaredField("permissions");
                permissionField.setAccessible(true);
                Collection<PermissionAttachmentInfo> permissions = ((Map<String, PermissionAttachmentInfo>) permissionField.get(base)).values();

                perms.putAll(permissions.stream().collect(Collectors.toMap(p -> p.getPermission().toLowerCase(), PermissionAttachmentInfo::getValue)));
            } catch (Exception ignored) {}

        return perms;
    }

    @Override
    Map<String, Boolean> getCalculatedGlobalPermissions() {
        Map<String, Boolean> map = getBukkitPlayerPermissions();
        map.putAll(super.getCalculatedGlobalPermissions());
        return map;
    }

    @Override
    Map<String, Boolean> getCalculatedServerGroupPermissions(String serverGroup) {
        Map<String, Boolean> map = getBukkitPlayerPermissions();
        map.putAll(super.getCalculatedServerGroupPermissions(serverGroup));
        return map;
    }

    @Override
    Map<String, Boolean> getCalculatedWorldPermissions(String serverGroup, String world) {
        Map<String, Boolean> map = getBukkitPlayerPermissions();
        map.putAll(super.getCalculatedWorldPermissions(serverGroup, world));
        return map;
    }

}
