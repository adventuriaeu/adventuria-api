package de.fabtopf.contray.adventuria.api.permission;

import de.fabtopf.contray.adventuria.api.utils.arango.parser.Serializable;
import lombok.*;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FallbackPermissionGroupEntity implements Serializable {

    private String permissionGroup;

    private long duration;

    private FallbackPermissionGroupEntity fallback;

    public FallbackPermissionGroupEntity(Map<String, Object> data) {
        this.deserialize(data);
    }

    @Override
    public void serialize(Map<String, Object> data) {
        data.put("permissionGroup", permissionGroup);
        data.put("duration", duration);

        Map<String, Object> fallbackData = new HashMap<>();
        if(fallback != null) fallback.serialize(fallbackData);

        data.put("fallback", fallbackData);
    }

    @Override
    public void deserialize(Map<String, Object> data) {
        this.permissionGroup = (String) data.get("permissionGroup");
        this.duration = (long) data.get("duration");

        Map<String, Object> fallbackData;
        if((fallbackData = (Map<String, Object>) data.get("fallback")) == null || fallbackData.isEmpty()) this.fallback = null;
        else this.fallback = new FallbackPermissionGroupEntity(fallbackData);
    }

}
