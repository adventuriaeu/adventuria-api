package de.fabtopf.contray.adventuria.api.utils.bukkit.command.base;

import lombok.Getter;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.UUID;
import java.util.regex.Pattern;

@Getter
public class SimpleCallbackSubCommand extends SimpleSubCommand {

    private Action action;

    public SimpleCallbackSubCommand(String permission, String[] requiredArgs, String[] helpMessageParams,
                                    boolean showInHelp, int display, Class<? extends CommandSender>[] allowedSenders, Action action) {

        try {

            this.permission = permission;
            this.requiredArgs = requiredArgs;
            this.helpMessageParams = helpMessageParams;
            this.showInHelp = showInHelp;
            this.display = display;
            this.allowedSenders = allowedSenders;
            this.action = action;

            this.requiredArgPatterns = (Pattern[]) Arrays.stream(this.requiredArgs).map(s -> Pattern.compile(s, Pattern.CASE_INSENSITIVE)).toArray();

        } catch (Exception ignored) {}

    }

    @Override
    public boolean trigger(CommandSender sender, SimpleCommand command, String label, String[] args) {
        if(!this.checkSender(sender))
            return false;

        if(!this.checkPermission(sender))
            return false;

        if(!this.checkArguments(args))
            return false;

        Object[] params = new Object[args.length];

        for(int i = 0; i < this.getRequiredArgs().length; i++)
            switch (this.getRequiredArgs()[i]) {
                case REGEX_FLOAT:
                    params[i] = Float.parseFloat(args[i].replace(",", "."));
                    break;
                case REGEX_INT:
                    params[i] = Integer.parseInt(args[i]);
                    break;
                case REGEX_UUID:
                    params[i] = UUID.fromString(args[i]);
                    break;
                default:
                    params[i] = args[i];
                    break;
            }

        try {

            this.getAction().call(sender, command, params);

            return true;
        } catch (Exception e) {

            command.getExecutor().sendHelpMessage(sender, command);
            return true;
        }

    }

    public interface Action {

        void call(CommandSender sender, SimpleCommand command, Object... args);

    }

}
