package de.fabtopf.contray.adventuria.api.utils.bukkit.inventory;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

import java.util.*;

@Getter
public class SimpleInventory {

    private String title;

    private boolean temporary;

    private InventoryLayout layout;

    private final Map<Integer, SimpleItem> content = new HashMap<>();
    private final Map<String, Object> replacements = new HashMap<>();

    private List<SimpleItem> variableContent;

    private InventoryEventAction<InventoryCloseEvent> closeAction;
    private InventoryEventAction<InventoryOpenEvent> openAction;
    private InventoryEventAction<InventoryClickEvent> clickAction;

    private transient Inventory instance;

    public SimpleInventory(String title, boolean temporary, InventoryLayout layout, InventoryEventAction<InventoryCloseEvent> closeAction, InventoryEventAction<InventoryOpenEvent> openAction, InventoryEventAction<InventoryClickEvent> clickAction) {
        this.title = title;
        this.temporary = temporary;
        this.layout = layout;
        this.closeAction = closeAction;
        this.openAction = openAction;
        this.clickAction = clickAction;

        this.variableContent = new ArrayList<>();
    }

    public boolean isValid() {
        return this.title != null &&
                this.layout != null &&
                (!(this.layout instanceof MultiPageInventoryLayout) ||
                        this.variableContent != null) &&
                (this.layout.getType() != InventoryType.CHEST ||
                        (this.layout.getSlots() > 0 && this.layout.getSlots() % 9 == 0));
    }

    public Inventory getInstance() {
        if(this.instance == null)
            this.updateInstance();

        return this.instance;
    }

    public void updateInstance() {

        if(this.instance == null) {

            if (this.isValid()) {

                if (this.layout.getType() == InventoryType.CHEST)
                    this.instance = Bukkit.createInventory(null, this.layout.getSlots(), this.title);
                else
                    this.instance = Bukkit.createInventory(null, this.layout.getType(), this.title);

            } else {

                return;
            }

        }

        if(this.layout instanceof MultiPageInventoryLayout) {
            MultiPageInventoryLayout layout = (MultiPageInventoryLayout) this.layout;

            SimpleItem filler = this.layout.getEmptySlotFiller();
            if(layout.fillEmptySlots() &&  filler != null)
                for (int i = 0; i < this.layout.getSlots(); i++)
                    this.instance.setItem(i, filler.getItemStack());


            int page = (int) this.replacements.get("page");
            Iterator<SimpleItem> variableItemIterator = this.variableContent
                    .subList((page - 1) * layout.getVariableSlots().length,
                            this.variableContent.size() >= page * layout.getVariableSlots().length - 1 ?
                                    page * layout.getVariableSlots().length :
                                    this.variableContent.size())
                    .iterator();

            SimpleItem variableFiller = layout.getVariableSlotFiller();
            for (int i : layout.getVariableSlots())
                if (variableItemIterator.hasNext()) {

                    this.instance.setItem(i, variableItemIterator.next().getItemStack());

                } else {

                    if (layout.fillEmptyVariableSlots() && variableFiller != null)
                        this.instance.setItem(i, variableFiller.getItemStack());
                    else
                        this.instance.setItem(i, null);

                }

            if(layout.getCurrentPageDisplay() != null)
                layout.setCurrentPageDisplayPosition(this, layout.getCurrentPageDisplay(), page);

            if(layout.getNextPageItem() != null)
                layout.setNextPageItemPosition(this, layout.getNextPageItem(), page);

            if(layout.getPreviousPageItem() != null)
                layout.setPreviousPageItemPosition(this, layout.getPreviousInventoryItem(), page);

            if(layout.getPreviousInventoryItem() != null)
                layout.setPreviousInventoryItemPosition(this, layout.getPreviousInventoryItem());

            this.content.forEach((k, v) -> this.instance.setItem(k, v.getItemStack()));

        } else {

            SimpleItem filler = this.layout.getEmptySlotFiller();
            if(this.layout.fillEmptySlots() && filler != null)
                for(int i = 0; i < this.layout.getSlots(); i++)
                    this.instance.setItem(i, filler.getItemStack());

            this.content.forEach((k, v) -> this.instance.setItem(k, v.getItemStack()));

        }

    }

}
