package de.fabtopf.contray.adventuria.api.permission;

import de.fabtopf.contray.adventuria.api.utils.arango.parser.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PermissionGroupEntity implements Serializable {

    private String permissionGroup;

    private long assignmentDate;
    private long duration;

    private FallbackPermissionGroupEntity fallback;

    public PermissionGroupEntity(FallbackPermissionGroupEntity fallback, PermissionGroupEntity currentGroup) {
        this(fallback.getPermissionGroup(), currentGroup.assignmentDate + currentGroup.duration,
                fallback.getDuration(), fallback.getFallback());
    }

    public PermissionGroupEntity(Map<String, Object> data) {
        this.deserialize(data);
    }

    public boolean isExpired() {
        return duration != 0 && System.currentTimeMillis() > assignmentDate + duration;
    }

    @Override
    public void serialize(Map<String, Object> data) {
        data.put("permissionGroup", permissionGroup);
        data.put("assignmentDate", assignmentDate);
        data.put("duration", duration);

        Map<String, Object> fallbackData = new HashMap<>();
        if(fallback != null) fallback.serialize(fallbackData);

        data.put("fallback", fallbackData);
    }

    @Override
    public void deserialize(Map<String, Object> data) {
        this.permissionGroup = (String) data.get("permissionGroup");
        this.assignmentDate = (long) data.get("assignmentDate");
        this.duration = (long) data.get("duration");

        Map<String, Object> fallbackData;
        if((fallbackData = (Map<String, Object>) data.get("fallback")) == null || fallbackData.isEmpty()) this.fallback = null;
        else this.fallback = new FallbackPermissionGroupEntity(fallbackData);
    }

}
