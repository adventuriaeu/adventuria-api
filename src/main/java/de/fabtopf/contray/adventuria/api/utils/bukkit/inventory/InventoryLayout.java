package de.fabtopf.contray.adventuria.api.utils.bukkit.inventory;

import org.bukkit.event.inventory.InventoryType;

public interface InventoryLayout {

    InventoryType getType();

    int getSlots();

    boolean fillEmptySlots();

    SimpleItem getEmptySlotFiller();

}
