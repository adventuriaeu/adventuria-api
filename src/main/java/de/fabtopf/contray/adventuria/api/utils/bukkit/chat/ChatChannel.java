package de.fabtopf.contray.adventuria.api.utils.bukkit.chat;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
public class ChatChannel {

    private String name;
    private String format;
    private String communicationChannel;
    private String prefix;
    private String display;
    private String writePermission;
    private String readPermission;
    private String spyPermission;
    private String command;

    private boolean isDefault;

    private int range;

    private Type type;

    @Setter(AccessLevel.PACKAGE)
    private transient Object commandObj;

    public ChatChannel(String name, String format, String communicationChannel, String prefix, String display, String writePermission,
                       String readPermission, String spyPermission, String command, boolean standard, int range, Type type) {
        this.name = name;
        this.format = format;
        this.communicationChannel = communicationChannel;
        this.prefix = prefix;
        this.display = display;
        this.writePermission = writePermission;
        this.readPermission = readPermission;
        this.spyPermission = spyPermission;
        this.command = command;
        this.isDefault = standard;
        this.range = range;
        this.type = type;
    }

    public boolean equals(ChatChannel chatChannel) {
        return (chatChannel.getName() != null && chatChannel.getName().equals(name)) || (chatChannel.getPrefix() != null && chatChannel.getPrefix().equals(prefix));
    }

    public enum Type {

        LOCAL,
        SERVER,
        GLOBAL,
        CUSTOM

    }

}
