package de.fabtopf.contray.adventuria.api;

import com.arangodb.ArangoDB;
import com.arangodb.ArangoDatabase;
import de.fabtopf.contray.adventuria.api.internal.AdviConfiguration;
import de.fabtopf.contray.adventuria.api.permission.PermissionPool;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayerProvider;
import de.fabtopf.contray.adventuria.api.utils.arango.CollectionManager;
import de.fabtopf.contray.adventuria.api.utils.bukkit.chat.ChatManager;
import de.fabtopf.contray.adventuria.api.utils.cloud.CloudUtilities;
import de.fabtopf.contray.adventuria.api.utils.economy.EconomyProvider;
import lombok.Getter;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.UUID;

@Getter
public abstract class AdviAPI {

    @Getter
    private static AdviAPI instance;

    protected boolean devMode;
    protected ArangoDB arango;
    protected Jedis redisClient;
    protected JedisPool redisPool;
    protected ArangoDatabase arangoDatabase;
    protected CloudUtilities cloudUtilities;

    protected PermissionPool permissionPool;
    protected ChatManager chatManager;
    protected CollectionManager<EconomyProvider, String> economyStorage;

    private String redisPassword;

    protected AdviAPI(AdviConfiguration configuration) {

        AdviAPI.instance = this;

        this.arango = configuration.buildArangoConnection();
        this.arangoDatabase = this.arango.db(configuration.getArangoDatabase());

        try {
            this.redisPool = new JedisPool(configuration.getRedisHost(), configuration.getRedisPort());

            this.redisClient = this.redisPool.getResource();
            this.redisClient.getClient().setTimeoutInfinite();
            this.redisClient.auth(redisPassword = configuration.getRedisPassword());
        } catch(Exception e) {
            System.err.println("Could not connect to Redis");
            e.printStackTrace();
        }

        this.permissionPool = new PermissionPool("permission_groups", this.arangoDatabase);
        this.permissionPool.getGroupNames(true).forEach(key -> this.permissionPool.get(key));

        this.cloudUtilities = new CloudUtilities();
        this.economyStorage = new CollectionManager<>("economy_settings", EconomyProvider::new,
                true, (key, obj) -> key.equals(getServerName()));
    }

    protected void cleanup() {

        this.arango.shutdown();
        this.getPlayerProvider().clearCache();
        this.permissionPool.clearCache();
        this.redisPool.close();
        this.redisClient.close();

        this.arangoDatabase = null;
        this.redisPool = null;
        this.arango = null;
        this.redisClient = null;

        AdviAPI.instance = null;

    }

    public AdviAPIBukkit bukkit() {
        return (AdviAPIBukkit) AdviAPI.instance;
    }

    public AdviAPIBungee bungee() {
        return (AdviAPIBungee) AdviAPI.instance;
    }

    public abstract NetworkPlayerProvider getPlayerProvider();

    public abstract boolean isOnline(UUID uniqueId);

    public boolean isProxy() {
        return (this instanceof AdviAPIBungee);
    }

    public String getServerName() {
        return this.cloudUtilities.getServerName();
    }

    public EconomyProvider getEconomyProvider(String serverId) {
        return economyStorage.get(serverId);
    }

    public <T> T saveRedisRequest(RedisCallback<T> callable, T defaultValue) {
        T result = defaultValue;
        try (Jedis redis = redisPool.getResource()) {
            redis.auth(this.redisPassword);
            result = callable.call(redis);
        }

       return result;
    }

    public interface RedisCallback<T> {
        T call(Jedis jedis);
    }

}
