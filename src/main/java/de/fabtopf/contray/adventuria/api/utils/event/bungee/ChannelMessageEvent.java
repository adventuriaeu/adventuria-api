package de.fabtopf.contray.adventuria.api.utils.event.bungee;

import de.fabtopf.contray.adventuria.api.utils.cloud.ChannelMessageDocument;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Event;

@Getter
@AllArgsConstructor
public class ChannelMessageEvent extends Event {

    private String channel;
    private String message;
    private ChannelMessageDocument document;

}
