package de.fabtopf.contray.adventuria.api.internal;

import com.arangodb.ArangoDB;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class AdviConfiguration {

    private String arangoHost;
    private String arangoUser;
    private Integer arangoPort;
    private String arangoPassword;
    private String arangoDatabase;

    private String redisHost;
    private Integer redisPort;
    private String redisPassword;

    private Map<String, Object> additional;

    public ArangoDB buildArangoConnection() {
        return new ArangoDB.Builder()
                .host(arangoHost, arangoPort)
                .user(arangoUser)
                .password(arangoPassword)
                .build();
    }

}
