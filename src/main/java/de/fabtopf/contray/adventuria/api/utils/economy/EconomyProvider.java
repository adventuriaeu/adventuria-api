package de.fabtopf.contray.adventuria.api.utils.economy;

import com.arangodb.entity.BaseDocument;
import de.fabtopf.contray.adventuria.api.utils.arango.ArangoWritable;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
public class EconomyProvider implements ArangoWritable<String> {

    @Getter
    private static CurrencyProvider currencyProvider = new CurrencyProvider();

    private String key;

    private List<UUID> enabledCurrencies;

    @Setter
    private UUID mainCurrency;

    public EconomyProvider(String key) {
        this.key = key;

        this.enabledCurrencies = new ArrayList<>();
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void read(BaseDocument document) {
        this.enabledCurrencies = ((List<String>) document.getAttribute("enabled_currencies"))
                .stream().map(UUID::fromString).collect(Collectors.toList());

        String mainCurrency = (String) document.getAttribute("main_currency");
        this.mainCurrency = mainCurrency == null ? null : UUID.fromString(mainCurrency);
    }

    @Override
    public void save(BaseDocument document) {
        document.addAttribute("enabled_currencies", enabledCurrencies);
        document.addAttribute("main_currency", mainCurrency);
    }

}
