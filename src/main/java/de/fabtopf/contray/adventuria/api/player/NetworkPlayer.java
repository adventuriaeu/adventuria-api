package de.fabtopf.contray.adventuria.api.player;

import com.arangodb.entity.BaseDocument;
import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.permission.BukkitPermissionUser;
import de.fabtopf.contray.adventuria.api.permission.GroupData;
import de.fabtopf.contray.adventuria.api.permission.PermissionGroup;
import de.fabtopf.contray.adventuria.api.permission.PermissionUser;
import de.fabtopf.contray.adventuria.api.utils.arango.ArangoWritable;
import de.fabtopf.contray.adventuria.api.utils.bukkit.scoreboard.AdviScoreboard;
import de.fabtopf.contray.adventuria.api.utils.bukkit.scoreboard.ScoreboardValue;
import de.fabtopf.contray.adventuria.api.utils.cloud.ChannelMessageDocument;
import de.fabtopf.contray.adventuria.api.utils.economy.EconomyObject;
import lombok.AccessLevel;
import lombok.Getter;
import org.bukkit.Bukkit;

import java.util.*;
import java.util.stream.Collectors;

@Getter
public class NetworkPlayer implements ArangoWritable<UUID>, EconomyObject {

    private UUID uniqueId;
    private String name;

    private PermissionUser permissionUser;

    @Getter(AccessLevel.PACKAGE)
    private Map<String, Object> properties;

    public NetworkPlayer(UUID uniqueId) {
        this.uniqueId = uniqueId;
        this.name = CloudAPI.getInstance().getOfflinePlayer(uniqueId).getName();
        this.properties = new HashMap<>();
        this.permissionUser = new PermissionUser(this, new HashMap<>());

        for(PlayerProperty<?, ?> property : PlayerProperty.values())
            if(property.getDefaultCacheValue() != null)
                this.properties.put(property.getIdentifier(), property.getDefaultCacheValue());
    }

    @Override
    public UUID getKey() {
        return uniqueId;
    }

    @Override
    public void read(BaseDocument document) {

        this.properties = (Map<String, Object>) document.getProperties().getOrDefault("properties", new HashMap<>());
        for(PlayerProperty property : PlayerProperty.values())
            this.properties.put(property.getIdentifier(), property.getCallback().parse(this.properties));

        this.permissionUser =
                AdviAPI.getInstance().isProxy() ?
                new PermissionUser(this, (Map<String, Object>) document.getProperties().getOrDefault("permissionData", new HashMap<>())) :
                new BukkitPermissionUser(this, (Map<String, Object>) document.getProperties().getOrDefault("permissionData", new HashMap<>()));
    }

    @Override
    public void save(BaseDocument document) {
        document.addAttribute("name", this.name);
        document.addAttribute("properties", this.properties);
        document.addAttribute("highestGroup", this.getHighestGroup());

        Map<String, Object> permissionData = new HashMap<>();
        permissionUser.serialize(permissionData);

        document.addAttribute("permissionData", permissionData);
    }

    public boolean isTeamMember() {
        return this.getGroups().stream()
                .map(s -> AdviAPI.getInstance().getPlayerProvider().getGroupData(s))
                .anyMatch(o -> o != null && o.isTeamGroup());
    }

    public boolean isLocalTeamMember() {
        return this.getGroups().stream()
                .map(s -> AdviAPI.getInstance().getPlayerProvider().getGroupData(s))
                .anyMatch(o -> o != null && o.isAvailable() && o.isTeamGroup());
    }

    public String getNameFormatted() {

        String group;
        if((group = AdviAPI.getInstance().getCloudUtilities().getGroup()) != null)
            return getPermissionUser().getDisplay(group) + getName();

        return getPermissionUser().getDisplay() + getName();
    }

    public Collection<String> getGroups() {
        return getPermissionUser().getCurrentGroups().stream().map(PermissionGroup::getName).collect(Collectors.toList());
    }

    public String getHighestGroup() {
        return getHighestPermissionGroup().getName();
    }

    public PermissionGroup getHighestPermissionGroup() {

        String group;
        if((group = AdviAPI.getInstance().getCloudUtilities().getGroup()) != null)
            return getPermissionUser().getCurrentGroups(group).get(0);

        return getPermissionUser().getCurrentGroups().get(0);
    }

    public GroupData getHighestGroupData() {
        return AdviAPI.getInstance().getPlayerProvider().getGroupData(this.getHighestGroup());
    }

    public void updateScoreboard(String scoreboard) {
        updateScoreboard(AdviAPI.getInstance().bukkit().getScoreboardManager().getScoreboards().get(scoreboard));
    }

    public void updateScoreboard(List<ScoreboardValue> values) {
        AdviScoreboard scoreboard = new AdviScoreboard(AdviAPI.getInstance().bukkit().getScoreboardManager(), UUID.randomUUID().toString(), false, "§2§lAdvi§a§lStats");
        scoreboard.getValues().addAll(values);

        updateScoreboard(scoreboard);
    }

    public void updateScoreboard(AdviScoreboard scoreboard) {
        if(AdviAPI.getInstance().isProxy() || !isOnline())
            return;

        List<String> valueList = new ArrayList<>();
        Map<String, List<String>> scoreboards = (Map<String, List<String>>) this.properties.get(PlayerProperty.SCOREBOARD);
        for(ScoreboardValue value : scoreboard.getValues())
            for(Map.Entry entry : AdviAPI.getInstance().bukkit().getScoreboardManager().getValueMap().entrySet())
                if(value.equals(entry.getValue()))
                    valueList.add((String) entry.getKey());

        scoreboards.put(AdviAPI.getInstance().getServerName(), valueList);
        this.updateProperty(PlayerProperty.SCOREBOARD, scoreboards);

        AdviAPI.getInstance().getPlayerProvider().getLocal(uniqueId).setScoreboard(scoreboard);
        AdviAPI.getInstance().bukkit().getScoreboardManager().updateScoreboard(Bukkit.getPlayer(uniqueId), scoreboard);
    }

    public void updateSkin(String name) {
        if(name == null)
            return;

        AdviAPI.getInstance().getPlayerProvider().updateSkin(uniqueId, name);
    }

    public <Type> Type getProperty(PlayerProperty<Type, ?> property) {
        if (this.properties.containsKey(property.getIdentifier()))
            return property.getCallback().get(this);

        return property.getDefaultOutputValue();
    }

    public <Type> void updateProperty(PlayerProperty<?, Type> property, Type value) {
        this.updateProperty$(property, value);
        this.sendPropertyUpdate();
    }

    public <Type> void updateProperty$(PlayerProperty<?, Type> property, Type value) {
        this.properties.put(property.getIdentifier(), value);
        AdviAPI.getInstance().getPlayerProvider().save(this);
    }

    public void sendPropertyUpdate() {
        AdviAPI.getInstance().getCloudUtilities().sendProxyChannelMessage(
                "advi-player", "update",
                new ChannelMessageDocument(new Document("uniqueId", this.getKey())));
        AdviAPI.getInstance().getCloudUtilities().sendServerChannelMessage(
                "advi-player", "update",
                new ChannelMessageDocument(new Document("uniqueId", this.getKey())));
    }

    public boolean isOnline() {
        return AdviAPI.getInstance().isOnline(uniqueId);
    }

    public void send(String server) {
        CloudAPI.getInstance().sendCustomSubProxyMessage("advi-player", "send",
                new Document("uniqueId", this.uniqueId).append("server", server));
    }

    public void sendMessage(String message) {
        CloudAPI.getInstance().sendCustomSubProxyMessage("advi-player", "message",
                new Document("uniqueId", this.uniqueId).append("message", message));
    }

    @Override
    public long depositMoney(UUID currency, long value) {
        long money = getProperty(PlayerProperty.MONEY).getOrDefault(currency, 0L);
        if(value > 0)
            money += value;

        Map<String, Long> moneyMap = (Map<String, Long>) properties.get(PlayerProperty.MONEY.getIdentifier());
        moneyMap.put(currency.toString(), money);

        updateProperty(PlayerProperty.MONEY, moneyMap);

        return money;
    }

    @Override
    public long withdrawMoney(UUID currency, long value) {
        long money = getProperty(PlayerProperty.MONEY).getOrDefault(currency, 0L);
        if(value > 0 && money >= value)
            money -= value;

        Map<String, Long> moneyMap = (Map<String, Long>) properties.get(PlayerProperty.MONEY.getIdentifier());
        moneyMap.put(currency.toString(), money);

        updateProperty(PlayerProperty.MONEY, moneyMap);

        return money;
    }

    @Override
    public long setMoney(UUID currency, long value) {
        if(value < 0)
            value = 0;

        Map<String, Long> moneyMap = (Map<String, Long>) properties.get(PlayerProperty.MONEY.getIdentifier());
        moneyMap.put(currency.toString(), value);

        updateProperty(PlayerProperty.MONEY, moneyMap);

        return value;
    }

    @Override
    public long balanceMoney(UUID currency) {
        return getProperty(PlayerProperty.MONEY).getOrDefault(currency, 0L);
    }

    @Override
    public Map<UUID, Long> transformMoney(UUID fromCurrency, UUID toCurrency, long value) {
        return null;
    }

}
