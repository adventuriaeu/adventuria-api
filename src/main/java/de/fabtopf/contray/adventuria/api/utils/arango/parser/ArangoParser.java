package de.fabtopf.contray.adventuria.api.utils.arango.parser;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArangoParser<T> {

    private FieldModifier[] fieldModifiers;
    private Map<String, ArangoParser> innerObjects;
    private Class<T> objectClass;

    public ArangoParser(Class<T> objectClass) {
        this.objectClass = objectClass;
        this.innerObjects = new HashMap<>();
    }

    public ArangoParser<T> fieldModifiers(FieldModifier... fieldModifiers) {
        this.fieldModifiers = fieldModifiers;
        return this;
    }

    public ArangoParser<T> innerObject(String field, ArangoParser parser) {
        this.innerObjects.put(field, parser);
        return this;
    }

    @SuppressWarnings("unchecked")
    public T parse(Map<String, Object> toParse) {
        T object;

        try {
            object = this.objectClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }

        for(Map.Entry<String, Object> entry : toParse.entrySet()) {
            Object value = entry.getValue();

            if(this.innerObjects.containsKey(entry.getKey())) {
                ArangoParser parser = this.innerObjects.get(entry.getKey());
                if(value instanceof List) {
                    List list = (List) value;
                    if(isListTypeOf(list, HashMap.class)) {
                        value = parser.parseList(list);
                    }
                } else if(value instanceof Map) {
                    value = parser.parse((Map<String, Object>) value);
                }
            }

            if(this.fieldModifiers != null) {
                for (FieldModifier modifier : this.fieldModifiers) {
                    if (modifier.getField().equalsIgnoreCase(entry.getKey()))
                        value = modifier.getValue(value);
                }
            }

            try {
                Field field = this.objectClass.getDeclaredField(entry.getKey());
                field.setAccessible(true);
                if((field.getType() == int.class || field.getType() == Integer.class)
                        && value instanceof Long)
                    value = Math.toIntExact((Long) value);
                field.set(object, value);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return object;
    }

    @SuppressWarnings("unchecked")
    public void parseInObject(Map<String, Object> toParse, T object) {
        for(Map.Entry<String, Object> entry : toParse.entrySet()) {
            Object value = entry.getValue();

            if(this.innerObjects.containsKey(entry.getKey())) {
                ArangoParser parser = this.innerObjects.get(entry.getKey());
                if(value instanceof List) {
                    List list = (List) value;
                    if(isListTypeOf(list, HashMap.class)) {
                        value = parser.parseList(list);
                    }
                } else if(value instanceof Map) {
                    value = parser.parse((Map<String, Object>) value);
                }
            }

            if(this.fieldModifiers != null) {
                for (FieldModifier modifier : this.fieldModifiers) {
                    if (modifier.getField().equalsIgnoreCase(entry.getKey()))
                        value = modifier.getValue(value);
                }
            }

            try {
                Field field = object.getClass().getDeclaredField(entry.getKey());
                field.setAccessible(true);
                if((field.getType() == int.class || field.getType() == Integer.class)
                        && value instanceof Long)
                    value = Math.toIntExact((Long) value);
                field.set(object, value);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public List<T> parseList(List<Map<String, Object>> toParse) {
        List<T> list = new ArrayList<>();
        for(Map<String, Object> map : toParse)
            list.add(this.parse(map));

        return list;
    }

    public static boolean isListTypeOf(List list, Class<?> clazz) {
        for(Object object : list) {
            if(object.getClass() == clazz)
                return true;
        }

        return false;
    }


    public interface ParseMethod<F, T> {
        T parse(F input);
    }

    public interface FieldModifier<F, T> {
        String getField();
        T getValue(F input);
    }

}
