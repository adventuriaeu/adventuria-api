package de.fabtopf.contray.adventuria.api.utils.event.bukkit;

import de.fabtopf.contray.adventuria.api.utils.cloud.ChannelMessageDocument;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Getter
@AllArgsConstructor
public class ChannelMessageEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private String channel;
    private String message;
    private ChannelMessageDocument document;

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

}
