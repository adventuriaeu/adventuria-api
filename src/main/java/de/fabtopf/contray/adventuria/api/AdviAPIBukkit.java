package de.fabtopf.contray.adventuria.api;

import de.fabtopf.contray.adventuria.api.player.skin.SkinDataProvider;
import de.fabtopf.contray.adventuria.api.utils.bukkit.inventory.SimpleInventoryProvider;
import de.fabtopf.contray.adventuria.api.utils.bukkit.inventory.SimpleItemProvider;
import de.fabtopf.contray.adventuria.api.utils.economy.BankProvider;
import de.fabtopf.contray.adventuria.api.utils.economy.EconomyProvider;
import de.fabtopf.contray.adventuria.api.internal.AdviConfiguration;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.player.BukkitNetworkPlayerProvider;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayerProvider;
import de.fabtopf.contray.adventuria.api.utils.bukkit.chat.BukkitChatManager;
import de.fabtopf.contray.adventuria.api.utils.bukkit.scoreboard.ScoreboardManager;
import de.fabtopf.contray.adventuria.api.utils.cloud.ServerData;
import lombok.Getter;
import org.bukkit.Bukkit;

import java.io.File;
import java.util.UUID;

@Getter
public class AdviAPIBukkit extends AdviAPI {

    private boolean isLobby;
    private ServerData.SignState state;
    private NetworkPlayerProvider playerProvider;

    private ScoreboardManager scoreboardManager;
    private EconomyProvider economyProvider;
    private BankProvider bankProvider;
    private SkinDataProvider skinProvider;

    private SimpleInventoryProvider inventoryProvider;
    private SimpleItemProvider itemProvider;

    public AdviAPIBukkit(AdviConfiguration configuration) {
        super(configuration);

        this.state = ServerData.SignState.OFFLINE;
        this.playerProvider = new BukkitNetworkPlayerProvider();
        this.isLobby = (boolean) configuration.getAdditional().getOrDefault("lobby", false);

        this.scoreboardManager = new ScoreboardManager(BukkitCore.getInstance());
        this.chatManager = new BukkitChatManager(new File(BukkitCore.getInstance().getDataFolder(), "chat_settings.yml"));
        this.skinProvider = new SkinDataProvider();

        this.inventoryProvider = new SimpleInventoryProvider();
        this.itemProvider = new SimpleItemProvider();

        if((economyProvider = economyStorage.getNoCached(cloudUtilities.getServerName())) == null) {
            economyStorage.register(cloudUtilities.getServerName());
            economyProvider = economyStorage.get(cloudUtilities.getServerName());
        }

        bankProvider = new BankProvider();
    }

    public void setServerInfo(String motd) {
        this.setServerInfo(motd, Bukkit.getMaxPlayers());
    }

    public void setServerInfo(String motd, ServerData.SignState state) {
        this.setServerInfo(motd, Bukkit.getMaxPlayers(), state);
    }

    public void setServerInfo(String motd, int maxPlayers) {
        this.setServerInfo(motd, maxPlayers, this.state);
    }

    public void setServerInfo(String motd, int maxPlayers, ServerData.SignState state) {
        this.state = state;

        this.cloudUtilities.setMotd(motd);
        this.cloudUtilities.setServerState(state.getEquivalent());
        this.cloudUtilities.setMaxPlayers(maxPlayers);
        this.cloudUtilities.update();
    }

    @Override
    public boolean isOnline(UUID uniqueId) {
        return Bukkit.getPlayer(uniqueId) != null;
    }

    @Override
    public void cleanup() {
        super.cleanup();
    }

    public String getPackageVersion() {

        switch(Bukkit.getBukkitVersion()) {

            case "1.13.2-R0.1-SNAPSHOT":
                return "v1_13_R2";

            default:
                return "unknown";
        }

    }

}
