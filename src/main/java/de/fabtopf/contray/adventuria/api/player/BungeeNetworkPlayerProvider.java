package de.fabtopf.contray.adventuria.api.player;

import de.dytanic.cloudnet.lib.player.OfflinePlayer;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;

public class BungeeNetworkPlayerProvider extends NetworkPlayerProvider {

    public BungeeNetworkPlayerProvider() {
        super(new CacheCondition<NetworkPlayer, UUID>() {

            @Override
            public boolean checkCache(UUID uniqueId, NetworkPlayer networkPlayer) {
                return !AdviAPI.getInstance().isOnline(uniqueId);
            }

        });
    }

    public NetworkPlayer get(ProxiedPlayer player) {
        return this.get(player.getUniqueId());
    }

    public NetworkPlayer get(OfflinePlayer player) {
        return this.get(player.getUniqueId());
    }

}
