package de.fabtopf.contray.adventuria.api.utils.economy;

import com.arangodb.ArangoCursor;
import com.arangodb.util.MapBuilder;
import com.arangodb.velocypack.VPackSlice;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.utils.arango.CollectionManager;
import de.fabtopf.contray.adventuria.api.utils.cloud.ChannelMessageDocument;

import java.util.*;
import java.util.stream.Collectors;

public class BankProvider extends CollectionManager<BankAccount, UUID> {

    private Map<String, UUID> accountUUIDMap;

    public BankProvider() {
        super("economy_accounts", BankAccount::new, true, (key, obj) -> obj.getType() == BankAccount.Type.EXTERNAL ||
                obj.getOwners().stream().anyMatch(u -> AdviAPI.getInstance().getPlayerProvider().isOnline(u)) ||
                obj.getMembers().stream().anyMatch(u -> AdviAPI.getInstance().getPlayerProvider().isOnline(u)));

        this.accountUUIDMap = new HashMap<>();
    }

    public BankAccount get(String accountName) {

        UUID uniqueId;
        if((uniqueId = getUniqueId(accountName)) == null)
            return null;

        return this.get(uniqueId);
    }

    public String getName(UUID uniqueId) {
        String name = null;

        if(AdviAPI.getInstance().saveRedisRequest(redis -> redis.exists("advi-bank-" + uniqueId.toString()), false))
            name = AdviAPI.getInstance().saveRedisRequest(redis -> redis.get("advi-bank-" + uniqueId.toString()), null);

        if(name == null)
            return get(uniqueId).getName();

        return name;
    }

    public UUID getUniqueId(String accountName) {
        UUID uniqueId = null;

        if(accountUUIDMap.containsKey(accountName.toLowerCase()))
            uniqueId = accountUUIDMap.get(accountName.toLowerCase());

        if(uniqueId == null && AdviAPI.getInstance().saveRedisRequest(redis -> redis.exists("advi-bank-" + accountName.toLowerCase()), false))
            try {
                uniqueId = UUID.fromString(AdviAPI.getInstance().saveRedisRequest(redis -> redis.get("advi-bank-" + accountName.toLowerCase()), null));
            } catch (Exception ignored) {}

        if(uniqueId == null) {
            ArangoCursor<VPackSlice> cursor = AdviAPI.getInstance().getArangoDatabase().query(
                    "FOR account IN " + this.collection.name() + " FILTER LIKE(account.name, @account, true) " +
                            "RETURN {uniqueId: account._key}",
                    new MapBuilder().put("account", accountName).get(),
                    null,
                    VPackSlice.class);

            if(cursor.hasNext())
                uniqueId = UUID.fromString(cursor.next().get("uniqueId").getAsString());

            UUID uuid = uniqueId;
            if(uniqueId != null)
                AdviAPI.getInstance().saveRedisRequest(redis ->
                        redis.set("advi-bank-" + accountName.toLowerCase(), uuid.toString()), null);

            this.closeCursor(cursor);
        }

        return uniqueId;
    }

    public List<UUID> getAccountIds(BankAccount.Type type, boolean fetch) {
        if(!fetch)
            return cache.values().stream().filter(account -> account.getType().equals(type)).map(BankAccount::getUniqueId).collect(Collectors.toList());

        ArangoCursor<VPackSlice> cursor = AdviAPI.getInstance().getArangoDatabase().query(
                "FOR account IN " + this.collection.name() + " FILTER LIKE(account.type, @type, true) " +
                        "RETURN {uniqueId: account._key}",
                new MapBuilder().put("type", type).get(),
                null,
                VPackSlice.class);

        List<UUID> accountIds = new ArrayList<>();
        while(cursor.hasNext())
            accountIds.add(UUID.fromString(cursor.next().get("uniqueId").getAsString()));

        this.closeCursor(cursor);

        return accountIds;
    }

    public void update(BankAccount account) {
        this.save(account);
        this.uncache(account.getKey());

        AdviAPI.getInstance().getCloudUtilities().sendServerChannelMessage("advi-economy", "update",
                new ChannelMessageDocument(new Document()
                        .append("uniqueId", account.getUniqueId())));
    }

    @Override
    public void uncache(UUID key) {
        super.uncache(key);

        this.accountUUIDMap.entrySet().removeIf(e -> e.getValue().equals(key));
    }
}
