package de.fabtopf.contray.adventuria.api.internal.bukkit.command;

import com.arangodb.util.MapBuilder;
import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.lib.player.CloudPlayer;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.player.PlayerProperty;
import de.fabtopf.contray.adventuria.api.utils.bukkit.chat.ChatChannel;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommand;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommandHandler;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleFunctionCommandExecutor;
import de.fabtopf.contray.adventuria.api.utils.event.bukkit.AdviChatEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.*;
import java.util.stream.Collectors;

@SimpleCommandHandler(name = "message", permission = "adventuria.api.command.message.use", aliases = { "msg", "m", "reply", "r" })
public class MessageCommand implements SimpleFunctionCommandExecutor, Listener {

    private ChatChannel channel;

    public MessageCommand() {
        Bukkit.getPluginManager().registerEvents(this, BukkitCore.getInstance());

        channel = new ChatChannel("privatemessage", null, "private-message-channel", "@", "Privat",
                "adventuria.api.command.message.use", null, "adventuria.api.command.message.spy",
                null, false, 0, ChatChannel.Type.CUSTOM) ;

        AdviAPI.getInstance().getChatManager().registerChannel(channel);
    }

    @Override
    public String getUsage() {
        return "§cUsage: §7/m <Spieler> <Nachricht> | /r <Nachricht>";
    }

    @Override
    public String getPermissionMessage() {
        return BukkitCore.getInstance().getLanguage().getPhraseContent("command-no-perm");
    }

    @Override
    public String getDescription() {
        return "Befehl zum versenden von privaten Nachrichten.";
    }

    @Override
    public void sendHelpMessage(CommandSender sender, SimpleCommand command) {
        sender.sendMessage(this.getUsage());
    }

    @Override
    public void onExecute(CommandSender sender, SimpleCommand command, String label, String[] args) {

        if(!sender.hasPermission(channel.getWritePermission())) {
            sender.sendMessage(command.getPermissionMessage());
            return;
        }

        Map<String, Object> info = new HashMap<>();
        info.put("server", AdviAPI.getInstance().getServerName());

        String[] subLabel = label.toLowerCase().split(":");
        switch(subLabel[subLabel.length - 1]) {

            case "m":
            case "msg":
            case "message":
                if(args.length < 2)
                    sender.sendMessage(command.getUsage());
                else
                    AdviAPI.getInstance().getChatManager().invokeChatMessage(
                            sender instanceof Player ? AdviAPI.getInstance().getPlayerProvider().get(((Player) sender).getUniqueId()) : null,
                            "@" + String.join(" ", args), channel.getName(), true, info);
               break;

            case "r":
            case "reply":
                if(args.length < 1)
                    sender.sendMessage(command.getUsage());
                else
                    AdviAPI.getInstance().getChatManager().invokeChatMessage(
                            sender instanceof Player ? AdviAPI.getInstance().getPlayerProvider().get(((Player) sender).getUniqueId()) : null,
                            "@ " + String.join(" ", args), channel.getName(), true, info);

                break;

        }

    }

    @Override
    public List<String> onTabComplete(CommandSender sender, SimpleCommand command, String label, String[] args, Location location) {
        if(args.length == 1 && !(label.split(" ")[label.split(" ").length - 1].equalsIgnoreCase("r") ||
                label.split(" ")[label.split(" ").length - 1].equalsIgnoreCase("reply")))
            return CloudAPI.getInstance().getOnlinePlayers().stream().map(CloudPlayer::getName)
                    .filter(s -> !s.equals(sender.getName()) && s.toLowerCase().startsWith(args[0].toLowerCase())).collect(Collectors.toList());

        return new ArrayList<>();
    }

    @EventHandler
    public void onPrivateMessage(AdviChatEvent event) {

        ChatChannel channel = this.channel;
        if(!event.getChannel().equals(channel.getName()))
            return;

        event.setCancelled(true);

        String computedMessage;
        if(event.getMessage().startsWith(channel.getPrefix())) {

            if(!event.getMessage().split(" ")[0].equals(channel.getPrefix())) {
                computedMessage = computeMessage(event.getInvoker().getUniqueId(), event.getMessage().replaceFirst(channel.getPrefix(), ""));
            } else {

                UUID latestMessageTarget;
                if (event.getInvoker() != null && AdviAPI.getInstance().saveRedisRequest(redis -> redis.exists("latest-private-message-target-" + event.getInvoker().getName().toLowerCase()), false)) {
                    latestMessageTarget = UUID.fromString(AdviAPI.getInstance().saveRedisRequest(redis -> redis.get("latest-private-message-target-" + event.getInvoker().getName().toLowerCase()), null));
                } else if(event.getInvoker() == null && AdviAPI.getInstance().saveRedisRequest(redis -> redis.exists("latest-private-message-target-" + AdviAPI.getInstance().getServerName() + "-console"), false)) {
                    latestMessageTarget = UUID.fromString(AdviAPI.getInstance().saveRedisRequest(redis -> redis.get("latest-private-message-target-" + AdviAPI.getInstance().getServerName() + "-console"), null));
                } else {

                    Player player;
                    if(event.getInvoker() != null && (player = Bukkit.getPlayer(event.getInvoker().getUniqueId())) != null)
                        player.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-private-message-no-saved-target"));
                    else
                        Bukkit.getLogger().warning(BukkitCore.getInstance().getLanguage().getPhraseContent("command-private-message-no-saved-target"));

                    event.setCancelled(true);
                    return;
                }

                computedMessage = computeMessage(event.getInvoker().getUniqueId(), latestMessageTarget, event.getMessage().replaceFirst(channel.getPrefix() + " ", ""));

            }

        } else {
            computedMessage = event.getMessage();
        }

        if(computedMessage == null)
            return;

        event.setMessage(computedMessage);
        Map<String, Object> content = BukkitCore.getInstance().getGson().fromJson(computedMessage, Map.class);

        NetworkPlayer invoker = content.containsKey("invoker") ? AdviAPI.getInstance().getPlayerProvider().get(UUID.fromString((String) content.get("invoker"))) : null;
        NetworkPlayer target = AdviAPI.getInstance().getPlayerProvider().get(UUID.fromString((String) content.get("target")));
        String message = (String) content.get("message");
        String server = (String) content.get("server");

        if(invoker != null) {
            if (invoker.isOnline()) {
                Bukkit.getPlayer(invoker.getUniqueId()).sendMessage(AdviAPI.getInstance().getChatManager()
                        .convertChannelFormat(target.getUniqueId(), invoker, message, BukkitCore.getInstance().getLanguage().getPhraseContent("command-private-message-from-format"),
                                channel.getName(), channel.getDisplay()));

                Bukkit.getLogger().info("[Chat-private] " + AdviAPI.getInstance().getChatManager().convertChannelFormat(target.getUniqueId(),
                        invoker, message, BukkitCore.getInstance().getLanguage().getPhraseContent("command-private-message-spy-format"), channel.getName(), channel.getDisplay()));
            }
        } else {
            if(server.equals(AdviAPI.getInstance().getServerName()))
                Bukkit.getLogger().info(AdviAPI.getInstance().getChatManager().convertChannelFormat(target.getUniqueId(), invoker, message,
                        BukkitCore.getInstance().getLanguage().getPhraseContent("command-private-message-from-format"), channel.getName(), channel.getDisplay()));
        }

        if(target.isOnline()) {
            Bukkit.getPlayer(target.getUniqueId()).sendMessage(AdviAPI.getInstance().getChatManager()
                    .convertChannelFormat(target.getUniqueId(), invoker, message, BukkitCore.getInstance().getLanguage().getPhraseContent("command-private-message-to-format"),
                            channel.getName(), channel.getDisplay()));

            if(invoker != null)
                AdviAPI.getInstance().saveRedisRequest(redis -> redis.set("latest-private-message-target-" + target.getName().toLowerCase(), invoker.getUniqueId().toString()), null);

            if(!server.equals(AdviAPI.getInstance().getServerName()))
                Bukkit.getLogger().info("[Chat-private] " + AdviAPI.getInstance().getChatManager().convertChannelFormat(target.getUniqueId(),
                        invoker, message, BukkitCore.getInstance().getLanguage().getPhraseContent("command-private-message-spy-format"), channel.getName(), channel.getDisplay()));
        }

        for(Player player : Bukkit.getOnlinePlayers()) {
            NetworkPlayer p = AdviAPI.getInstance().getPlayerProvider().get(player.getUniqueId());
            if(p.getPermissionUser().hasPermission(channel.getSpyPermission()) &&
                    p.getProperty(PlayerProperty.SPY_CHAT_CHANNEL).contains(channel) &&
                    !(player.getUniqueId().equals(target.getUniqueId()) || (invoker != null && p.getUniqueId().equals(invoker.getUniqueId()))))
                player.sendMessage(AdviAPI.getInstance().getChatManager().convertChannelFormat(target.getUniqueId(),
                        invoker, message, BukkitCore.getInstance().getLanguage().getPhraseContent("command-private-message-spy-format"), channel.getName(), channel.getDisplay()));
        }

    }

    private String computeMessage(UUID invoker, String message) {
        String[] parts = message.split(" ");

        UUID target;
        if((target = AdviAPI.getInstance().getPlayerProvider().getUniqueId(parts[0])) == null){

            if(invoker != null) {

                Player player;
                if((player = Bukkit.getPlayer(invoker)) != null)
                    player.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-private-message-player-not-registered", parts[0]));

            } else {
                Bukkit.getLogger().warning(BukkitCore.getInstance().getLanguage().getPhraseContent("command-private-message-player-not-registered", parts[0]));
            }

            return null;
        }

        if (invoker != null) {
            String invokerName;
            if((invokerName = AdviAPI.getInstance().getPlayerProvider().getName(invoker)) == null)
                return null;

            AdviAPI.getInstance().saveRedisRequest(redis -> redis.set("latest-private-message-target-" + invokerName.toLowerCase(), target.toString()), null);
        } else {
            AdviAPI.getInstance().saveRedisRequest(redis -> redis.set("latest-private-message-target-" + AdviAPI.getInstance().getServerName() + "-console", target.toString()), null);
        }

        return computeMessage(invoker, target, message.replaceFirst(parts[0] + " ", ""));
    }

    private String computeMessage(UUID invoker, UUID target, String message) {

        if(!AdviAPI.getInstance().getCloudUtilities().isOnline(target)) {

            if(invoker != null) {

                Player player;
                if((player = Bukkit.getPlayer(invoker)) != null)
                    player.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-private-message-player-not-online",
                            AdviAPI.getInstance().getPlayerProvider().getName(target)));

            } else {
                Bukkit.getLogger().warning(BukkitCore.getInstance().getLanguage().getPhraseContent("command-private-message-player-not-online",
                        AdviAPI.getInstance().getPlayerProvider().getName(target)));
            }

            return null;
        }

        return BukkitCore.getInstance().getGson().toJson(new MapBuilder()
                .put("invoker", invoker)
                .put("target", target)
                .put("message", message)
                .put("server", AdviAPI.getInstance().getServerName())
                .get());
    }

}
