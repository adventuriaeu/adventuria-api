package de.fabtopf.contray.adventuria.api.utils.feature;

import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class TimedCommandManager {

    protected List<TimedCommand> commands;

    protected File file;

    protected Plugin plugin;

    public TimedCommandManager(File config, Plugin plugin) {
        this.file = config;
        this.plugin = plugin;
        this.commands = new ArrayList<>();

        this.reload();
    }

    public TimedCommandManager(Plugin plugin) {
        this(new File(plugin.getDataFolder(), "scheduledCommands.yml"), plugin);
    }

    public void reload() {
        stopScheduler();
        readCommands();

        if(!commands.isEmpty())
            runScheduler();
    }

    protected abstract void runScheduler();

    protected abstract void stopScheduler();

    protected abstract String replaceCommandValues(String command);

    protected abstract void readCommands();

    protected class CommandRunnable implements Runnable {

        private long cycle;

        public CommandRunnable() {
            this.cycle = 0L;
        }

        @Override
        public void run() {
            cycle++;

            for(TimedCommand command : commands)
                if(cycle - command.getDelay() >= 0 &&
                        (cycle - command.getDelay()) % command.getInterval() == 0)
                    command.execute(replaceCommandValues(command.getCommand()));

        }

    }

}
