package de.fabtopf.contray.adventuria.api.utils.vault;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.utils.economy.BankAccount;
import de.fabtopf.contray.adventuria.api.utils.economy.Currency;
import de.fabtopf.contray.adventuria.api.utils.economy.CurrencyProvider;
import de.fabtopf.contray.adventuria.api.utils.economy.EconomyProvider;
import net.milkbowl.vault.economy.AbstractEconomy;
import net.milkbowl.vault.economy.EconomyResponse;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Collectors;

public class VaultEconomyImpl extends AbstractEconomy {

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getName() {
        return "AdventuriaAPI-Economy";
    }

    @Override
    public boolean hasBankSupport() {
        return true;
    }

    @Override
    public int fractionalDigits() {
        return 2;
    }

    @Override
    public String format(double value) {
        return NumberFormat.getCurrencyInstance(Locale.GERMANY).format(value).split(" ")[0];
    }

    @Override
    public String currencyNamePlural() {
        return getMainCurrency().getName() + "s";
    }

    @Override
    public String currencyNameSingular() {
        return getMainCurrency().getName();
    }

    @Override
    public boolean hasAccount(String playerName) {
        return hasAccount(playerName, null);
    }

    @Override
    public boolean hasAccount(String playerName, String world) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("hasAccount => " + playerName + " | " + world);

        return getPlayerUniqueId(playerName) != null || getAccountUniqueId(playerName) != null;
    }

    @Override
    public double getBalance(String playerName) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("hasAccount => " + playerName);

        return getBalance(playerName, null);
    }

    @Override
    public double getBalance(String playerName, String world) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("getBalance => " + playerName + " | " + world);

        UUID playerUniqueId;
        UUID accountUniqueId = null;
        if((playerUniqueId = getPlayerUniqueId(playerName)) == null && (accountUniqueId = getAccountUniqueId(playerName)) == null)
            return 0;

        Currency currency;
        if((currency = getMainCurrency()) == null)
            return 0;

        if(playerUniqueId != null)
            return ((Long) getPlayer(playerUniqueId).balanceMoney(currency.getKey())).doubleValue() / 100;

        return ((Long) getAccount(accountUniqueId).balanceMoney(currency.getKey())).doubleValue() / 100;
    }

    @Override
    public boolean has(String playerName, double value) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("has => " + playerName + " | " + value);

        return getBalance(playerName) >= value;
    }

    @Override
    public boolean has(String playerName, String world, double value) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("getBalance => " + playerName + " | " + world + " | " + value);

        return getBalance(playerName, world) >= value;
    }

    @Override
    public EconomyResponse withdrawPlayer(String playerName, double value) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("withdrawPlayer => " + playerName + " | " + value);

        return withdrawPlayer(playerName, null, value);
    }

    @Override
    public EconomyResponse withdrawPlayer(String playerName, String world, double value) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("withdrawPlayer => " + playerName + " | " + world + " | " + value);

        UUID playerUniqueId;
        UUID accountUniqueId = null;
        if((playerUniqueId = getPlayerUniqueId(playerName)) == null && (accountUniqueId = getAccountUniqueId(playerName)) == null)
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Account not found!");

        if(playerUniqueId != null)
            return new EconomyResponse(-value, getPlayer(playerUniqueId).withdrawMoney(getMainCurrency().getKey(), ((Double) (value * 100)).longValue()), EconomyResponse.ResponseType.SUCCESS, "");

        return new EconomyResponse(-value, getAccount(accountUniqueId).withdrawMoney(getMainCurrency().getKey(), ((Double) (value * 100)).longValue()), EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public EconomyResponse depositPlayer(String playerName, double value) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("depositPlayer => " + playerName + " | " + value);

        return depositPlayer(playerName, null, value);
    }

    @Override
    public EconomyResponse depositPlayer(String playerName, String world, double value) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("depositPlayer => " + playerName + " | " + world + " | " + value);

        UUID playerUniqueId;
        UUID accountUniqueId = null;
        if((playerUniqueId = getPlayerUniqueId(playerName)) == null && (accountUniqueId = getAccountUniqueId(playerName)) == null)
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Account not found!");

        if(AdviAPI.getInstance().isDevMode())
            System.out.println("Account found => " + playerName);

        if(playerUniqueId != null)
            return new EconomyResponse(value, getPlayer(playerUniqueId).depositMoney(getMainCurrency().getKey(), ((Double) (value * 100)).longValue()), EconomyResponse.ResponseType.SUCCESS, "");

        if(AdviAPI.getInstance().isDevMode())
            System.out.println("Adding money => " + value);

        return new EconomyResponse(value, getAccount(accountUniqueId).depositMoney(getMainCurrency().getKey(), ((Double) (value * 100)).longValue()), EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public EconomyResponse createBank(String accountName, String playerName) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("createBank => " + accountName + " | " + playerName);

        if(getAccountUniqueId(accountName) != null)
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Account already exists!");

        UUID player;
        if((player = getPlayerUniqueId(playerName)) == null)
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Player not found!");

        UUID currency;
        if((currency = AdviAPI.getInstance().bukkit().getEconomyProvider().getMainCurrency()) == null)
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Currency not found!");

        UUID uniqueId = UUID.randomUUID();
        AdviAPI.getInstance().bukkit().getBankProvider().register(uniqueId);
        BankAccount account = AdviAPI.getInstance().bukkit().getBankProvider().get(uniqueId);
        account.setName(accountName);
        account.setType(BankAccount.Type.BANK);
        account.setCurrency(currency);
        account.getOwners().add(player);

        AdviAPI.getInstance().bukkit().getBankProvider().update(account);

        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.SUCCESS, "Account created!");
    }

    @Override
    public EconomyResponse deleteBank(String accountName) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("deleteBank => " + accountName);

        UUID uniqueId;
        if((uniqueId = getAccountUniqueId(accountName)) == null)
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Account doesn't exist!");

        BankAccount account = getAccount(uniqueId);
        AdviAPI.getInstance().bukkit().getBankProvider().delete(uniqueId);
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.SUCCESS, "Account deleted!");
    }

    @Override
    public EconomyResponse bankBalance(String accountName) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("bankBalance => " + accountName);

        UUID uniqueId;
        if((uniqueId = getAccountUniqueId(accountName)) == null)
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Account doesn't exist!");

        BankAccount account = getAccount(uniqueId);
        return new EconomyResponse(0, account.getBalance(), EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public EconomyResponse bankHas(String accountName, double value) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("bankHas => " + accountName + " | " + value);

        UUID uniqueId;
        if((uniqueId = getAccountUniqueId(accountName)) == null)
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Account doesn't exist!");

        BankAccount account = getAccount(uniqueId);
        if(account.getBalance() < value)
            return new EconomyResponse(0, account.getBalance(), EconomyResponse.ResponseType.FAILURE, "Not enough money!");

        return new EconomyResponse(0, account.getBalance(), EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public EconomyResponse bankWithdraw(String accountName, double value) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("bankWithdraw => " + accountName + " | " + value);

        UUID uniqueId;
        if((uniqueId = getAccountUniqueId(accountName)) == null)
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Account doesn't exist!");

        BankAccount account = getAccount(uniqueId);
        return new EconomyResponse(-value, account.withdrawMoney(null, (long) (value * 100)), EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public EconomyResponse bankDeposit(String accountName, double value) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("bankDeposit => " + accountName + " | " + value);

        UUID uniqueId;
        if((uniqueId = getAccountUniqueId(accountName)) == null)
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Account doesn't exist!");

        BankAccount account = getAccount(uniqueId);
        return new EconomyResponse(value, account.depositMoney(null, (long) (value * 100)), EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public EconomyResponse isBankOwner(String accountName, String playerName) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("isBankOwner => " + accountName + " | " + playerName);

        UUID uniqueId;
        if((uniqueId = getAccountUniqueId(accountName)) == null)
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Account doesn't exist!");

        BankAccount account = getAccount(uniqueId);
        if(account.getOwners().stream().noneMatch(owner -> playerName.equalsIgnoreCase(AdviAPI.getInstance().getPlayerProvider().getName(owner))))
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Not an owner!");

        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public EconomyResponse isBankMember(String accountName, String playerName) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("isBankMember => " + accountName + " | " + playerName);

        UUID uniqueId;
        if((uniqueId = getAccountUniqueId(accountName)) == null)
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Account doesn't exist!");

        BankAccount account = getAccount(uniqueId);
        if(account.getMembers().stream().noneMatch(owner -> playerName.equalsIgnoreCase(AdviAPI.getInstance().getPlayerProvider().getName(owner))))
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Not an owner!");

        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public List<String> getBanks() {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("getBanks");

        return AdviAPI.getInstance().bukkit().getBankProvider().getAccountIds(BankAccount.Type.BANK, true).stream().map(UUID::toString).collect(Collectors.toList());
    }

    @Override
    public boolean createPlayerAccount(String playerName) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("createPlayerAccount => " + playerName);

        return createPlayerAccount(playerName, null);
    }

    @Override
    public boolean createPlayerAccount(String playerName, String world) {
        if(AdviAPI.getInstance().isDevMode())
            System.out.println("createPlayerAccount => " + playerName + " | " + world);

        if(AdviAPI.getInstance().bukkit().getBankProvider().getUniqueId(playerName) != null)
            return false;

        UUID currency;
        if((currency = AdviAPI.getInstance().bukkit().getEconomyProvider().getMainCurrency()) == null)
            return false;

        UUID uniqueId = UUID.randomUUID();
        AdviAPI.getInstance().bukkit().getBankProvider().register(uniqueId);
        BankAccount account = AdviAPI.getInstance().bukkit().getBankProvider().get(uniqueId);
        account.setName(playerName);
        account.setType(BankAccount.Type.EXTERNAL);
        account.setCurrency(currency);

        AdviAPI.getInstance().bukkit().getBankProvider().update(account);
        return true;
    }

    private CurrencyProvider getCurrencyProvider() {
        return EconomyProvider.getCurrencyProvider();
    }

    private Currency getMainCurrency() {
        return getCurrencyProvider().get(AdviAPI.getInstance().getEconomyProvider(AdviAPI.getInstance().getServerName()).getMainCurrency());
    }

    private UUID getPlayerUniqueId(String playerName) {
        if(playerName == null)
            return null;

        return AdviAPI.getInstance().getPlayerProvider().getUniqueId(playerName);
    }

    private NetworkPlayer getPlayer(UUID uniqueId) {
        if(uniqueId == null)
            return null;

        return AdviAPI.getInstance().getPlayerProvider().get(uniqueId);
    }

    private UUID getAccountUniqueId(String accountName) {
        if(accountName == null)
            return null;

        return AdviAPI.getInstance().bukkit().getBankProvider().getUniqueId(accountName);
    }

    private BankAccount getAccount(UUID uniqueId) {
        if(uniqueId == null)
            return null;

        return AdviAPI.getInstance().bukkit().getBankProvider().get(uniqueId);
    }
}
