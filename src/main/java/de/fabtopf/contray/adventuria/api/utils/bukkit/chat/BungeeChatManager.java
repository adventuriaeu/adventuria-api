package de.fabtopf.contray.adventuria.api.utils.bukkit.chat;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.internal.bungee.BungeeCore;
import de.fabtopf.contray.adventuria.api.internal.bungee.command.AdviCommand;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BungeeChatManager extends ChatManager {

    public BungeeChatManager(File configFile) {
        super(configFile);
    }

    @Override
    protected void readChannels() {
        {

            try {

                Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);

                for(String s : configuration.getKeys()) {
                    String name = configuration.getString(s + ".name");
                    String format = configuration.getString(s + ".format");
                    String prefix = configuration.getString(s + ".prefix");
                    String display = configuration.getString(s + ".display");
                    String writePermission = configuration.getString(s + ".write_permission");
                    String readPermission = configuration.getString(s + ".read_permission");
                    String spyPermission = configuration.getString(s + ".spy_permission");
                    String communiationChannel = configuration.getString(s + ".communication_channel");
                    String command = configuration.getString(s + ".command");
                    boolean standard = configuration.getBoolean(s + ".default");
                    int range = configuration.getInt(s + ".range");
                    String typeName = configuration.getString(s + ".type");

                    ChatChannel.Type type;
                    if((type = ChatChannel.Type.valueOf(typeName.toUpperCase())) == null) {
                        BungeeCord.getInstance().getLogger().warning("ChatChannel " + name + " could not be registered: invalid channel mode");
                        continue;
                    }

                    registerChannel(name, format, communiationChannel, prefix, display, writePermission, readPermission, spyPermission, command, standard, range, type);

                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public boolean registerChannel(String name, String format, String communicationChannel, String prefix, String display, String writePermission, String readPermission, String spyPermission, String command, boolean standard, int range, ChatChannel.Type type) {

        if(name == null || format == null || display == null || command == null || type == null ||
                (prefix == null && type == ChatChannel.Type.SERVER)) {
            BungeeCord.getInstance().getLogger().warning("ChatChannel " + name + " could not be registered: missing information");
            return false;
        }

        ChatChannel channel = new ChatChannel(
                name,
                format,
                communicationChannel != null && communicationChannel.equals("") ? null : communicationChannel,
                prefix,
                display,
                writePermission != null && writePermission.equals("") ? null : writePermission,
                readPermission != null && readPermission.equals("") ? null : readPermission,
                spyPermission != null && spyPermission.equals("") ? null : spyPermission,
                command,
                standard,
                range,
                type);

        if(channels.contains(channel)) {
            BungeeCord.getInstance().getLogger().warning("ChatChannel " + name + " could not be registered: name already registered");
            return false;
        }

        channels.add(channel);

        if(type != ChatChannel.Type.CUSTOM) {
            channel.setCommandObj(new BungeeChatCommand(name));
            BungeeCord.getInstance().getPluginManager().registerCommand(BungeeCore.getInstance(), (BungeeChatCommand) channel.getCommandObj());
        }

        BungeeCord.getInstance().getLogger().info("ChatChannel " + name + " is now registered");

        return true;
    }

    @Override
    public boolean unregisterChannel(String channel)  {

        ChatChannel c;
        if((c = getChannel(channel)) == null) {
            BungeeCord.getInstance().getLogger().warning("ChatChannel " + channel + " could not be unregistered: channel doesn't exist");
            return false;
        }

        if(c.equals(getDefaultChannel())) {
            BungeeCord.getInstance().getLogger().warning("ChatChannel " + channel + " could not be unregistered: channel is default");
            return false;
        }

        channels.remove(c);

        BungeeCord.getInstance().getPluginManager().unregisterCommand((Command) c.getCommandObj());
        BungeeCord.getInstance().getLogger().warning("ChatChannel " + channel + " unregistered");

        return true;
    }

    private static class BungeeChatCommand extends AdviCommand {

        BungeeChatCommand(String name) {
            super(name);
        }

        @Override
        public void execute(CommandSender commandSender, String[] strings) {
            ChatManager manager = AdviAPI.getInstance().getChatManager();
            ChatChannel channel = null;
            for(ChatChannel c : manager.getChannels())
                if(c.getCommand().equalsIgnoreCase(getName()))
                    channel = c;

            if(channel == null)
                return;

            if(channel.getType() == ChatChannel.Type.SERVER) {

                if(commandSender instanceof ProxiedPlayer)
                    ((ProxiedPlayer) commandSender).chat(channel.getPrefix() + String.join(" ", strings));
                else
                    BungeeCord.getInstance().broadcast(TextComponent.fromLegacyText(AdviAPI.getInstance().getChatManager().convertChannelFormat(null, null, String.join(" ", strings), channel, true, new HashMap<>())));

            } else {

                Map<String, Object> info = new HashMap<>();

                info.put("server", AdviAPI.getInstance().getServerName());

                if(commandSender instanceof ProxiedPlayer)
                    manager.invokeChatMessage(AdviAPI.getInstance().getPlayerProvider().get(((ProxiedPlayer) commandSender).getUniqueId()), String.join(" ", strings), channel.getName(), true, info);
                else
                    manager.invokeChatMessage(null, String.join(" ", strings), channel.getName(), true, info);

            }



        }

    }

    @Override
    public boolean invokeChatMessage(NetworkPlayer invoker, String message, String channel, boolean command, boolean externalTrigger, Map<String, Object> info) {
        // TODO Proxyevent
        return false;
    }

    @Override
    protected void sendChatMessage(UUID receiver, NetworkPlayer invoker, String message, ChatChannel channel, boolean command, boolean spy, Map<String, Object> info) {
        ProxiedPlayer player;
        if((player = BungeeCord.getInstance().getPlayer(receiver)) == null)
            return;

        player.sendMessage(TextComponent.fromLegacyText((spy ? BukkitCore.getInstance().getLanguage().getPhraseContent("system-chat-spy-prefix") : "") +
                convertChannelFormat(receiver, invoker, message, channel, command, info)));
    }

}
