package de.fabtopf.contray.adventuria.api.internal.bungee.listener;

import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.bridge.internal.util.CloudPlayerCommandSender;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bungee.BungeeCore;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.player.PlayerProperty;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.util.Map;
import java.util.UUID;

public class ProxiedPlayerListener implements Listener {

    @EventHandler(priority = EventPriority.LOW)
    public void onLogin(LoginEvent event) {

        NetworkPlayer player;
        if((player = AdviAPI.getInstance().getPlayerProvider().handleJoin(event.getConnection().getUniqueId())) == null)
            player = AdviAPI.getInstance().getPlayerProvider().get(event.getConnection().getUniqueId());

        player.updateProperty$(PlayerProperty.LAST_CONNECTION, System.currentTimeMillis());

        if (player.getProperty(PlayerProperty.FIRST_CONNECTION).equals(PlayerProperty.FIRST_CONNECTION.getDefaultOutputValue()))
            player.updateProperty$(PlayerProperty.FIRST_CONNECTION, System.currentTimeMillis());
    }

    @EventHandler
    public void onPostLogin(PostLoginEvent event) {
        AdviAPI.getInstance().saveRedisRequest(redis -> redis.set(
                "advi-player-" + event.getPlayer().getName().toLowerCase(),
                event.getPlayer().getUniqueId().toString()), null);
        AdviAPI.getInstance().saveRedisRequest(redis -> redis.set(
                "advi-player-" + event.getPlayer().getUniqueId().toString(),
                event.getPlayer().getName()), null);
    }

    @EventHandler
    public void onQuit(PlayerDisconnectEvent event) {
        handleQuit(event.getPlayer().getUniqueId());
    }

    public static void handleQuit(UUID uniqueId) {
        AdviAPI.getInstance().getPlayerProvider().uncache(uniqueId);
    }

    @EventHandler
    public void onCommand(ChatEvent event) {

        if(event.getSender() instanceof ProxiedPlayer) {
            if (BungeeCore.getInstance().isUseAuthMe() && event.getMessage().startsWith("/")) {

                String command = event.getMessage();
                if(command.startsWith("/authme:") || command.startsWith("/login") || command.startsWith("/register") || command.startsWith("/logout"))
                    return;

                ProxiedPlayer player = (ProxiedPlayer) event.getSender();
                if(!AdviAPI.getInstance().saveRedisRequest(redis -> redis.exists("adviapi-authmebridge-" + player.getUniqueId().toString()), false)) {
                    event.setCancelled(true);
                    player.sendMessage(TextComponent.fromLegacyText(BungeeCore.getInstance().getLanguage().getPhraseContent("system-integration-authme-notloggedin")));
                    return;
                }

                String authmeInfo = AdviAPI.getInstance().saveRedisRequest(redis ->
                        AdviAPI.getInstance().getRedisClient().get("adviapi-authmebridge-" + player.getUniqueId().toString()), null);
                Map<String, Object> authInfo = (Map<String, Object>) BungeeCore.getInstance().getGson().fromJson(authmeInfo, Map.class);

                if(player.getAddress().getAddress().getHostAddress().equals(authInfo.get("address")) &&
                        Double.doubleToLongBits((double) authInfo.get("timestamp")) > System.currentTimeMillis() - 86400L)
                    return;

                event.setCancelled(true);
                player.sendMessage(TextComponent.fromLegacyText(BungeeCore.getInstance().getLanguage().getPhraseContent("system-integration-authme-notloggedin")));
            }
        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void handlePermissionCheck(PermissionCheckEvent e) {

        if (e.getSender() instanceof ProxiedPlayer) {

            NetworkPlayer player;
            if((player = AdviAPI.getInstance().getPlayerProvider().get(((ProxiedPlayer) e.getSender()).getUniqueId())) == null) {
                e.setHasPermission(false);
                return;
            }

            e.setHasPermission(player.getPermissionUser().hasPermission(e.getPermission(), CloudAPI.getInstance().getGroup()));
        } else if (e.getSender() instanceof CloudPlayerCommandSender) {

            NetworkPlayer player;
            if((player = AdviAPI.getInstance().getPlayerProvider().get(e.getSender().getName())) == null) {
                e.setHasPermission(false);
                return;
            }

            e.setHasPermission(player.getPermissionUser().hasPermission(e.getPermission(), CloudAPI.getInstance().getGroup()));
        }
    }

}
