package de.fabtopf.contray.adventuria.api.permission;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.utils.arango.parser.Serializable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class PermissionUser implements Serializable {

    private transient NetworkPlayer player;

    private String prefix;
    private String suffix;
    private String display;

    private int joinPower;

    private List<PermissionGroupEntity> currentGroups;

    private Map<String, Boolean> globalPermissions;
    private Map<String, Map<String, Boolean>> serverGroupPermissions;
    private Map<String, Map<String, Map<String, Boolean>>> worldPermissions;

    @Setter
    private boolean override;

    private Map<String, Boolean> cachedGlobalPermissions;
    private Map<String, Map<String, Boolean>> cachedServerGroupPermissions;
    private Map<String, Map<String, Map<String, Boolean>>> cachedWorldPermissions;

    public PermissionUser(NetworkPlayer player, Map<String, Object> data) {
        this.player = player;
        this.cachedServerGroupPermissions = new HashMap<>();
        this.cachedWorldPermissions = new HashMap<>();
        this.override = false;
        deserialize(data);
    }

    public void clearCachedPermissions() {
        this.cachedGlobalPermissions = null;
        this.cachedServerGroupPermissions.clear();
        this.cachedWorldPermissions.clear();
    }

    public String getPrefix() {
        return prefix != null ? prefix : getCurrentGroups().get(0).getPrefix();
    }

    public String getPrefix(String serverGroup) {
        return prefix != null ? prefix : getCurrentGroups(serverGroup.toLowerCase()).get(0).getPrefix();
    }

    public String getSuffix() {
        return suffix != null ? suffix : getCurrentGroups().get(0).getSuffix();
    }

    public String getSuffix(String serverGroup) {
        return suffix != null ? suffix : getCurrentGroups(serverGroup.toLowerCase()).get(0).getSuffix();
    }

    public String getDisplay() {
        return display != null ? display : getCurrentGroups().get(0).getDisplay();
    }

    public String getDisplay(String serverGroup) {
        return display != null ? display : getCurrentGroups(serverGroup.toLowerCase()).get(0).getDisplay();
    }

    public void removeInvalidGroups() {
        this.currentGroups = this.currentGroups.stream().map(s -> {
            PermissionGroupEntity entity = s;
            while(entity != null && (entity.isExpired() || AdviAPI.getInstance().getPermissionPool().get(entity.getPermissionGroup()) == null))
                entity = entity.getFallback() != null ? new PermissionGroupEntity(entity.getFallback(), entity) : null;

            return entity;
        }).filter(Objects::nonNull).collect(Collectors.toList());

        if(this.currentGroups.isEmpty())
            this.currentGroups.add(new PermissionGroupEntity(AdviAPI.getInstance().getPermissionPool().getDefaultGroup().getKey(), System.currentTimeMillis(), 1, null));
    }

    public List<PermissionGroup> getCurrentGroups() {
        removeInvalidGroups();

        List<PermissionGroup> groups = currentGroups.stream().map(g -> getPermissionPool().get(g.getPermissionGroup()))
                .filter(Objects::nonNull).filter(g -> g.getAvailableOn().isEmpty()).sorted().collect(Collectors.toList());

        if(groups.isEmpty())
            groups.add(getPermissionPool().getDefaultGroup());

        return groups;
    }

    public List<PermissionGroup> getCurrentGroups(String serverGroup) {
        removeInvalidGroups();

        List<PermissionGroup> groups = currentGroups.stream().map(g -> getPermissionPool().get(g.getPermissionGroup()))
                .filter(Objects::nonNull).filter(g -> g.getAvailableOn().isEmpty() || g.getAvailableOn().contains(serverGroup.toLowerCase())).sorted().collect(Collectors.toList());

        if(groups.isEmpty())
            groups.add(getPermissionPool().getDefaultGroup());

        return groups;
    }

    public List<PermissionGroup> getUnfilteredGroups() {
        removeInvalidGroups();

        List<PermissionGroup> groups = currentGroups.stream().map(g -> getPermissionPool().get(g.getPermissionGroup()))
                .filter(Objects::nonNull).sorted().collect(Collectors.toList());

        return groups;
    }

    Map<String, Boolean> getCalculatedGlobalPermissions() {
        Map<String, Boolean> permissionMap = new HashMap<>();

        if(this.cachedGlobalPermissions != null)
            return this.cachedGlobalPermissions;

        List<PermissionGroup> implementedGroups = getCurrentGroups();
        Collections.reverse(implementedGroups);

        for(PermissionGroup group : implementedGroups)
            if(group.getAvailableOn().isEmpty())
                permissionMap.putAll(group.getCalculatedGlobalPermissions());

        permissionMap.putAll(this.getGlobalPermissions());

        this.cachedGlobalPermissions = new HashMap<>();
        this.cachedGlobalPermissions.putAll(permissionMap);

        return permissionMap;
    }

    Map<String, Boolean> getCalculatedServerGroupPermissions(String serverGroup) {
        Map<String, Boolean> permissionMap = new HashMap<>();

        serverGroup = serverGroup.toLowerCase();

        if(this.cachedServerGroupPermissions == null)
            this.cachedServerGroupPermissions = new HashMap<>();

        if(this.cachedServerGroupPermissions.containsKey(serverGroup))
            return this.cachedServerGroupPermissions.get(serverGroup);

        List<PermissionGroup> implementedGroups = getCurrentGroups(serverGroup);
        Collections.reverse(implementedGroups);

        for(PermissionGroup group : implementedGroups)
            if(group.getAvailableOn().isEmpty() || group.getAvailableOn().contains(serverGroup))
                permissionMap.putAll(group.getCalculatedServerGroupPermissions(serverGroup));

        permissionMap.putAll(this.getGlobalPermissions());
        permissionMap.putAll(this.getServerGroupPermissions().getOrDefault(serverGroup, new HashMap<>()));

        this.cachedServerGroupPermissions.put(serverGroup, new HashMap<>(permissionMap));

        return permissionMap;
    }

    Map<String, Boolean> getCalculatedWorldPermissions(String serverGroup, String world) {
        Map<String, Boolean> permissionMap = new HashMap<>();

        serverGroup = serverGroup.toLowerCase();
        world = world.toLowerCase();

        if(this.cachedWorldPermissions == null)
            this.cachedWorldPermissions = new HashMap<>();

        Map<String, Boolean> worldPerms;
        Map<String, Map<String, Boolean>> serverPerms;
        if((serverPerms = this.cachedWorldPermissions.get(serverGroup)) != null)
            if((worldPerms = serverPerms.get(world)) != null)
                return worldPerms;

        List<PermissionGroup> implementedGroups = getCurrentGroups(serverGroup);
        Collections.reverse(implementedGroups);

        for(PermissionGroup group : implementedGroups)
            if(group.getAvailableOn().isEmpty() || group.getAvailableOn().contains(serverGroup))
                permissionMap.putAll(group.getCalculatedWorldPermissions(serverGroup, world));

        permissionMap.putAll(this.getGlobalPermissions());
        permissionMap.putAll(this.getServerGroupPermissions().getOrDefault(serverGroup, new HashMap<>()));
        permissionMap.putAll(this.getWorldPermissions().getOrDefault(serverGroup, new HashMap<>()).getOrDefault(world, new HashMap<>()));

        if(!this.cachedWorldPermissions.containsKey(serverGroup))
            this.cachedWorldPermissions.put(serverGroup, new HashMap<>());

        this.cachedWorldPermissions.get(serverGroup).put(world, new HashMap<>(permissionMap));

        return permissionMap;
    }


    public boolean hasPermission(String permission) {
        if(permission == null)
            return false;

        return (this.override && !permission.startsWith("-")) ||  getPermissionPool().hasPermission(permission, getCalculatedGlobalPermissions());
    }

    public boolean hasPermission(String permission, String serverGroup) {
        if(permission == null || serverGroup == null)
            return false;

        return (this.override && !permission.startsWith("-")) ||  getPermissionPool().hasPermission(permission, getCalculatedServerGroupPermissions(serverGroup));
    }

    public boolean hasPermission(String permission, String serverGroup, String world) {
        if(permission == null || serverGroup == null || world == null)
            return false;

        return (this.override && !permission.startsWith("-")) || getPermissionPool().hasPermission(permission, getCalculatedWorldPermissions(serverGroup, world));
    }

    public boolean addPermission(String permission) {
        if(permission == null)
            return false;

        String perm = permission.startsWith("-") ? permission.replaceFirst("-", "") : permission;
        this.getGlobalPermissions().put(perm.toLowerCase(), !permission.startsWith("-"));

        return true;
    }

    public boolean addPermission(String permission, String serverGroup) {
        if(permission == null || serverGroup == null)
            return false;

        String perm = permission.startsWith("-") ? permission.replaceFirst("-", "") : permission;
        Map<String, Boolean> serverGroupPermissions = this.getServerGroupPermissions().getOrDefault(serverGroup, new HashMap<>());
        serverGroupPermissions.put(perm.toLowerCase(), !permission.startsWith("-"));

        this.getServerGroupPermissions().put(serverGroup, serverGroupPermissions);

        return true;
    }

    public boolean addPermission(String permission, String serverGroup, String world) {
        if(permission == null || serverGroup == null || world == null)
            return false;

        String perm = permission.startsWith("-") ? permission.replaceFirst("-", "") : permission;
        Map<String, Map<String, Boolean>> serverGroupPermissions = this.getWorldPermissions().getOrDefault(serverGroup, new HashMap<>());
        Map<String, Boolean> worldPermissions = serverGroupPermissions.getOrDefault(world, new HashMap<>());
        worldPermissions.put(perm.toLowerCase(), !permission.startsWith("-"));

        serverGroupPermissions.put(world, worldPermissions);
        this.getWorldPermissions().put(serverGroup, serverGroupPermissions);

        return true;
    }

    public boolean removePermission(String permission) {
        if(permission == null)
            return false;

        String perm = permission.startsWith("-") ? permission.replaceFirst("-", "") : permission;
        this.getGlobalPermissions().remove(perm.toLowerCase());

        return true;
    }

    public boolean removePermission(String permission, String serverGroup) {
        if(permission == null || serverGroup == null)
            return false;

        String perm = permission.startsWith("-") ? permission.replaceFirst("-", "") : permission;
        Map<String, Boolean> serverGroupPermissions = this.getServerGroupPermissions().getOrDefault(serverGroup, new HashMap<>());
        serverGroupPermissions.remove(perm.toLowerCase());

        this.getServerGroupPermissions().put(serverGroup, serverGroupPermissions);

        return true;
    }

    public boolean removePermission(String permission, String serverGroup, String world) {
        if(permission == null || serverGroup == null || world == null)
            return false;

        String perm = permission.startsWith("-") ? permission.replaceFirst("-", "") : permission;
        Map<String, Map<String, Boolean>> serverGroupPermissions = this.getWorldPermissions().getOrDefault(serverGroup, new HashMap<>());
        Map<String, Boolean> worldPermissions = serverGroupPermissions.getOrDefault(world, new HashMap<>());
        worldPermissions.remove(perm.toLowerCase());

        serverGroupPermissions.put(world, worldPermissions);
        this.getWorldPermissions().put(serverGroup, serverGroupPermissions);

        return true;
    }

    public boolean addGroup(PermissionGroupEntity entity) {
        if(entity == null || entity.getDuration() < 0)
            return false;

        PermissionGroup group;
        if((group = getPermissionPool().get(entity.getPermissionGroup())) == null)
            return false;

        if(group == getPermissionPool().getDefaultGroup() || getCurrentGroups().contains(group))
            return false;

        currentGroups.add(entity);
        return true;
    }

    public boolean removeGroup(PermissionGroup group) {
        if(group == null || group == getPermissionPool().getDefaultGroup() || currentGroups.stream().noneMatch(g -> g.getPermissionGroup().equals(group.getName())))
            return false;

        currentGroups.removeIf(permissionGroupEntity -> permissionGroupEntity.getPermissionGroup().equalsIgnoreCase(group.getName()));

        return true;
    }

    public boolean setGroup(PermissionGroupEntity entity) {
        if(entity == null || entity.getDuration() < 0)
            return false;

        PermissionGroup group;
        if((group = getPermissionPool().get(entity.getPermissionGroup())) == null)
            return false;

        currentGroups.clear();
        currentGroups.add(entity);

        return true;
    }

    public void setJoinPower(int joinPower) {
        if(joinPower < -1)
            return;

        this.joinPower = joinPower;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    @Override
    public void serialize(Map<String, Object> data) {
        if(prefix != null) data.put("prefix", prefix);
        if(suffix != null) data.put("suffix", suffix);
        if(display != null) data.put("display", display);

        if(joinPower != -1) data.put("joinPower", joinPower);

        this.removeInvalidGroups();
        List<Map<String, Object>> groupData = new ArrayList<>();
        for(PermissionGroupEntity entity : currentGroups) {
            Map<String, Object> group = new HashMap<>();
            entity.serialize(group);
            groupData.add(group);
        }

        data.put("currentGroups", groupData);

        data.put("globalPermissions", globalPermissions.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().toLowerCase(), Map.Entry::getValue)));
        data.put("serverGroupPermissions", serverGroupPermissions.entrySet().stream().collect(
                Collectors.toMap(e -> e.getKey().toLowerCase(), e -> e.getValue().entrySet().stream().collect(
                        Collectors.toMap(e2 -> e2.getKey().toLowerCase(), Map.Entry::getValue)))));
        data.put("worldPermissions", worldPermissions.entrySet().stream().collect(
                Collectors.toMap(e -> e.getKey().toLowerCase(), e -> e.getValue().entrySet().stream().collect(
                        Collectors.toMap(e2 -> e2.getKey().toLowerCase(), e2 -> e2.getValue().entrySet().stream().collect(
                                Collectors.toMap(e3 -> e3.getKey().toLowerCase(), Map.Entry::getValue)))))));
    }

    @Override
    public void deserialize(Map<String, Object> data) {
        this.prefix = (String) data.get("prefix");
        this.suffix = (String) data.get("suffix");
        this.display = (String) data.get("display");

        this.joinPower = ((Long) data.getOrDefault("joinPower", -1L)).intValue();

        this.currentGroups = ((List<Object>) data.getOrDefault("currentGroups", new ArrayList<>()))
                .stream().map(s -> new PermissionGroupEntity((Map<String, Object>) s)).collect(Collectors.toList());
        this.removeInvalidGroups();

        this.globalPermissions = (Map<String, Boolean>) data.getOrDefault("globalPermissions", new HashMap<>());

        this.serverGroupPermissions = new HashMap<>();
        for(Map.Entry<String, Object> entry : ((Map<String, Object>) data.getOrDefault("serverGroupPermissions", new HashMap<>())).entrySet())
            if(!((Map<String, Boolean>) entry.getValue()).isEmpty())
                this.serverGroupPermissions.put(entry.getKey(), (Map<String, Boolean>) entry.getValue());

        this.worldPermissions = new HashMap<>();
        for(Map.Entry<String, Object> entry : ((Map<String, Object>) data.getOrDefault("worldPermissions", new HashMap<>())).entrySet()) {

            Map<String, Map<String, Boolean>> worldPermissions = new HashMap<>();
            for(Map.Entry<String, Object> entry1 : ((Map<String, Object>) entry.getValue()).entrySet())
                if(!((Map<String, Boolean>) entry1.getValue()).isEmpty())
                    worldPermissions.put(entry1.getKey(), (Map<String, Boolean>) entry1.getValue());

            if(!worldPermissions.isEmpty())
                this.worldPermissions.put(entry.getKey(), worldPermissions);
        }
    }

    public void sendUpdate() {
        AdviAPI.getInstance().getPlayerProvider().save(player);
        player.sendPropertyUpdate();
    }

    private PermissionPool getPermissionPool() {
        return AdviAPI.getInstance().getPermissionPool();
    }

}
