package de.fabtopf.contray.adventuria.api.player;

import com.google.common.collect.Lists;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.player.skin.SkinData;
import de.fabtopf.contray.adventuria.api.utils.bukkit.chat.ChatChannel;
import de.fabtopf.contray.adventuria.api.utils.bukkit.scoreboard.ScoreboardValue;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;

@Getter
public class PlayerProperty<OutputType, CacheType> {

    public static final PlayerProperty<SkinData, UUID> SKIN =
            new PlayerProperty<>("SKIN", new SkinData(UUID.fromString("c06f8906-4c8a-4911-9c29-ea1dbd1aab82"), "Alex", "FevxcI4pPNO8pxj7+FsjCMRuI01KEgNLdu4r93+OB/wb//OHoxWjaAh0NKPMPMqZRYsMF7qTXXLZphKviWhI+ZhPYyMugsGubT+jgEFyVDE6A9FBhFQrCbgp9j5uUH78EMk/FBnARZ2nBKmdiRZFy2QPkFgR4OzIaqbxAEe430zT9He5vz1edO3vS8ZYYrNLHly12nFldXf/UqMIZeBneksIG/srkNBhkESlCFhxEhYjfkDmTOxdbMJCRN4rXEf+xE4pQXQS6se5hF3QtDG6/e43n4e5FnqBqYiX1wgzsmqCtT3hd3gEDKJ1L0lzGNpcRz7xEZyDWu6v05nz8coWrkMxijEFhOCUlp0eCwnAJWsS9Zik8Ih1JegkPAlyl4wRtUKeKbvNEWEYXtGjOgdY5KbvigW050igRRpc8rMt2lWfFSkn+XjDKiBzDUNBHjfQvtX7FmSnZW0M7DTO+X8xwTr3JSZsTrl5AHPJdol3BRfRqZGbT7dkUvMh2sqxGNzX0myN5yAvKIPX65zMl8NkgUmCBZF8q2DWjSd/QugwkNeUIISfIvv8ZvfBMisbQM9zh+jQjuKpKnHVRqKnWJMzWHLznraIOkAoHW2P9hkH6jTP6hLOhyVYalcFxx8P6zlqAagy6SFOxOtwY9JXG2Q0ZLJmESlEsCMfUIn5FhmzOLI=", "eyJ0aW1lc3RhbXAiOjE1NTM0MzE1NTYwNDcsInByb2ZpbGVJZCI6ImMwNmY4OTA2NGM4YTQ5MTE5YzI5ZWExZGJkMWFhYjgyIiwicHJvZmlsZU5hbWUiOiJNSEZfU3RldmUiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnt9fQ==", false, Lists.newArrayList(UUID.randomUUID())), null,
                    new Callback<SkinData, UUID>() {

                        @Override
                        public SkinData get(NetworkPlayer player) {

                            UUID uniqueId;
                            if((uniqueId = (UUID) player.getProperties().get(PlayerProperty.SKIN.getIdentifier())) == null)
                                return PlayerProperty.SKIN.getDefaultOutputValue();

                            return AdviAPI.getInstance().getPlayerProvider().getSkinDataProvider().get(uniqueId);
                        }

                        @Override
                        public UUID parse(Map<String, Object> data) {

                            String id;
                            if((id = (String) data.get(PlayerProperty.SKIN.getIdentifier())) == null)
                                return null;

                            return UUID.fromString(id);
                        }

                    });

    public static final PlayerProperty<Long, Long> FIRST_CONNECTION =
            new PlayerProperty<>("FIRST_CONNECTION", 0L);

    public static final PlayerProperty<Long, Long> LAST_CONNECTION =
            new PlayerProperty<>("LAST_CONNECTION", 0L);

    public static final PlayerProperty<Long, Long> TOTAL_ONTIME =
            new PlayerProperty<>("TOTAL_ONTIME", 0L, 0L, new Callback<Long, Long>() {

                @Override
                public Long get(NetworkPlayer player) {
                    return (long) player.getProperties().get("TOTAL_ONTIME") + (player.isOnline() ?
                            (System.currentTimeMillis() - (long) player.getProperties().get("LAST_CONNECTION")) : 0L);
                }

                @Override
                public Long parse(Map data) {
                    return (long) data.getOrDefault("TOTAL_ONTIME", 0L);
                }

            });

    public static final PlayerProperty<Map<String, Long>, Map<String, Long>> ONTIME =
            new PlayerProperty<>("ONTIME", new HashMap<>());

    public static final PlayerProperty<Map<String, List<ScoreboardValue>>, Map<String, List<String>>> SCOREBOARD =
            new PlayerProperty<>("SCOREBOARD", new HashMap<>(), new HashMap<>(), new Callback<Map<String, List<ScoreboardValue>>, Map<String, List<String>>>() {

                @Override
                public Map<String, List<String>> parse(Map<String, Object> data) {
                    return (Map<String, List<String>>) data.getOrDefault(SCOREBOARD.getIdentifier(), SCOREBOARD.getDefaultCacheValue());
                }

                @Override
                public Map<String, List<ScoreboardValue>> get(NetworkPlayer player) {

                    Map<String, List<ScoreboardValue>> result = new HashMap<>();
                    for(Map.Entry<String, List<String>> entry : ((Map<String, List<String>>) player.getProperties().getOrDefault(SCOREBOARD.getIdentifier(), SCOREBOARD.getDefaultCacheValue())).entrySet())
                        result.put(entry.getKey(), entry.getValue().stream().map(s -> AdviAPI.getInstance().bukkit().getScoreboardManager().getValueMap().get(s))
                                .filter(Objects::nonNull).collect(Collectors.toList()));

                    return result;
                }

            });

    public static final PlayerProperty<Map<String, String>, Map<String, String>> CHAT_CHANNEL =
            new PlayerProperty<>("CHAT_CHANNEL", new HashMap<>());

    public static final PlayerProperty<List<ChatChannel>, List<String>> IGNORED_CHAT_CHANNEL =
            new PlayerProperty<>("IGNORED_CHAT_CHANNEL", new ArrayList<>(), new ArrayList<>(), new Callback<List<ChatChannel>, List<String>>() {

                @Override
                public List<ChatChannel> get(NetworkPlayer player) {
                    return ((List<String>) player.getProperties().getOrDefault(IGNORED_CHAT_CHANNEL.getIdentifier(), IGNORED_CHAT_CHANNEL.getDefaultCacheValue()))
                            .stream().map(s -> AdviAPI.getInstance().getChatManager() != null ? AdviAPI.getInstance().getChatManager().getChannel(s) : null).filter(Objects::nonNull)
                            .collect(Collectors.toList());
                }

                @Override
                public List<String> parse(Map<String, Object> data) {
                    return (List<String>) data.getOrDefault(IGNORED_CHAT_CHANNEL.getIdentifier(), IGNORED_CHAT_CHANNEL.getDefaultCacheValue());
                }

            });

    public static final PlayerProperty<List<ChatChannel>, List<String>> SPY_CHAT_CHANNEL =
            new PlayerProperty<>("SPY_CHAT_CHANNEL", new ArrayList<>(), new ArrayList<>(), new Callback<List<ChatChannel>, List<String>>() {

                @Override
                public List<ChatChannel> get(NetworkPlayer player) {
                    return ((List<String>) player.getProperties().getOrDefault(SPY_CHAT_CHANNEL.getIdentifier(), SPY_CHAT_CHANNEL.getDefaultCacheValue()))
                            .stream().map(s -> AdviAPI.getInstance().getChatManager() != null ? AdviAPI.getInstance().getChatManager().getChannel(s) : null).filter(Objects::nonNull)
                            .collect(Collectors.toList());
                }

                @Override
                public List<String> parse(Map<String, Object> data) {
                    return (List<String>) data.getOrDefault(SPY_CHAT_CHANNEL.getIdentifier(), SPY_CHAT_CHANNEL.getDefaultCacheValue());
                }

            });

    public static final PlayerProperty<Map<UUID, Long>, Map<String, Long>> MONEY =
            new PlayerProperty<>("MONEY", new HashMap<>(), new HashMap<>(), new Callback<Map<UUID, Long>, Map<String, Long>>() {

                @Override
                public Map<UUID, Long> get(NetworkPlayer player) {

                    Map<UUID, Long> result = new HashMap<>();
                    for(Map.Entry<String, Long> entry : ((Map<String, Long>) player.getProperties().getOrDefault(MONEY.getIdentifier(), MONEY.getDefaultCacheValue())).entrySet())
                        result.put(UUID.fromString(entry.getKey()), entry.getValue());

                    return result;
                }

                @Override
                public Map<String, Long> parse(Map<String, Object> data) {
                    Map<String, Long> result = new HashMap<>();
                    for(Map.Entry<String, Long> entry : ((Map<String, Long>) data.getOrDefault(MONEY.getIdentifier(), MONEY.getDefaultCacheValue())).entrySet())
                        result.put(entry.getKey(), entry.getValue());

                    return result;
                }

            });

    private final String identifier;
    private final OutputType defaultOutputValue;
    private final CacheType defaultCacheValue;
    private final Callback<OutputType, CacheType> callback;

    public PlayerProperty(String identifier, CacheType defaultCacheValue) {
        this.identifier = identifier;
        this.defaultOutputValue = null;
        this.defaultCacheValue = defaultCacheValue;

        this.callback = new SimpleCallback<>();
    }

    public PlayerProperty(String identifier, OutputType defaultOutputValue, CacheType defaultCacheValue, Callback<OutputType, CacheType> callback) {
        this.identifier = identifier;
        this.defaultOutputValue = defaultOutputValue;
        this.defaultCacheValue = defaultCacheValue;

        this.callback = callback;
    }

    public static PlayerProperty<?, ?> valueOf(String identifier) {
        for (PlayerProperty playerProperty : PlayerProperty.values())
            if (playerProperty.getIdentifier().equals(identifier))
                return playerProperty;

        return null;
    }

    public static PlayerProperty[] values() {
        return new PlayerProperty[] {
                PlayerProperty.SKIN,
                PlayerProperty.FIRST_CONNECTION,
                PlayerProperty.LAST_CONNECTION,
                PlayerProperty.TOTAL_ONTIME,
                PlayerProperty.ONTIME,
                PlayerProperty.SCOREBOARD,
                PlayerProperty.CHAT_CHANNEL,
                PlayerProperty.IGNORED_CHAT_CHANNEL,
                PlayerProperty.SPY_CHAT_CHANNEL,
                PlayerProperty.MONEY
        };
    }

    interface Callback<GetterType, ParserType> {
        GetterType get(NetworkPlayer player);
        ParserType parse(Map<String, Object> data);
    }

    class SimpleCallback<GetterType, ParserType> implements Callback<GetterType, ParserType> {

        @Override
        @SuppressWarnings("unchecked")
        public GetterType get(NetworkPlayer player) {
            if(getDefaultOutputValue() != null)
                return (GetterType) player.getProperties().getOrDefault(getIdentifier(), getDefaultOutputValue());

            return (GetterType) player.getProperties().getOrDefault(getIdentifier(), getDefaultCacheValue());
        }

        @Override
        @SuppressWarnings("unchecked")
        public ParserType parse(Map<String, Object> data) {
            if (getDefaultCacheValue() instanceof Integer)
                return (ParserType) Integer.valueOf(((Number) data.getOrDefault(getIdentifier(), getDefaultCacheValue())).intValue());

            return (ParserType) data.getOrDefault(getIdentifier(), getDefaultCacheValue());
        }

    }

}
