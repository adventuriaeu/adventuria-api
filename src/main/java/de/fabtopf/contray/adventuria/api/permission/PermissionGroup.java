package de.fabtopf.contray.adventuria.api.permission;

import com.arangodb.entity.BaseDocument;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.utils.arango.ArangoWritable;
import lombok.*;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter(AccessLevel.PACKAGE)
@RequiredArgsConstructor
@AllArgsConstructor
public class PermissionGroup implements ArangoWritable<String>, Comparable<PermissionGroup> {

    private transient PermissionPool permissionPool = AdviAPI.getInstance().getPermissionPool();

    @NonNull
    @Setter(AccessLevel.NONE)
    private String name;

    private String prefix;
    private String suffix;
    private String display;

    private boolean defaultGroup;

    private Integer tagId;
    private int joinPower;

    private List<String> availableOn;

    private Map<String, Object> options;

    private Map<String, Boolean> globalPermissions;
    private Map<String, Map<String, Boolean>> serverGroupPermissions;
    private Map<String, Map<String, Map<String, Boolean>>> worldPermissions;

    private Map<String, Boolean> cachedGlobalPermissions;
    private Map<String, Map<String, Boolean>> cachedServerGroupPermissions;
    private Map<String, Map<String, Map<String, Boolean>>> cachedWorldPermissions;

    private List<String> implementedGroups;

    public PermissionGroup(PermissionPool pool, String name, String prefix, String suffix, String display, boolean defaultGroup, int tagId, int joinPower, List<String> availableOn, Map<String, Object> options,
                           Map<String, Boolean> globalPermissions, Map<String, Map<String, Boolean>> serverGroupPermissions, Map<String, Map<String, Map<String, Boolean>>> worldPermissions,
                           List<String> implementedGroups) {
        this.permissionPool = pool;
        this.name = name;
        this.prefix = prefix;
        this.suffix = suffix;
        this.display = display;
        this.defaultGroup = defaultGroup;
        this.tagId = tagId;
        this.joinPower = joinPower;
        this.options = options;
        this.globalPermissions = globalPermissions;
        this.serverGroupPermissions = serverGroupPermissions;
        this.worldPermissions = worldPermissions;
        this.implementedGroups = implementedGroups;
        this.availableOn = availableOn;
    }

    public List<PermissionGroup> getImplementedGroups() {
        List<PermissionGroup> implementedGroups = new ArrayList<>();

        Iterator<String> groupIterator = this.implementedGroups.iterator();
        while(groupIterator.hasNext()) {
            String implementedGroup = groupIterator.next();

            PermissionGroup group;
            if ((group = permissionPool.get(implementedGroup)) == null)
                groupIterator.remove();

            if(group != null)
                implementedGroups.add(group);
        }

        Collections.sort(implementedGroups);

        return implementedGroups;
    }

    public void clearCachedPermissions() {
        this.cachedGlobalPermissions = null;
        this.cachedServerGroupPermissions.clear();
        this.cachedWorldPermissions.clear();
    }

    @Override
    public String getKey() {
        return name;
    }

    @Override
    public void read(BaseDocument document) {
        this.prefix = (String) document.getProperties().getOrDefault("prefix", "§7");
        this.suffix = (String) document.getProperties().getOrDefault("suffix", "§7");
        this.display = (String) document.getProperties().getOrDefault("display", "§7");

        this.defaultGroup = (boolean) document.getProperties().getOrDefault("default", false);

        this.tagId = ((Long) document.getProperties().getOrDefault("tagId", 1000L)).intValue();
        this.joinPower = ((Long) document.getProperties().getOrDefault("joinPower", 0L)).intValue();

        this.availableOn = (List<String>) document.getProperties().getOrDefault("availableOn", new ArrayList<>());
        this.implementedGroups = (List<String>) document.getProperties().getOrDefault("implementedGroups", new ArrayList<>());

        this.options = (Map<String, Object>) document.getProperties().getOrDefault("options", new HashMap<>());

        this.globalPermissions = (Map<String, Boolean>) document.getProperties().getOrDefault("globalPermissions", new HashMap<>());

        this.serverGroupPermissions = new HashMap<>();
        for(Map.Entry<String, Object> entry : ((Map<String, Object>) document.getProperties().getOrDefault("serverGroupPermissions", new HashMap<>())).entrySet())
            if(!((Map<String, Boolean>) entry.getValue()).isEmpty())
                this.serverGroupPermissions.put(entry.getKey(), (Map<String, Boolean>) entry.getValue());

        this.worldPermissions = new HashMap<>();
        for(Map.Entry<String, Object> entry : ((Map<String, Object>) document.getProperties().getOrDefault("worldPermissions", new HashMap<>())).entrySet()) {

            Map<String, Map<String, Boolean>> worldPermissions = new HashMap<>();
            for(Map.Entry<String, Object> entry1 : ((Map<String, Object>) entry.getValue()).entrySet())
                if(!((Map<String, Boolean>) entry.getValue()).isEmpty())
                    worldPermissions.put(entry1.getKey(), (Map<String, Boolean>) entry1.getValue());

            if(!worldPermissions.isEmpty())
                this.worldPermissions.put(entry.getKey(), worldPermissions);
        }

        this.cachedServerGroupPermissions = new HashMap<>();
        this.cachedWorldPermissions = new HashMap<>();
    }

    @Override
    public void save(BaseDocument document) {
        document.addAttribute("prefix", prefix);
        document.addAttribute("suffix", suffix);
        document.addAttribute("display", display);

        document.addAttribute("default", defaultGroup);

        document.addAttribute("tagId", tagId);
        document.addAttribute("joinPower", joinPower);

        document.addAttribute("availableOn", availableOn.stream().map(String::toLowerCase).collect(Collectors.toList()));
        document.addAttribute("implementedGroups", implementedGroups);

        document.addAttribute("options", options);

        document.addAttribute("globalPermissions", globalPermissions.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().toLowerCase(), Map.Entry::getValue)));
        document.addAttribute("serverGroupPermissions", serverGroupPermissions.entrySet().stream().collect(
                Collectors.toMap(e -> e.getKey().toLowerCase(), e -> e.getValue().entrySet().stream().collect(
                        Collectors.toMap(e2 -> e2.getKey().toLowerCase(), Map.Entry::getValue)))));
        document.addAttribute("worldPermissions", worldPermissions.entrySet().stream().collect(
                Collectors.toMap(e -> e.getKey().toLowerCase(), e -> e.getValue().entrySet().stream().collect(
                        Collectors.toMap(e2 -> e2.getKey().toLowerCase(), e2 -> e2.getValue().entrySet().stream().collect(
                                Collectors.toMap(e3 -> e3.getKey().toLowerCase(), Map.Entry::getValue)))))));
    }

    Map<String, Boolean> getCalculatedGlobalPermissions() {
        Map<String, Boolean> permissionMap = new HashMap<>();

        if(this.cachedGlobalPermissions != null)
            return this.cachedGlobalPermissions;

        List<PermissionGroup> implementedGroups = getImplementedGroups();
        Collections.reverse(implementedGroups);

        for(PermissionGroup group : implementedGroups)
            if(group.getAvailableOn().isEmpty())
                permissionMap.putAll(group.getGlobalPermissions());

        if(availableOn.isEmpty())
            permissionMap.putAll(this.getGlobalPermissions());

        this.cachedGlobalPermissions = new HashMap<>();
        this.cachedGlobalPermissions.putAll(permissionMap);

        return permissionMap;
    }

    Map<String, Boolean> getCalculatedServerGroupPermissions(String serverGroup) {
        Map<String, Boolean> permissionMap = new HashMap<>();

        serverGroup = serverGroup.toLowerCase();

        if(this.cachedServerGroupPermissions == null)
            this.cachedServerGroupPermissions = new HashMap<>();

        if(this.cachedServerGroupPermissions.containsKey(serverGroup))
            return this.cachedServerGroupPermissions.get(serverGroup);

        List<PermissionGroup> implementedGroups = getImplementedGroups();
        Collections.reverse(implementedGroups);

        for(PermissionGroup group : implementedGroups) {
            if(group.getAvailableOn().isEmpty() || group.getAvailableOn().contains(serverGroup)) {
                permissionMap.putAll(group.getGlobalPermissions());

                if ((boolean) options.getOrDefault("includeservergrouppermissions", false))
                    permissionMap.putAll(group.getServerGroupPermissions().getOrDefault(serverGroup, new HashMap<>()));
            }
        }

        if(this.getAvailableOn().isEmpty() || this.getAvailableOn().contains(serverGroup)) {
            permissionMap.putAll(this.getGlobalPermissions());
            permissionMap.putAll(this.getServerGroupPermissions().getOrDefault(serverGroup, new HashMap<>()));
        }

        this.cachedServerGroupPermissions.put(serverGroup, new HashMap<>(permissionMap));

        return permissionMap;
    }

    Map<String, Boolean> getCalculatedWorldPermissions(String serverGroup, String world) {
        Map<String, Boolean> permissionMap = new HashMap<>();

        serverGroup = serverGroup.toLowerCase();
        world = world.toLowerCase();

        if(this.cachedWorldPermissions == null)
            this.cachedWorldPermissions = new HashMap<>();

        Map<String, Boolean> worldPerms;
        Map<String, Map<String, Boolean>> serverPerms;
        if((serverPerms = this.cachedWorldPermissions.get(serverGroup)) != null)
            if((worldPerms = serverPerms.get(world)) != null)
                return worldPerms;

        List<PermissionGroup> implementedGroups = getImplementedGroups();
        Collections.reverse(implementedGroups);

        for(PermissionGroup group : implementedGroups) {
            if(group.getAvailableOn().isEmpty() || group.getAvailableOn().contains(serverGroup)) {
                permissionMap.putAll(group.getGlobalPermissions());

                if ((boolean) options.getOrDefault("includeservergrouppermissions", false))
                    permissionMap.putAll(group.getServerGroupPermissions().getOrDefault(serverGroup, new HashMap<>()));

                if ((boolean) options.getOrDefault("includeworldpermissions", false))
                    permissionMap.putAll(group.getWorldPermissions().getOrDefault(serverGroup, new HashMap<>()).getOrDefault(world, new HashMap<>()));
            }
        }

        if(this.getAvailableOn().isEmpty() || this.getAvailableOn().contains(serverGroup)) {
            permissionMap.putAll(this.getGlobalPermissions());
            permissionMap.putAll(this.getServerGroupPermissions().getOrDefault(serverGroup, new HashMap<>()));
            permissionMap.putAll(this.getWorldPermissions().getOrDefault(serverGroup, new HashMap<>()).getOrDefault(world, new HashMap<>()));
        }

        if(!this.cachedWorldPermissions.containsKey(serverGroup))
            this.cachedWorldPermissions.put(serverGroup, new HashMap<>());

        this.cachedWorldPermissions.get(serverGroup).put(world, new HashMap<>(permissionMap));

        return permissionMap;
    }


    public boolean hasPermission(String permission) {
        if(permission == null)
            return false;

        if(!availableOn.isEmpty())
            return false;

        return permissionPool.hasPermission(permission, getCalculatedGlobalPermissions());
    }

    public boolean hasPermission(String permission, String serverGroup) {
        if(permission == null || serverGroup == null)
            return false;

        return permissionPool.hasPermission(permission, getCalculatedServerGroupPermissions(serverGroup));
    }

    public boolean hasPermission(String permission, String serverGroup, String world) {
        if(permission == null || serverGroup == null || world == null)
            return false;

        return permissionPool.hasPermission(permission, getCalculatedWorldPermissions(serverGroup, world));
    }

    @Override
    public int compareTo(PermissionGroup o) {
        return this.tagId.compareTo(o.tagId);
    }

    public static class DefaultPermissionGroup extends PermissionGroup {
        public DefaultPermissionGroup(PermissionPool permissionPool) {
            super(permissionPool, "default", "", "", "", false, 1000, 0, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>());
        }
    }

}
