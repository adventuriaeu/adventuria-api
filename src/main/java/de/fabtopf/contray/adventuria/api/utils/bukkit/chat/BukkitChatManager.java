package de.fabtopf.contray.adventuria.api.utils.bukkit.chat;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.ICommandExecutor;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommand;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleFunctionCommandExecutor;
import de.fabtopf.contray.adventuria.api.utils.event.bukkit.AdviChatEvent;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.plugin.Command;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.*;

public class BukkitChatManager extends ChatManager {

    public BukkitChatManager(File configFile) {
        super(configFile);
    }

    @Override
    protected void readChannels() {
        Configuration configuration = org.bukkit.configuration.file.YamlConfiguration.loadConfiguration(configFile);

        for(String s : configuration.getKeys(false)) {
            String name = configuration.getString(s + ".name");
            String format = configuration.getString(s + ".format");
            String prefix = configuration.getString(s + ".prefix");
            String display = configuration.getString(s + ".display");
            String writePermission = configuration.getString(s + ".write_permission");
            String readPermission = configuration.getString(s + ".read_permission");
            String spyPermission = configuration.getString(s + ".spy_permission");
            String communicationChannel = configuration.getString(s + ".communication_channel");
            String command = configuration.getString(s + ".command");
            boolean standard = configuration.getBoolean(s + ".default");
            int range = configuration.getInt(s + ".range");
            String typeName = configuration.getString(s + ".type");

            ChatChannel.Type type;
            if(typeName == null || (type = ChatChannel.Type.valueOf(typeName.toUpperCase())) == null) {
                BukkitCore.getInstance().getLogger().warning("ChatChannel " + name + " could not be registered: invalid channel mode");
                continue;
            }

            registerChannel(name, format, communicationChannel, prefix, display, writePermission, readPermission, spyPermission, command, standard, range, type);

        }

        if(channels.isEmpty() || channels.stream().noneMatch(ChatChannel::isDefault))
            registerChannel("default", BukkitCore.getInstance().getLanguage().getPhraseContent("system-chat-format"),
                    null, "!", "Server", null, null, null,
                    "serverchat", true, 0, ChatChannel.Type.SERVER);

    }

    @Override
    public boolean registerChannel(String name, String format, String communicationChannel, String prefix, String display, String writePermission, String readPermission, String spyPermission, String command, boolean standard, int range, ChatChannel.Type type) {

        if(name == null || format == null || display == null || command == null || type == null ||
                (prefix == null && type == ChatChannel.Type.SERVER)) {
            Bukkit.getLogger().warning("ChatChannel " + name + " could not be registered: missing information");
            return false;
        }

        ChatChannel channel = new ChatChannel(
                name,
                format,
                communicationChannel != null && communicationChannel.equals("") ? null : communicationChannel,
                prefix,
                display,
                writePermission != null && writePermission.equals("") ? null : writePermission,
                readPermission != null && readPermission.equals("") ? null : readPermission,
                spyPermission != null && spyPermission.equals("") ? null : spyPermission,
                command,
                standard,
                range,
                type);

        if(channels.contains(channel)) {
            Bukkit.getLogger().warning("ChatChannel " + name + " could not be registered: name already registered");
            return false;
        }

        channels.add(channel);

        Bukkit.getLogger().info("ChatChannel " + name + " is now registered");
        if(type == ChatChannel.Type.CUSTOM)
            return true;

        BukkitChatCommand cmd;
        if((cmd = BukkitChatCommand.register(command)).register())
            channel.setCommandObj(cmd);

        return true;
    }

    @Override
    public boolean unregisterChannel(String channel)  {

        ChatChannel c;
        if((c = getChannel(channel)) == null) {

            if(AdviAPI.getInstance().isProxy())
                BungeeCord.getInstance().getLogger().warning("ChatChannel " + channel + " could not be unregistered: channel doesn't exist");
            else
                Bukkit.getLogger().warning("ChatChannel " + channel + " could not be registered: channel doesn't exist");

            return false;
        }

        if(c.equals(getDefaultChannel())) {

            if(AdviAPI.getInstance().isProxy())
                BungeeCord.getInstance().getLogger().warning("ChatChannel " + channel + " could not be unregistered: channel is default");
            else
                Bukkit.getLogger().warning("ChatChannel " + channel + " could not be unregistered: channel is default");

            return false;
        }

        channels.remove(c);

        if(AdviAPI.getInstance().isProxy()) {
            BungeeCord.getInstance().getPluginManager().unregisterCommand((Command) c.getCommandObj());
            BungeeCord.getInstance().getLogger().warning("ChatChannel " + channel + " unregistered");
        } else {
            ((SimpleCommand) c.getCommandObj()).unregister();
            Bukkit.getLogger().warning("ChatChannel " + channel + " unregistered");
        }

        return true;
    }

    private static class BukkitChatCommand extends SimpleCommand {

        public static BukkitChatCommand register(String name) {
            return new BukkitChatCommand(name,
                    BukkitCore.getInstance().getLanguage().getPhraseContent("system-chat-command-description"),
                    BukkitCore.getInstance().getLanguage().getPhraseContent("system-chat-command-usage"),
                    new ArrayList<>(), "adventuria.api.command.chat",
                    BukkitCore.getInstance().getLanguage().getPhraseContent("system-chat-command-permissionMessage"),
                    BukkitCore.getInstance(), BukkitChatCommand.getCommandExecutor(name));
        }

        BukkitChatCommand(String name, String description, String usage, List<String> aliases, String permission, String permissionMessage, Plugin plugin, ICommandExecutor executor) {
            super(name, "adviapi-chat", description, usage, aliases, permission, permissionMessage, plugin, executor);
        }

        static SimpleFunctionCommandExecutor getCommandExecutor(String name) {

            return new SimpleFunctionCommandExecutor() {

                @Override
                public void sendHelpMessage(CommandSender executor, SimpleCommand command) {}

                @Override
                public String getUsage() {
                    return "/" + name + " <Nachricht>";
                }

                @Override
                public String getPermissionMessage() {
                    return BukkitCore.getInstance().getLanguage().getPhraseContent("no-permission");
                }

                @Override
                public String getDescription() {
                    return "Command zur Interaktion mit einem Chat";
                }

                @Override
                public void onExecute(CommandSender sender, SimpleCommand command, String label, String[] args) {

                    ChatManager manager = AdviAPI.getInstance().getChatManager();
                    ChatChannel channel = null;
                    for(ChatChannel c : manager.getChannels())
                        if(c.getCommand() != null && c.getCommand().equalsIgnoreCase(command.getName()))
                            channel = c;

                    if(channel == null)
                        return;

                    if(channel.getWritePermission() == null || sender.hasPermission(channel.getWritePermission())) {

                        if (channel.getType() == ChatChannel.Type.SERVER) {

                            if (sender instanceof Player) ((Player) sender).chat(channel.getPrefix() + String.join(" ", args));
                            else Bukkit.broadcastMessage(AdviAPI.getInstance().getChatManager().convertChannelFormat(null, null, String.join(" ", args), channel, true, new HashMap<>()));

                        } else {

                            Map<String, Object> info = new HashMap<>();

                            info.put("server", AdviAPI.getInstance().getServerName());

                            if (sender instanceof Player) manager.invokeChatMessage(AdviAPI.getInstance().getPlayerProvider().get(((Player) sender).getUniqueId()), String.join(" ", args), channel.getName(), true, info);
                            else manager.invokeChatMessage(null, String.join(" ", args), channel.getName(), true, info);

                        }

                    } else {

                        sender.sendMessage(this.getPermissionMessage());
                    }

                }

            };

        }

    }

    @Override
    public boolean invokeChatMessage(NetworkPlayer invoker, String message, String channel, boolean command, boolean externalTrigger, Map<String, Object> info) {
        Cancellable cancellable = new AdviChatEvent(invoker, message, channel, info, externalTrigger, command);
        Bukkit.getPluginManager().callEvent((AdviChatEvent) cancellable);

        return cancellable.isCancelled();
    }

    @Override
    protected void sendChatMessage(UUID receiver, NetworkPlayer invoker, String message, ChatChannel channel, boolean command, boolean spy, Map<String, Object> info) {
        Player player;
        if((player = Bukkit.getPlayer(receiver)) == null)
            return;

        player.sendMessage((spy ? BukkitCore.getInstance().getLanguage().getPhraseContent("system-chat-spy-prefix") : "") +
                convertChannelFormat(receiver, invoker, message, channel, command, info));
    }

}
