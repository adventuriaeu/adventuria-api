package de.fabtopf.contray.adventuria.api.utils.bukkit.chat;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.internal.bungee.BungeeCore;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import lombok.Getter;

import java.io.File;
import java.util.*;
import java.util.regex.Pattern;

@Getter
public abstract class ChatManager {

    protected File configFile;

    protected List<ChatChannel> channels = new ArrayList<>();

    public ChatManager(File configFile) {
        this.configFile = configFile;

        readChannels();
    }

    protected abstract void readChannels();

    public abstract boolean registerChannel(String name, String format, String communicationChannel, String prefix, String display, String writePermission, String readPermission,
                                   String spyPermission, String command, boolean standard, int range, ChatChannel.Type type);

    public boolean registerChannel(ChatChannel channel) {
        if(channel.getType() != ChatChannel.Type.CUSTOM || channels.contains(channel))
            return false;

        channels.add(channel);
        return true;
    }

    public abstract boolean unregisterChannel(String channel);

    public ChatChannel getDefaultChannel() {
        for(ChatChannel channel : channels)
            if(channel.isDefault())
                return channel;

        return null;
    }

    public ChatChannel getChannel(String channel) {
        for(ChatChannel ch : channels)
            if(ch.getName().equalsIgnoreCase(channel))
                return ch;

        return null;
    }

    public String convertChannelFormat(UUID receiver, NetworkPlayer invoker, String message, ChatChannel channel, boolean command, Map<String, Object> info) {
        if(!command && channel.getPrefix() != null)
            message = message.replaceFirst(Pattern.quote(channel.getPrefix()), "");

        return convertChannelFormat(receiver, invoker, message, channel.getFormat(), channel.getName(), channel.getDisplay(), info);
    }

    public String convertChannelFormat(UUID receiver, NetworkPlayer invoker, String message, String format, String channelName, String channelDisplay) {
        return convertChannelFormat(receiver, invoker, message, format, channelName, channelDisplay, new HashMap<>());
    }

    public String convertChannelFormat(UUID receiver, NetworkPlayer invoker, String message, String format, String channelName, String channelDisplay, Map<String, Object> info) {
        Map<String, String> replacements = new HashMap<>();

        String console = AdviAPI.getInstance().isProxy() ?
                BungeeCore.getInstance().getLanguage().getPhraseContent("system-chat-console-display") :
                BukkitCore.getInstance().getLanguage().getPhraseContent("system-chat-console-display");

        NetworkPlayer target = null;
        if(receiver != null)
            target = AdviAPI.getInstance().getPlayerProvider().get(receiver);

        if(invoker == null || invoker.getPermissionUser().hasPermission("adventuria.api.chat.color")) {
            char[] b = message.toCharArray();
            for(int i = 0; i < b.length - 1; ++i) {
                if (b[i] == '&' && "0123456789AaBbCcDdEeFfKkLlMmNnOoRr".indexOf(b[i + 1]) > -1) {
                    b[i] = 167;
                    b[i + 1] = Character.toLowerCase(b[i + 1]);
                }
            }

            message = new String(b);
        }

        replacements.put("target_name", target == null ? "" : target.getName());
        replacements.put("target_displayname", target == null ? "" : target.getNameFormatted());
        replacements.put("target_group", target == null ? "" : target.getHighestPermissionGroup().getDisplay());
        replacements.put("target_prefix", target == null ? "" : target.getPermissionUser().getPrefix(AdviAPI.getInstance().getCloudUtilities().getGroup()));
        replacements.put("target_suffix", target == null ? "" : target.getPermissionUser().getSuffix(AdviAPI.getInstance().getCloudUtilities().getGroup()));
        replacements.put("target_display", target == null ? "" : target.getPermissionUser().getDisplay(AdviAPI.getInstance().getCloudUtilities().getGroup()));
        replacements.put("invoker_name", invoker == null ? console : invoker.getName());
        replacements.put("invoker_displayname", invoker == null ? console : invoker.getNameFormatted());
        replacements.put("invoker_group", invoker == null ? "" : invoker.getHighestPermissionGroup().getDisplay());
        replacements.put("invoker_prefix", invoker == null ? "" : invoker.getPermissionUser().getPrefix(AdviAPI.getInstance().getCloudUtilities().getGroup()));
        replacements.put("invoker_suffix", invoker == null ? "" : invoker.getPermissionUser().getSuffix(AdviAPI.getInstance().getCloudUtilities().getGroup()));
        replacements.put("invoker_display", invoker == null ? "" : invoker.getPermissionUser().getDisplay(AdviAPI.getInstance().getCloudUtilities().getGroup()));
        replacements.put("console", console);
        replacements.put("channel", channelName);
        replacements.put("channel_display", channelDisplay);
        replacements.put("server", info.containsKey("server") ? (String) info.get("server") : AdviAPI.getInstance().getServerName());
        replacements.put("message", message);

        for(Map.Entry<String, String> entry : replacements.entrySet())
            format = format.replace("%" + entry.getKey() + "%", entry.getValue());

        return format;
    }

    public boolean invokeChatMessage(NetworkPlayer invoker, String message, String channel, boolean command, Map<String, Object> info) {
        return this.invokeChatMessage(invoker, message, channel, command, false, info);
    }

    public abstract boolean invokeChatMessage(NetworkPlayer invoker, String message, String channel, boolean command, boolean externalTrigger, Map<String, Object> info);

    public void sendChatMessage(UUID receiver, NetworkPlayer invoker, String message, ChatChannel channel, boolean command, Map<String, Object> info) {
        this.sendChatMessage(receiver, invoker, message, channel, command, false, info);
    }

    public void sendSpyMessage(UUID receiver, NetworkPlayer invoker, String message, ChatChannel channel, boolean command, Map<String, Object> info) {
        this.sendChatMessage(receiver, invoker, message, channel, command, true, info);
    }

    protected abstract void sendChatMessage(UUID receiver, NetworkPlayer invoker, String message, ChatChannel channel, boolean command, boolean spy, Map<String, Object> info);

}
