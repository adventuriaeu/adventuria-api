package de.fabtopf.contray.adventuria.api.utils.bukkit.command.base;

import org.bukkit.command.CommandSender;

public interface SimpleSubCommandExecutor extends ICommandExecutor {

    @Override
    default void onExecute(CommandSender sender, SimpleCommand command, String label, String[] args) {

        for(ISimpleSubCommand subCommand : command.getSubCommandList())
            if(subCommand.trigger(sender, command, label, args))
                return;

        this.sendHelpMessage(sender, command);
    }

    @Override
    default void sendHelpMessage(CommandSender sender, SimpleCommand command) {

        sender.sendMessage("§7§m»------------------§8× §e" + command.getName() + " §8×§7§m------------------«");

        for (ISimpleSubCommand subCommand : command.getSubCommandList()) {
            if (!subCommand.checkSender(sender)) continue;

            if (!subCommand.checkPermission(sender)) continue;

            if (!subCommand.isShowInHelp()) continue;

            StringBuilder builder = new StringBuilder();
            builder.append("§b§l/").append(command.getName()).append(" ");
            for (String arg : subCommand.getHelpMessageParams())
                builder.append(arg).append(" ");

            sender.sendMessage(builder.toString());
        }

        sender.sendMessage("§7§m»------------------§8× §e" + command.getName() + " §8×§7§m------------------«");

    }

}
