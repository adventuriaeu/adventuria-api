package de.fabtopf.contray.adventuria.api.utils.arango;

import com.arangodb.ArangoCollection;
import com.arangodb.ArangoCursor;
import com.arangodb.ArangoDatabase;
import com.arangodb.entity.BaseDocument;
import de.fabtopf.contray.adventuria.api.AdviAPI;

import java.util.HashMap;
import java.util.Map;

public class CollectionManager<Writable extends ArangoWritable<KeyType>, KeyType> {

    protected ArangoCollection collection;
    protected Map<KeyType, Writable> cache;
    protected CacheCondition<Writable, KeyType> cacheCondition;
    protected WritableGenerator<Writable, KeyType> generator;

    public CollectionManager(String collection, WritableGenerator<Writable, KeyType> generator) {
        this(collection, generator, AdviAPI.getInstance().getArangoDatabase());
    }

    public CollectionManager(String collection, WritableGenerator<Writable, KeyType> generator, boolean enableCache) {
        this(collection, generator, AdviAPI.getInstance().getArangoDatabase(), enableCache, new CacheCondition<Writable, KeyType>() {

            @Override
            public boolean checkCache(KeyType key, Writable obj) {
                return false;
            }

        });
    }

    public CollectionManager(String collection, WritableGenerator<Writable, KeyType> generator, boolean enableCache, CacheCondition<Writable, KeyType> cacheCondition) {
        this(collection, generator, AdviAPI.getInstance().getArangoDatabase(), enableCache, cacheCondition);
    }

    public CollectionManager(String collection, WritableGenerator<Writable, KeyType> generator, ArangoDatabase database) {
        this(collection, generator, database, true, new CacheCondition<Writable, KeyType>() {

            @Override
            public boolean checkCache(KeyType key, Writable obj) {
                return false;
            }

        });
    }

    public CollectionManager(String collection, WritableGenerator<Writable, KeyType> generator, ArangoDatabase database, boolean enableCache, CacheCondition<Writable, KeyType> cacheCondition) {
        if(enableCache) {
            this.cache = new HashMap<>();
            this.cacheCondition = cacheCondition;
        }

        if(!database.collection(collection).exists())
            database.createCollection(collection);

        this.collection = database.collection(collection);
        this.generator = generator;
    }

    public Writable get(KeyType key) {
        if(this.cache != null && this.cache.containsKey(key))
            return this.cache.get(key);

        Writable value = this.getNoCached(key);

        if(value != null && this.cache != null)
            if(this.cacheCondition.checkCache(key, value))
                this.cache.put(value.getKey(), value);

        return value;
    }

    protected Writable getOnce(KeyType key) {
        if(this.cache != null && this.cache.containsKey(key))
            return this.cache.get(key);

        return this.getNoCached(key);
    }

    public Writable getNoCached(KeyType key) {
        BaseDocument baseDocuemnt = this.collection.getDocument(key.toString(), BaseDocument.class);

        if(baseDocuemnt != null) {
            Writable value = this.generator.generate(key);
            value.read(baseDocuemnt);

            return value;
        }

        return null;
    }

    public boolean saveCached(KeyType key) {
        if(this.cache == null)
            return false;

        return this.save(this.cache.get(key));
    }

    public boolean save(Writable data) {
        if(data == null) {
            System.err.println("[AdviAPI] CollectionManager -> Save-Value is null");
            return false;
        }

        BaseDocument document = new BaseDocument();
        document.setKey(data.getKey().toString());
        data.save(document);

        this.collection.replaceDocument(data.getKey().toString(), document);

        if(this.cache != null)
            if(this.cache.containsKey(data.getKey()))
                this.cache.put(data.getKey(), data);

        return true;
    }

    public void delete(KeyType key) {
        this.collection.deleteDocument(key.toString());
        this.uncache(key);
    }

    public void register(KeyType key) {
        Writable data = this.generator.generate(key);
        BaseDocument document = new BaseDocument();
        document.setKey(key.toString());
        data.save(document);
        this.collection.insertDocument(document);
    }

    public void uncache(KeyType key) {
        if(this.cache != null)
            this.cache.remove(key);
    }

    public void clearCache() {
        if(this.cache != null)
            this.cache.clear();
    }

    protected void closeCursor(ArangoCursor<?> cursor) {
        try {
            cursor.close();
        } catch(Exception e) {

        }
    }

    public interface CacheCondition<Writable extends ArangoWritable<KeyType>, KeyType> {
        boolean checkCache(KeyType key, Writable obj);
    }

}
