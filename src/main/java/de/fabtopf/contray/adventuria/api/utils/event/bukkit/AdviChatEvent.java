package de.fabtopf.contray.adventuria.api.utils.event.bukkit;

import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.Map;

@Getter
public class AdviChatEvent extends Event implements Cancellable {

    @Getter
    private static final HandlerList handlerList = new HandlerList();

    private NetworkPlayer invoker;
    @Setter private String message;
    private String channel;

    private boolean externalTrigger;
    private boolean command;
    @Setter private boolean cancelled;

    private Map<String, Object> info;

    public AdviChatEvent(NetworkPlayer invoker, String message, String channel, Map<String, Object> info) {
        this(invoker, message, channel, info, false);
    }

    public AdviChatEvent(NetworkPlayer invoker, String message, String channel, Map<String, Object> info, boolean externalTrigger) {
        this(invoker, message, channel, info, externalTrigger, false);
    }

    public AdviChatEvent(NetworkPlayer invoker, String message, String channel, Map<String, Object> info, boolean externalTrigger, boolean command) {
        this.invoker = invoker;
        this.message = message;
        this.channel = channel;
        this.externalTrigger = externalTrigger;
        this.command = command;
        this.info = info;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

}
