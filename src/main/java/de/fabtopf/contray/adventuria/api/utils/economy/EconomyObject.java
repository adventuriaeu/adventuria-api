package de.fabtopf.contray.adventuria.api.utils.economy;

import java.util.Map;
import java.util.UUID;

public interface EconomyObject {

    long depositMoney(UUID currency, long value);

    long withdrawMoney(UUID currency, long value);

    long setMoney(UUID currency, long value);

    long balanceMoney(UUID currency);

    Map<UUID, Long> transformMoney(UUID fromCurrency, UUID toCurrency, long value);

}
