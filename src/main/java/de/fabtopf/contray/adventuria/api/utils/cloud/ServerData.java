package de.fabtopf.contray.adventuria.api.utils.cloud;

import de.dytanic.cloudnet.lib.server.ServerState;
import de.dytanic.cloudnet.lib.server.info.ServerInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public class ServerData implements Comparable<ServerData> {

    private String name;
    private Integer number;
    private String template;

    @Setter
    private Integer maxPlayerCount;

    @Setter
    private Integer currentPlayerCount;

    public ServerData(ServerInfo info) {
        this(info.getServiceId().getServerId(),
                info.getServiceId().getId(),
                info.getTemplate().getName(),
                info.getMaxPlayers(),
                info.getOnlineCount());
    }

    @Override
    public int compareTo(ServerData data) {
        if(this.template.equals(data.template))
            return this.number.compareTo(data.number);
        else
            return this.template.compareTo(data.template);
    }

    @Getter
    @AllArgsConstructor
    public enum SignState {
        ONLINE(ServerState.LOBBY),
        STATIC(ServerState.INGAME),
        INGAME(ServerState.INGAME),
        OFFLINE(ServerState.OFFLINE);

        private ServerState equivalent;

        static SignState byServerState(ServerState serverState) {
            for(SignState state : SignState.values())
                if(state.getEquivalent() == serverState)
                    return state;

            return SignState.ONLINE;
        }
    }

}
