package de.fabtopf.contray.adventuria.api.internal.bungee;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.fabtopf.contray.adventuria.api.AdviAPIBungee;
import de.fabtopf.contray.adventuria.api.internal.AdviConfiguration;
import de.fabtopf.contray.adventuria.api.internal.PluginSupport;
import de.fabtopf.contray.adventuria.api.internal.bungee.command.PermissionCommand;
import de.fabtopf.contray.adventuria.api.internal.bungee.listener.CloudNetListener;
import de.fabtopf.contray.adventuria.api.internal.bungee.listener.ProxiedPlayerListener;
import de.fabtopf.contray.adventuria.api.utils.Language;
import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;

@Getter
public class BungeeCore extends Plugin {

    @Getter
    private static BungeeCore instance;

    private Language language;
    private AdviAPIBungee api;

    private Gson gson;

    private boolean useAuthMe;

    @Override
    public void onLoad() {
        PluginSupport.loadFiles(new File("dependencies"), ClassLoader.getSystemClassLoader());
    }

    @Override
    public void onEnable() {
        BungeeCore.instance = this;

        this.language = new Language("api-messages.properties", this.getClass());

        this.gson = new GsonBuilder().setPrettyPrinting().create();

        this.api = new AdviAPIBungee(this.loadConfig());

        this.getProxy().getPluginManager().registerCommand(this, new PermissionCommand());

        this.getProxy().getPluginManager().registerListener(this, new CloudNetListener());
        this.getProxy().getPluginManager().registerListener(this, new ProxiedPlayerListener());
    }

    @Override
    public void onDisable() {
        this.getProxy().getPlayers().stream().map(ProxiedPlayer::getUniqueId).forEach(ProxiedPlayerListener::handleQuit);
        this.api.cleanup();
    }

    public AdviConfiguration loadConfig() {
        AdviConfiguration config = new AdviConfiguration();

        try {

            File configFile = new File(getDataFolder(), "config.yml");
            if(!configFile.exists()) {
                if(!this.getDataFolder().exists())
                    this.getDataFolder().mkdirs();

                configFile.createNewFile();

                Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
                try {

                    configuration.set("database.arango.host", "127.0.0.1");
                    configuration.set("database.arango.port", 8529);
                    configuration.set("database.arango.user", "adventuria");
                    configuration.set("database.arango.password", "**********");
                    configuration.set("database.arango.database", "adventuria");
                    configuration.set("database.redis.host", "127.0.0.1");
                    configuration.set("database.redis.port", 6379);
                    configuration.set("database.redis.password", "**********");

                    configuration.set("useAuthMe", false);

                    ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, configFile);

                } catch (Exception e) {

                    e.printStackTrace();

                }
            }

            Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);

            config.setArangoHost(configuration.getString("database.arango.host"));
            config.setArangoPort(configuration.getInt("database.arango.port"));
            config.setArangoUser(configuration.getString("database.arango.user"));
            config.setArangoPassword(configuration.getString("database.arango.password"));
            config.setArangoDatabase(configuration.getString("database.arango.database"));
            config.setRedisHost(configuration.getString("database.redis.host"));
            config.setRedisPassword(configuration.getString("database.redis.password"));
            config.setRedisPort(configuration.getInt("database.redis.port"));

            this.useAuthMe = configuration.getBoolean("useAuthMe");

        } catch (Exception e) {

            e.printStackTrace();

        }

        return config;
    }

}
