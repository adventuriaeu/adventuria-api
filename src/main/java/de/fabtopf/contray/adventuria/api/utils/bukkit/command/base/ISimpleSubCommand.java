package de.fabtopf.contray.adventuria.api.utils.bukkit.command.base;

import org.bukkit.command.CommandSender;

public interface ISimpleSubCommand {

    boolean checkSender(CommandSender sender);
    boolean checkPermission(CommandSender sender);
    boolean checkArguments(String[] args);

    boolean trigger(CommandSender sender, SimpleCommand command, String label, String[] args);

    String getPermission();

    String[] getRequiredArgs();
    String[] getHelpMessageParams();

    boolean isShowInHelp();

    int getDisplay();

    Class<? extends CommandSender>[] getAllowedSenders();

}
