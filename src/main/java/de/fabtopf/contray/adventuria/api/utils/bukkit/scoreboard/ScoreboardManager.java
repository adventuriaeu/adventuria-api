package de.fabtopf.contray.adventuria.api.utils.bukkit.scoreboard;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.player.LocalPlayer;
import de.fabtopf.contray.adventuria.api.player.PlayerProperty;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.*;

@Getter
public class ScoreboardManager implements Listener {

    protected final static char[] teams =
            new char[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e'};

    private final Map<String, AdviScoreboard> scoreboards = new HashMap<>();
    private final Map<String, ScoreboardValue> valueMap = new HashMap<>();
    private final JavaPlugin plugin;

    public ScoreboardManager(JavaPlugin plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
        Bukkit.getOnlinePlayers().forEach(p -> initPlayer(p, true));
    }

    private void initPlayer(Player player) {
        initPlayer(player, false);
    }

    private void initPlayer(Player player, boolean init) {
        if(init || player.getScoreboard() == null)
            player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());

        AdviScoreboard template;
        if((template = getScoreboard(player)) == null)
            return;

        updateScoreboard(player, template);
    }

    public void updateScoreboard() {
        for(Player player : Bukkit.getOnlinePlayers())
            this.updateScoreboard(player);
    }

    public void updateScoreboard(Player player) {
        updateScoreboard(player, getScoreboard(player));
    }

    public void updateScoreboard(Player player, AdviScoreboard template) {
        updateScoreboard(player, template, player.getScoreboard());
    }

    public void updateScoreboard(Player player, AdviScoreboard template, Scoreboard scoreboard) {
        if(template == null || template.getName().equals("custom"))
            return;

        if(template.getName().equals("none") && player.getScoreboard().getObjective(DisplaySlot.SIDEBAR) != null) {
            player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).unregister();
            return;
        }

        for (Team item : player.getScoreboard().getTeams()) {
            Team team;
            if ((team = item).getName().matches("value-[0-9a-e]"))
                team.unregister();
        }

        int counter = teams.length - (template.getValues().size() * 3 + 3);

        putValue(template, scoreboard, "§8§m----", "§8§m-----------", counter++);
        putValue(template, scoreboard, "§a§l", "", counter ++);

        for (ScoreboardValue value : template.getValues()) {
            putValue(template, scoreboard, "§8§l▰§7▰ ", value.getName(player), counter ++);
            putValue(template, scoreboard, "§8➜ §d", value.getValue(player), counter ++);
            putValue(template, scoreboard, "§a§l", "", counter ++);
        }

        putValue(template, scoreboard, "§8§m----", "§8§m-----------", counter++);
    }

    private void putValue(AdviScoreboard template, Scoreboard scoreboard, String prefix, String value, int i) {
        if(scoreboard.getObjective(DisplaySlot.SIDEBAR) == null)
            initScoreboard(template, scoreboard);

        Team team;
        if((team = scoreboard.getTeam("value-" + teams[i])) == null)
            team = scoreboard.registerNewTeam("value-" + teams[i]);

        team.setPrefix(prefix);
        team.setSuffix(value);
        team.addEntry("§" + teams[i]);

        scoreboard.getObjective(DisplaySlot.SIDEBAR).getScore("§" + teams[i]).setScore(teams.length - i);
    }

    private void initScoreboard(AdviScoreboard template, Scoreboard scoreboard) {
        if(scoreboard.getObjective("scoreboard") != null)
            scoreboard.getObjective("scoreboard").unregister();

        Objective objective = scoreboard.registerNewObjective("scoreboard", "manager-" + template.getName());
        objective.setDisplayName(template.getTitle()[0]);
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        Team team;
        for(char c : teams) {
            team = scoreboard.getTeam("value-" + c);

            if(team == null)
                team = scoreboard.registerNewTeam("value-" + c);

            team.setPrefix("");
            team.setSuffix("");
        }
    }

    private AdviScoreboard getScoreboard(Player player) {

        LocalPlayer localPlayer = AdviAPI.getInstance().getPlayerProvider().getLocal(player.getUniqueId());
        AdviScoreboard scoreboard;
        if((scoreboard = localPlayer.getScoreboard()) == null) {

            List<ScoreboardValue> values;
            if((values = AdviAPI.getInstance().getPlayerProvider().get(player.getUniqueId()).getProperty(PlayerProperty.SCOREBOARD).get(AdviAPI.getInstance().getCloudUtilities().getServerName())) == null) {

                Optional<AdviScoreboard> optional = scoreboards.values().stream().filter(AdviScoreboard::isDefaultScoreboard).findFirst();
                if (optional.isPresent())
                    scoreboard = optional.get();

            } else {

                scoreboard = new AdviScoreboard(this, UUID.randomUUID().toString(), false, "§2§lAdvi§a§lStats");
                scoreboard.getValues().addAll(values);

            }

            localPlayer.setScoreboard(scoreboard);
        }

        return scoreboard;
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onJoin(PlayerJoinEvent event) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(BukkitCore.getInstance(), () -> initPlayer(event.getPlayer(), true));
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        for(AdviScoreboard scoreboard : scoreboards.values())
            for (ScoreboardValue value : scoreboard.getValues())
                if (value instanceof DynamicScoreboardValue)
                    ((DynamicScoreboardValue) value).removeValue(event.getPlayer());
    }

}
