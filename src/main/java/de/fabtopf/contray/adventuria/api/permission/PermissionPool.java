package de.fabtopf.contray.adventuria.api.permission;

import com.arangodb.ArangoCursor;
import com.arangodb.ArangoDatabase;
import com.arangodb.util.MapBuilder;
import com.arangodb.velocypack.VPackSlice;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.utils.arango.CollectionManager;
import de.fabtopf.contray.adventuria.api.utils.arango.WritableGenerator;
import de.fabtopf.contray.adventuria.api.utils.cloud.ChannelMessageDocument;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;

@Getter
public class PermissionPool extends CollectionManager<PermissionGroup, String> {

    public PermissionPool(String collection, ArangoDatabase database) {
        super(collection, new WritableGenerator<PermissionGroup, String>() {
            @Override
            public PermissionGroup generate(String key) {
                return new PermissionGroup(key);
            }
        }, database, true, (key, obj) -> true);
    }

    public List<String> getGroupNames() {
        return getGroupNames(false);
    }

    public List<String> getGroupNames(boolean fetch) {
        List<String> groups = new ArrayList<>();

        if(fetch) {
            ArangoCursor<VPackSlice> cursor = AdviAPI.getInstance().getArangoDatabase().query(
                    "FOR doc IN @@collection RETURN doc._key",
                    new MapBuilder().put("@collection", collection.name()).get(),
                    null,
                    VPackSlice.class);

            while (cursor.hasNext())
                groups.add(cursor.next().getAsString());

            this.closeCursor(cursor);
        } else {
            groups.addAll(cache.keySet());
        }

        return groups;
    }

    public List<PermissionGroup> getGroups() {
        return getGroupNames().stream().map(this::get).filter(Objects::nonNull).sorted().collect(Collectors.toList());
    }

    public PermissionGroup getDefaultGroup() {
        List<PermissionGroup> groups = getGroups().stream().filter(g -> g.getAvailableOn().isEmpty() && g.isDefaultGroup()).collect(Collectors.toList());
        return groups.isEmpty() ? new PermissionGroup.DefaultPermissionGroup(this) : groups.get(0);
    }

    public PermissionGroup getDefaultGroup(String serverGroup) {
        List<PermissionGroup> groups = getGroups().stream().filter(g -> g.getAvailableOn().contains(serverGroup.toLowerCase()) && g.isDefaultGroup()).collect(Collectors.toList());
        return groups.isEmpty() ? getDefaultGroup() : groups.get(0);
    }

    boolean hasPermission(String permission, Map<String, Boolean> permissions) {
        if(permission == null || permissions == null)
            return false;

        boolean invert;
        if(invert = permission.startsWith("-"))
            permission = permission.replaceFirst("-", "");

        if(permission.equals("bukkit.broadcast"))
            return true;

        if(permission.matches("cloudnet.joinpower.\\d\\*")) {
            int power = Integer.valueOf(permission.replaceFirst("cloudnet.joinpower.", ""));

            List<Integer> list = permissions.entrySet().stream().filter(e -> e.getValue() && e.getKey().startsWith("cloudnet.joinpower.") && e.getKey().matches("cloudnet.joinpower.\\d\\*"))
                    .map(e -> Integer.valueOf(e.getKey().replaceFirst("cloudnet.joinpower.", ""))).sorted().collect(Collectors.toList());

            if(!list.isEmpty()) {
                Collections.reverse(list);
                if (power <= list.get(0))
                    return true;
            }
        }

        if(permissions.containsKey(permission))
            return invert != permissions.get(permission);

        return invert != hasWildcardPermission(permission, permissions);
    }

    boolean hasWildcardPermission(String permission, Map<String, Boolean> permissions) {
        List<String> permissionParts = Arrays.asList(permission.split("\\."));

        for(int i = permissionParts.size() - 1; i > 0; i--)
            if (permissions.containsKey(String.join(".", permissionParts.subList(0, i)) + ".*"))
                return permissions.get(String.join(".", permissionParts.subList(0, i)) + ".*");

        return permissions.getOrDefault("*", false);
    }

    public boolean createGroup(String group) {
        if(getGroupNames().contains(group))
            return false;

        this.register(group);
        return true;
    }

    public boolean deleteGroup(String group) {

        PermissionGroup permissionGroup;
        if((permissionGroup = get(group)) == null)
            return false;

        if(permissionGroup.equals(getDefaultGroup()))
            return false;

        this.delete(group);
        return true;
    }

    public boolean addPermission(PermissionGroup group, String permission) {
        if(group == null || permission == null)
            return false;

        String perm = permission.startsWith("-") ? permission.replaceFirst("-", "") : permission;
        group.getGlobalPermissions().put(perm.toLowerCase(), !permission.startsWith("-"));
        return true;
    }

    public boolean addPermission(PermissionGroup group, String permission, String serverGroup) {
        if(group == null || permission == null || serverGroup == null)
            return false;

        String perm = permission.startsWith("-") ? permission.replaceFirst("-", "") : permission;
        Map<String, Boolean> serverGroupPermissions = group.getServerGroupPermissions().getOrDefault(serverGroup, new HashMap<>());
        serverGroupPermissions.put(perm.toLowerCase(), !permission.startsWith("-"));

        group.getServerGroupPermissions().put(serverGroup, serverGroupPermissions);
        return true;
    }

    public boolean addPermission(PermissionGroup group, String permission, String serverGroup, String world) {
        if(group == null || permission == null || serverGroup == null || world == null)
            return false;

        String perm = permission.startsWith("-") ? permission.replaceFirst("-", "") : permission;
        Map<String, Map<String, Boolean>> serverGroupPermissions = group.getWorldPermissions().getOrDefault(serverGroup, new HashMap<>());
        Map<String, Boolean> worldPermissions = serverGroupPermissions.getOrDefault(world, new HashMap<>());
        worldPermissions.put(perm.toLowerCase(), !permission.startsWith("-"));

        serverGroupPermissions.put(world, worldPermissions);
        group.getWorldPermissions().put(serverGroup, serverGroupPermissions);
        return true;
    }

    public boolean removePermission(PermissionGroup group, String permission) {
        if(group == null || permission == null)
            return false;

        String perm = permission.startsWith("-") ? permission.replaceFirst("-", "") : permission;
        group.getGlobalPermissions().remove(perm.toLowerCase());
        return true;
    }

    public boolean removePermission(PermissionGroup group, String permission, String serverGroup) {
        if(group == null || permission == null || serverGroup == null)
            return false;

        String perm = permission.startsWith("-") ? permission.replaceFirst("-", "") : permission;
        Map<String, Boolean> serverGroupPermissions = group.getServerGroupPermissions().getOrDefault(serverGroup, new HashMap<>());
        serverGroupPermissions.remove(perm.toLowerCase());

        group.getServerGroupPermissions().put(serverGroup, serverGroupPermissions);
        return true;
    }

    public boolean removePermission(PermissionGroup group, String permission, String serverGroup, String world) {
        if(group == null || permission == null || serverGroup == null || world == null)
            return false;

        String perm = permission.startsWith("-") ? permission.replaceFirst("-", "") : permission;
        Map<String, Map<String, Boolean>> serverGroupPermissions = group.getWorldPermissions().getOrDefault(serverGroup, new HashMap<>());
        Map<String, Boolean> worldPermissions = serverGroupPermissions.getOrDefault(world, new HashMap<>());
        worldPermissions.remove(perm.toLowerCase());

        serverGroupPermissions.put(world, worldPermissions);
        group.getWorldPermissions().put(serverGroup, serverGroupPermissions);
        return true;
    }

    public boolean addImplementedGroup(PermissionGroup group, PermissionGroup implement) {
        if(group == null || implement == null)
            return false;

        if(group.getImplementedGroups().contains(implement))
            return false;

        List<PermissionGroup> groups = group.getImplementedGroups();
        groups.add(implement);

        group.setImplementedGroups(groups.stream().map(PermissionGroup::getName).collect(Collectors.toList()));
        return true;
    }

    public boolean removeImplementedGroup(PermissionGroup group, PermissionGroup implement) {
        if(group == null || implement == null)
            return false;

        if(!group.getImplementedGroups().contains(implement))
            return false;

        List<PermissionGroup> groups = group.getImplementedGroups();
        groups.remove(implement);

        group.setImplementedGroups(groups.stream().map(PermissionGroup::getName).collect(Collectors.toList()));
        return true;
    }

    public void setDefaultGroup(PermissionGroup group) {
        if(group == null)
            return;

        getGroups().stream().filter(g -> g.isDefaultGroup() && g.getAvailableOn().isEmpty()).peek(g -> {
           g.setDefaultGroup(false);
           sendGroupUpdate(g);
        });

        group.setDefaultGroup(true);
    }

    public boolean setGroupTagId(PermissionGroup group, int tagId) {
        if(group == null || tagId < 0)
            return false;

        group.setTagId(tagId);
        return true;
    }

    public boolean setGroupJoinPower(PermissionGroup group, int joinPower) {
        if(group == null || joinPower < 0)
            return false;

        group.setJoinPower(joinPower);
        return true;
    }

    public void setGroupOption(PermissionGroup group, String option, Object value) {
        if(group == null || option == null)
            return;

        if(value == null) group.getOptions().remove(option);
        else group.getOptions().put(option, value);
    }

    public boolean addGroupAvailability(PermissionGroup group, String serverGroup) {
        if(group == null || serverGroup == null)
            return false;

        if(group.getAvailableOn().contains(serverGroup.toLowerCase()))
            return false;

        group.getAvailableOn().add(serverGroup.toLowerCase());
        return true;
    }

    public boolean removeGroupAvailability(PermissionGroup group, String serverGroup) {
        if(group == null || serverGroup == null)
            return false;

        if(!group.getAvailableOn().contains(serverGroup.toLowerCase()))
            return false;

        group.getAvailableOn().remove(serverGroup.toLowerCase());
        return true;
    }

    public void clearGroupAvailability(PermissionGroup group) {
        if(group == null)
            return;

        group.getAvailableOn().clear();
    }

    public void setGroupPrefix(PermissionGroup group, String prefix) {
        if(group == null)
            return;

        group.setPrefix(prefix);
    }

    public void setGroupSuffix(PermissionGroup group, String suffix) {
        if(group == null)
            return;

        group.setSuffix(suffix);
    }

    public void setGroupDisplay(PermissionGroup group, String display) {
        if(group == null)
            return;

        group.setDisplay(display);
    }

    public void sendGroupUpdate(PermissionGroup group) {
        this.save(group);
        AdviAPI.getInstance().getCloudUtilities().sendProxyChannelMessage(
                "advi-group", "update",
                new ChannelMessageDocument(new Document("group", group.getKey())
                        .append("collection", this.collection.name())));
        AdviAPI.getInstance().getCloudUtilities().sendServerChannelMessage(
                "advi-group", "update",
                new ChannelMessageDocument(new Document("group", group.getKey())
                        .append("collection", this.collection.name())));
    }

}
