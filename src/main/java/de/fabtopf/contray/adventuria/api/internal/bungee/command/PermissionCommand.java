package de.fabtopf.contray.adventuria.api.internal.bungee.command;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bungee.BungeeCore;
import de.fabtopf.contray.adventuria.api.permission.PermissionGroup;
import de.fabtopf.contray.adventuria.api.permission.PermissionGroupEntity;
import de.fabtopf.contray.adventuria.api.permission.PermissionPool;
import de.fabtopf.contray.adventuria.api.permission.PermissionUser;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.utils.Language;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.command.ConsoleCommandSender;

import java.util.*;
import java.util.stream.Collectors;

public class PermissionCommand extends AdviCommand {

    public PermissionCommand() {
        super("perm", "adventuria.api.command.perm", "perms", "permission", "permissions");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        PermissionPool pool = AdviAPI.getInstance().getPermissionPool();
        Language lang = BungeeCore.getInstance().getLanguage();

        if(args.length > 0) {

            switch(args[0].toLowerCase()) {

                case "group":
                case "groups":

                    if(args.length == 1) {

                        List<PermissionGroup> groups = pool.getGroups();
                        sender.sendMessage(TextComponent.fromLegacyText(
                                lang.getPhraseContent("command-perm-groups",
                                        groups.size(),
                                        String.join("\n",
                                                groups.stream()
                                                        .map(g -> lang.getPhraseContent("command-perm-groupdisplay",
                                                                g.getName(), g.getJoinPower(), g.getTagId(),
                                                                String.join(", ",
                                                                        g.getImplementedGroups().stream().map(PermissionGroup::getName)
                                                                                .collect(Collectors.toList()))))
                                                        .collect(Collectors.toList())))));
                        return;

                    } else if(args.length == 2) {

                        switch(args[1].toLowerCase()) {

                            case "list":
                            case "show":

                                List<PermissionGroup> groups = pool.getGroups();
                                sender.sendMessage(TextComponent.fromLegacyText(
                                        lang.getPhraseContent("command-perm-groups",
                                                groups.size(),
                                                String.join("\n",
                                                        groups.stream()
                                                                .map(g -> lang.getPhraseContent("command-perm-groupdisplay",
                                                                        g.getName(), g.getJoinPower(), g.getTagId(),
                                                                        String.join(", ",
                                                                                g.getImplementedGroups().stream().map(PermissionGroup::getName)
                                                                                        .collect(Collectors.toList()))))
                                                                .collect(Collectors.toList())))));

                                break;

                            default:

                                PermissionGroup group = null;
                                for(PermissionGroup search : pool.getGroups())
                                    if(search.getName().equalsIgnoreCase(args[1]))
                                        group = search;

                                if(group == null) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                    return;
                                }

                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-info",
                                        group.getName(),
                                        group.isDefaultGroup(),
                                        group.getTagId(),
                                        group.getJoinPower(),
                                        makeColorCodesReadable(group.getDisplay()),
                                        makeColorCodesReadable(group.getPrefix()),
                                        makeColorCodesReadable(group.getSuffix()),
                                        group.getAvailableOn().isEmpty() ? "jeder Server" : String.join(", ", group.getAvailableOn()),
                                        String.join(", ", group.getImplementedGroups().stream().map(PermissionGroup::getName).collect(Collectors.toList())),
                                        String.join("\n", group.getOptions().entrySet().stream().map(e -> lang.getPhraseContent("command-perm-group-settings-line", e.getKey(), e.getValue().toString())).collect(Collectors.toList())))));

                                break;

                        }

                    } else if(args.length == 3) {

                        PermissionGroup group = null;
                        switch(args[2].toLowerCase()) {

                            case "all":


                                for(PermissionGroup search : pool.getGroups())
                                    if(search.getName().equalsIgnoreCase(args[1]))
                                        group = search;

                                if(group == null) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                    return;
                                }

                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-display",
                                        group.getName(),
                                        group.isDefaultGroup(),
                                        group.getTagId(),
                                        group.getJoinPower(),
                                        makeColorCodesReadable(group.getDisplay()),
                                        makeColorCodesReadable(group.getPrefix()),
                                        makeColorCodesReadable(group.getSuffix()),
                                        group.getAvailableOn().isEmpty() ? "jeder Server" : String.join(", ", group.getAvailableOn()),
                                        String.join(", ", group.getImplementedGroups().stream().map(PermissionGroup::getName).collect(Collectors.toList())),
                                        String.join("\n", group.getOptions().entrySet().stream().map(e -> lang.getPhraseContent("command-perm-group-settings-line", e.getKey(), e.getValue().toString())).collect(Collectors.toList())),
                                        formatPermissionMap(group.getGlobalPermissions(), 2),
                                        formatLocationBasedPermissions(group.getServerGroupPermissions(), group.getWorldPermissions()))));

                                break;

                            case "info":

                                for(PermissionGroup search : pool.getGroups())
                                    if(search.getName().equalsIgnoreCase(args[1]))
                                        group = search;

                                if(group == null) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                    return;
                                }

                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-info",
                                        group.getName(),
                                        group.isDefaultGroup(),
                                        group.getTagId(),
                                        group.getJoinPower(),
                                        makeColorCodesReadable(group.getDisplay()),
                                        makeColorCodesReadable(group.getPrefix()),
                                        makeColorCodesReadable(group.getSuffix()),
                                        group.getAvailableOn().isEmpty() ? "jeder Server" : String.join(", ", group.getAvailableOn()),
                                        String.join(", ", group.getImplementedGroups().stream().map(PermissionGroup::getName).collect(Collectors.toList())),
                                        String.join("\n", group.getOptions().entrySet().stream().map(e -> lang.getPhraseContent("command-perm-group-settings-line", e.getKey(), e.getValue().toString())).collect(Collectors.toList())))));

                                break;

                            case "create":

                                if(!(sender instanceof ConsoleCommandSender)) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-console-only")));
                                    return;
                                }

                                if(pool.createGroup(args[1])) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-create", args[1])));
                                    return;
                                }

                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-creationFailed", args[1])));
                                break;

                            case "del":
                            case "delete":

                                if(!(sender instanceof ConsoleCommandSender)) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-console-only")));
                                    return;
                                }

                                for(PermissionGroup search : pool.getGroups())
                                    if(search.getName().equalsIgnoreCase(args[1]))
                                        group = search;

                                if(group == null) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                    return;
                                }

                                if(pool.deleteGroup(args[1])) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-delete", args[1])));
                                    pool.sendGroupUpdate(group);
                                    return;
                                }

                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-removalFailed", args[1])));
                                break;

                            case "default":

                                if(!(sender instanceof ConsoleCommandSender)) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-console-only")));
                                    return;
                                }

                                for(PermissionGroup search : pool.getGroups())
                                    if(search.getName().equalsIgnoreCase(args[1]))
                                        group = search;

                                if(group == null) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                    return;
                                }

                                pool.setDefaultGroup(group);
                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-default-set")));

                                pool.save(group);
                                pool.sendGroupUpdate(group);
                                return;

                            default:
                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                return;

                        }

                    } else {

                        StringBuilder value = new StringBuilder();
                        PermissionGroup group = null;
                        switch(args[2].toLowerCase()) {

                            case "perm":
                            case "perms":
                            case "permission":
                            case "permissions":

                                for(PermissionGroup search : pool.getGroups())
                                    if(search.getName().equalsIgnoreCase(args[1]))
                                        group = search;

                                if(group == null) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                    return;
                                }

                                switch(args[3].toLowerCase()) {

                                    case "add":

                                        if(args.length == 5) {

                                            pool.addPermission(group, args[4].toLowerCase());
                                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-permission-added-global",
                                                    group.getName(), args[4].toLowerCase())));

                                            pool.save(group);
                                            pool.sendGroupUpdate(group);

                                            return;
                                        } else if(args.length == 6) {

                                            pool.addPermission(group, args[4].toLowerCase(), args[5]);
                                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-permission-added-group",
                                                    group.getName(), args[4].toLowerCase(), args[5])));

                                            pool.save(group);
                                            pool.sendGroupUpdate(group);

                                            return;
                                        } else {

                                            pool.addPermission(group, args[4].toLowerCase(), args[5], args[6]);
                                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-permission-added-world",
                                                    group.getName(), args[4].toLowerCase(), args[5], args[6])));

                                            pool.save(group);
                                            pool.sendGroupUpdate(group);

                                            return;
                                        }

                                    case "rem":
                                    case "remove":

                                        if(args.length == 5) {

                                            if(pool.removePermission(group, args[4].toLowerCase())) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-permission-removed-global",
                                                        group.getName(), args[4].toLowerCase())));

                                                pool.save(group);
                                                pool.sendGroupUpdate(group);
                                            } else
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-permission-failed-remove-global",
                                                        group.getName(), args[4].toLowerCase())));

                                            return;
                                        } else if(args.length == 6) {

                                            if(pool.removePermission(group, args[4].toLowerCase(), args[5])) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-permission-removed-group",
                                                        group.getName(), args[4].toLowerCase(), args[5])));

                                                pool.save(group);
                                                pool.sendGroupUpdate(group);
                                            } else
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-permission-failed-remove-group",
                                                        group.getName(), args[4].toLowerCase(), args[5])));

                                            return;
                                        } else {

                                            if(pool.removePermission(group, args[4].toLowerCase(), args[5], args[6])) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-permission-removed-world",
                                                        group.getName(), args[4].toLowerCase(), args[5], args[6])));

                                                pool.save(group);
                                                pool.sendGroupUpdate(group);
                                            } else
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-permission-failed-remove-world",
                                                        group.getName(), args[4].toLowerCase(), args[5], args[6])));

                                            return;
                                        }

                                    default:
                                        sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                        return;

                                }

                            case "parent":
                            case "parents":

                                if(args.length == 5) {

                                    for (PermissionGroup search : pool.getGroups())
                                        if (search.getName().equalsIgnoreCase(args[1])) group = search;

                                    if (group == null) {
                                        sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                        return;
                                    }

                                    List<PermissionGroup> filteredGroups;
                                    switch (args[3].toLowerCase()) {

                                        case "add":

                                            if ((filteredGroups = AdviAPI.getInstance().getPermissionPool().getGroups().stream().filter(s -> s.getName().equalsIgnoreCase(args[4])).collect(Collectors.toList())).isEmpty()) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[4])));
                                                return;
                                            }

                                            if (pool.addImplementedGroup(group, filteredGroups.get(0))) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-parent-added", filteredGroups.get(0).getName(), group.getName())));

                                                pool.save(group);
                                                pool.sendGroupUpdate(group);
                                                return;
                                            }

                                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-parent-failed-add", args[4])));
                                            return;

                                        case "rem":
                                        case "remove":

                                            if ((filteredGroups = AdviAPI.getInstance().getPermissionPool().getGroups().stream().filter(s -> s.getName().equalsIgnoreCase(args[4])).collect(Collectors.toList())).isEmpty()) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[4])));
                                                return;
                                            }

                                            if (pool.removeImplementedGroup(group, filteredGroups.get(0))) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-parent-removed", filteredGroups.get(0).getName(), group.getName())));

                                                pool.save(group);
                                                pool.sendGroupUpdate(group);
                                                return;
                                            }

                                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-parent-failed-remove", args[4])));
                                            return;

                                        default:
                                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                            return;

                                    }

                                } else {

                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                    return;
                                }

                            case "access":
                            case "available":
                            case "availability":

                                if(args.length == 5) {

                                    switch(args[3].toLowerCase()) {

                                        case "add":

                                            for (PermissionGroup search : pool.getGroups())
                                                if (search.getName().equalsIgnoreCase(args[1])) group = search;

                                            if (group == null) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                                return;
                                            }

                                            if (pool.addGroupAvailability(group, args[4])) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-availability-added", group.getName(), args[4])));

                                                pool.save(group);
                                                pool.sendGroupUpdate(group);
                                            } else
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-availability-failed-add", group.getName(), args[4])));

                                            return;

                                        case "rem":
                                        case "remove":

                                            for (PermissionGroup search : pool.getGroups())
                                                if (search.getName().equalsIgnoreCase(args[1])) group = search;

                                            if (group == null) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                                return;
                                            }

                                            if (pool.removeGroupAvailability(group, args[4])) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-availability-removed", group.getName(), args[4])));

                                                pool.save(group);
                                                pool.sendGroupUpdate(group);
                                            } else
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-availability-failed-remove", group.getName(), args[4])));

                                            return;

                                        default:
                                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                            return;

                                    }

                                } else {

                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                    return;
                                }

                            case "setting":
                            case "settings":

                                if(args.length > 4) {

                                    for (PermissionGroup search : pool.getGroups())
                                        if (search.getName().equalsIgnoreCase(args[1])) group = search;

                                    if (group == null) {
                                        sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                        return;
                                    }

                                    for(int i = 4; i < args.length; i++)
                                        value.append(" ").append(args[i]);

                                    pool.setGroupOption(group, args[3].toLowerCase(), value.toString().replaceFirst(" ", "").equals("") ? null : value.toString().replaceFirst(" ", ""));
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-setting-set",
                                            group.getName(), args[3].toLowerCase(), value.toString().replaceFirst(" ", "").equals("") ? "null" : value.toString().replaceFirst(" ", ""))));

                                    pool.save(group);
                                    pool.sendGroupUpdate(group);
                                    return;

                                } else {

                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                    return;
                                }

                            case "tagid":

                                for (PermissionGroup search : pool.getGroups())
                                    if (search.getName().equalsIgnoreCase(args[1])) group = search;

                                if (group == null) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                    return;
                                }

                                int tagId = 0;
                                try {
                                    tagId = Integer.parseInt(args[3]);
                                } catch (Exception e) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("system-command-invalid-datatype", "4", "Zahl")));
                                    return;
                                }

                                pool.setGroupTagId(group, tagId);
                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-tagid-set", group.getName(), args[3])));

                                pool.save(group);
                                pool.sendGroupUpdate(group);
                                return;

                            case "joinpower":

                                for (PermissionGroup search : pool.getGroups())
                                    if (search.getName().equalsIgnoreCase(args[1])) group = search;

                                if (group == null) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                    return;
                                }

                                int joinPower = 0;
                                try {
                                    joinPower = Integer.parseInt(args[3]);
                                } catch (Exception e) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("system-command-invalid-datatype", "4", "Zahl")));
                                    return;
                                }

                                pool.setGroupJoinPower(group, joinPower);
                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-joinpower-set", group.getName(), args[3])));

                                pool.save(group);
                                pool.sendGroupUpdate(group);
                                return;

                            case "prefix":

                                for (PermissionGroup search : pool.getGroups())
                                    if (search.getName().equalsIgnoreCase(args[1])) group = search;

                                if (group == null) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                    return;
                                }

                                for(int i = 3; i < args.length; i++)
                                    value.append(" ").append(args[i]);

                                pool.setGroupPrefix(group, value.toString().replaceFirst(" ", "").equals("\"\"") ? "" : ChatColor.translateAlternateColorCodes('&', value.toString().replaceFirst(" ", "")));
                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-prefix-set", group.getName(), value.toString().replaceFirst(" ", "").equals("\"\"") ? "(kein Präfix)" : ChatColor.translateAlternateColorCodes('&', value.toString().replaceFirst(" ", "")))));

                                pool.save(group);
                                pool.sendGroupUpdate(group);
                                return;

                            case "suffix":

                                for (PermissionGroup search : pool.getGroups())
                                    if (search.getName().equalsIgnoreCase(args[1])) group = search;

                                if (group == null) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                    return;
                                }

                                for(int i = 3; i < args.length; i++)
                                    value.append(" ").append(args[i]);

                                pool.setGroupSuffix(group, value.toString().replaceFirst(" ", "").equals("\"\"") ? "" : ChatColor.translateAlternateColorCodes('&', value.toString().replaceFirst(" ", "")));
                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-suffix-set", group.getName(), value.toString().replaceFirst(" ", "").equals("\"\"") ? "(kein Suffix)" : ChatColor.translateAlternateColorCodes('&', value.toString().replaceFirst(" ", "")))));

                                pool.save(group);
                                pool.sendGroupUpdate(group);
                                return;

                            case "display":

                                for (PermissionGroup search : pool.getGroups())
                                    if (search.getName().equalsIgnoreCase(args[1])) group = search;

                                if (group == null) {
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[1])));
                                    return;
                                }

                                for(int i = 3; i < args.length; i++)
                                    value.append(" ").append(args[i]);

                                pool.setGroupDisplay(group, value.toString().replaceFirst(" ", "").equals("\"\"") ? "" : ChatColor.translateAlternateColorCodes('&', value.toString().replaceFirst(" ", "")));
                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-display-set", group.getName(), value.toString().replaceFirst(" ", "").equals("\"\"") ? "(keine Anzeige)" : ChatColor.translateAlternateColorCodes('&', value.toString().replaceFirst(" ", "")))));

                                pool.save(group);
                                pool.sendGroupUpdate(group);
                                return;

                            default:
                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                return;

                        }

                    }

                    break;

                case "user":

                    if(args.length == 2) {

                        NetworkPlayer player;
                        if((player = AdviAPI.getInstance().getPlayerProvider().get(args[1])) == null) {
                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-player-does-not-exist", args[1])));
                            return;
                        }

                        sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-player-display",
                                player.getName(),
                                player.getUniqueId().toString(),
                                player.getHighestGroup(),
                                player.getPermissionUser().getUnfilteredGroups().stream().map(g -> g.getAvailableOn().isEmpty() ? g.getName() : g.getName() + "*").collect(Collectors.joining(", ")),
                                player.getPermissionUser().getJoinPower(),
                                makeColorCodesReadable(player.getPermissionUser().getDisplay()),
                                makeColorCodesReadable(player.getPermissionUser().getPrefix()),
                                makeColorCodesReadable(player.getPermissionUser().getSuffix()),
                                formatPermissionMap(player.getPermissionUser().getGlobalPermissions(), 2),
                                formatLocationBasedPermissions(player.getPermissionUser().getServerGroupPermissions(), player.getPermissionUser().getWorldPermissions()))));

                        return;

                    } else {

                        NetworkPlayer target;
                        if((target = AdviAPI.getInstance().getPlayerProvider().get(args[1])) == null) {
                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-player-does-not-exist", args[1])));
                            return;
                        }

                        PermissionUser permissionUser = target.getPermissionUser();
                        switch(args[2].toLowerCase()) {

                            case "group":
                            case "groups":

                                if(args.length > 4) {

                                    PermissionGroup group = null;
                                    for (PermissionGroup search : pool.getGroups())
                                        if (search.getName().equalsIgnoreCase(args[4])) group = search;

                                    if (group == null) {
                                        sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-group-not-existent", args[4])));
                                        return;
                                    }

                                    long duration = 0;
                                    if(args.length > 5)
                                        try {
                                            duration = Long.parseLong(args[5]) * 1000 * 60 * 60 * 24;
                                        } catch (Exception e) {
                                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("system-command-invalid-datatype", "6", "Zahl")));
                                            return;
                                        }

                                    switch (args[3].toLowerCase()) {

                                        case "add":

                                            if(permissionUser.addGroup(new PermissionGroupEntity(group.getName(), System.currentTimeMillis(), duration, null))) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-group-added", target.getName(), group.getName())));

                                                AdviAPI.getInstance().getPlayerProvider().save(target);
                                                target.sendPropertyUpdate();
                                            } else
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-group-failed-add", target.getName(), group.getName())));

                                            return;

                                        case "rem":
                                        case "remove":

                                            if(permissionUser.removeGroup(group)) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-group-removed", target.getName(), group.getName())));

                                                AdviAPI.getInstance().getPlayerProvider().save(target);
                                                target.sendPropertyUpdate();
                                            } else
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-group-failed-remove", target.getName(), group.getName())));

                                            return;

                                        case "set":

                                            permissionUser.setGroup(new PermissionGroupEntity(group.getName(), System.currentTimeMillis(), duration, null));
                                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-group-set", target.getName(), group.getName())));

                                            AdviAPI.getInstance().getPlayerProvider().save(target);
                                            target.sendPropertyUpdate();
                                            return;

                                        default:
                                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                            return;

                                    }

                                }

                                break;

                            case "perm":
                            case "perms":
                            case "permission":
                            case "permissions":

                                switch(args[3].toLowerCase()) {

                                    case "add":

                                        if(args.length == 5) {

                                            permissionUser.addPermission(args[4].toLowerCase());
                                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-permission-added-global",
                                                    target.getName(), args[4].toLowerCase())));

                                            AdviAPI.getInstance().getPlayerProvider().save(target);
                                            target.sendPropertyUpdate();
                                            return;
                                        } else if(args.length == 6) {

                                            permissionUser.addPermission(args[4].toLowerCase(), args[5]);
                                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-permission-added-group",
                                                    target.getName(), args[4].toLowerCase(), args[5])));

                                            AdviAPI.getInstance().getPlayerProvider().save(target);
                                            target.sendPropertyUpdate();
                                            return;
                                        } else {

                                            permissionUser.addPermission(args[4].toLowerCase(), args[5], args[6]);
                                            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-permission-added-world",
                                                    target.getName(), args[4].toLowerCase(), args[5], args[6])));

                                            AdviAPI.getInstance().getPlayerProvider().save(target);
                                            target.sendPropertyUpdate();
                                            return;
                                        }

                                    case "rem":
                                    case "remove":

                                        if(args.length == 5) {

                                            if(permissionUser.removePermission(args[4].toLowerCase())) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-permission-removed-global",
                                                        target.getName(), args[4].toLowerCase())));

                                                AdviAPI.getInstance().getPlayerProvider().save(target);
                                                target.sendPropertyUpdate();
                                            } else
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-permission-failed-remove-global",
                                                        target.getName(), args[4].toLowerCase())));

                                            return;
                                        } else if(args.length == 6) {

                                            if(permissionUser.removePermission(args[4].toLowerCase(), args[5])) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-permission-removed-group",
                                                        target.getName(), args[4].toLowerCase(), args[5])));

                                                AdviAPI.getInstance().getPlayerProvider().save(target);
                                                target.sendPropertyUpdate();
                                            } else
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-permission-failed-remove-group",
                                                        target.getName(), args[4].toLowerCase(), args[5])));

                                            return;
                                        } else {

                                            if(permissionUser.removePermission(args[4].toLowerCase(), args[5], args[6])) {
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-permission-removed-world",
                                                        target.getName(), args[4].toLowerCase(), args[5], args[6])));

                                                AdviAPI.getInstance().getPlayerProvider().save(target);
                                                target.sendPropertyUpdate();
                                            } else
                                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-permission-failed-remove-world",
                                                        target.getName(), args[4].toLowerCase(), args[5], args[6])));

                                            return;
                                        }

                                    default:
                                        sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                        return;

                                }

                            case "joinpower":

                                if(args.length == 4) {

                                    int joinPower = 0;
                                    try {
                                        joinPower = Integer.parseInt(args[3]);
                                    } catch (Exception e) {
                                        sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("system-command-invalid-datatype", "4", "Zahl")));
                                        return;
                                    }

                                    permissionUser.setJoinPower(joinPower);
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-joinpower-set", target.getName(), args[3])));

                                    AdviAPI.getInstance().getPlayerProvider().save(target);
                                    target.sendPropertyUpdate();
                                    return;

                                } else {

                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                    return;
                                }

                            case "prefix":

                                if(args.length > 3) {

                                    StringBuilder value = new StringBuilder();
                                    for(int i = 3; i < args.length; i++)
                                        value.append(" ").append(args[i]);

                                    permissionUser.setPrefix(value.toString().replaceFirst(" ", "").equals("\"\"") ? null : ChatColor.translateAlternateColorCodes('&', value.toString().replaceFirst(" ", "")));
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-prefix-set", target.getName(), value.toString().replaceFirst(" ", "").equals("\"\"") ? "(kein Präfix)" : ChatColor.translateAlternateColorCodes('&', value.toString().replaceFirst(" ", "")))));

                                    AdviAPI.getInstance().getPlayerProvider().save(target);
                                    target.sendPropertyUpdate();
                                    return;

                                } else {

                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                    return;
                                }

                            case "suffix":

                                if(args.length > 3) {

                                    StringBuilder value = new StringBuilder();
                                    for(int i = 3; i < args.length; i++)
                                        value.append(" ").append(args[i]);

                                    permissionUser.setSuffix(value.toString().replaceFirst(" ", "").equals("\"\"") ? null : ChatColor.translateAlternateColorCodes('&', value.toString().replaceFirst(" ", "")));
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-suffix-set", target.getName(), value.toString().replaceFirst(" ", "").equals("\"\"") ? "(kein Suffix)" : ChatColor.translateAlternateColorCodes('&', value.toString().replaceFirst(" ", "")))));

                                    AdviAPI.getInstance().getPlayerProvider().save(target);
                                    target.sendPropertyUpdate();
                                    return;

                                } else {

                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                    return;
                                }

                            case "display":

                                if(args.length > 3) {

                                    StringBuilder value = new StringBuilder();
                                    for(int i = 3; i < args.length; i++)
                                        value.append(" ").append(args[i]);

                                    permissionUser.setDisplay(value.toString().replaceFirst(" ", "").equals("\"\"") ? null : ChatColor.translateAlternateColorCodes('&', value.toString().replaceFirst(" ", "")));
                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-user-display-set", target.getName(), value.toString().replaceFirst(" ", "").equals("\"\"") ? "(keine Anzeige)" : ChatColor.translateAlternateColorCodes('&', value.toString().replaceFirst(" ", "")))));

                                    AdviAPI.getInstance().getPlayerProvider().save(target);
                                    target.sendPropertyUpdate();
                                    return;

                                } else {

                                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                    return;
                                }

                            default:
                                sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));
                                return;

                        }
                    }

                    break;

                default:
                    sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));

            }

        } else {

            sender.sendMessage(TextComponent.fromLegacyText(lang.getPhraseContent("command-perm-help")));

        }

    }

    private String makeColorCodesReadable(String input) {
        return input.replace("§", "&");
    }

    private String formatPermissionMap(Map<String, Boolean> permissionMap, int tab) {

        StringBuilder tabString = new StringBuilder();
        for(int i = 0; i < tab; i++)
            tabString.append("  ");

        return String.join("§f\n", permissionMap.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getKey)).map(e -> tabString.toString() + BungeeCore.getInstance().getLanguage().getPhraseContent("command-perm-permission-display", (e.getValue() ? "§7" : "§c") + e.getKey() + ": " + e.getValue())).collect(Collectors.toList()));
    }

    private String formatLocationBasedPermissions(Map<String, Map<String, Boolean>> serverGroupPermissions, Map<String, Map<String, Map<String, Boolean>>> worldPermissions) {
        List<String> servers = new ArrayList<>(serverGroupPermissions.keySet());
        for(String s : worldPermissions.keySet())
            if(!servers.contains(s))
                servers.add(s);

        Collections.sort(servers);

        StringBuilder builder = new StringBuilder();
        for(String server : servers) {
            builder.append("\n    §e").append(server).append(":\n");

            Map<String, Boolean> sgPermissions;
            if((sgPermissions = serverGroupPermissions.get(server)) != null)
                builder.append(formatPermissionMap(sgPermissions, 3)).append("\n");

            Map<String, Map<String, Boolean>> worldPerms = worldPermissions.getOrDefault(server, new HashMap<>());
            for(String world : worldPerms.keySet().stream().sorted().collect(Collectors.toList())) {
                builder.append("\n      §e").append(world).append(":\n");

                Map<String, Boolean> wPermissions;
                if((wPermissions = worldPerms.get(world)) != null)
                    builder.append(formatPermissionMap(wPermissions, 4));

                builder.append("\n");
            }

        }

        return builder.toString();
    }

}
