package de.fabtopf.contray.adventuria.api.utils.bukkit.inventory;

import de.fabtopf.contray.adventuria.api.player.skin.SkinData;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.inventory.meta.tags.ItemTagType;

import java.util.*;

@Getter
public class SimpleItem {

    private UUID uniqueId;

    private boolean clone;
    private boolean cancelAction;

    private ItemEventAction<InventoryClickEvent> clickAction;
    private ItemEventAction<PlayerInteractEvent> interactAction;
    private ItemEventAction<PlayerInteractAtEntityEvent> interactAtEntityAction;
    private ItemEventAction<PlayerDropItemEvent> dropAction;

    private final Map<String, Object> replacements = new HashMap<>();

    private ItemStack itemStack;

    public SimpleItem(Material material) {
        this(material, 1);
    }

    public SimpleItem(Material material, int amount) {
        this(material, amount, false);
    }

    public SimpleItem(Material material, boolean clone) {
        this(material, 1, clone);
    }

    public SimpleItem(Material material, int amount, boolean clone) {
        this.itemStack = new ItemStack(material, amount);
        this.clone = clone;

        this.uniqueId = UUID.randomUUID();

        ItemMeta meta = this.itemStack.getItemMeta();
        meta.getCustomTagContainer().setCustomTag(SimpleItemProvider.ITEM_IDENTIFIER, ItemTagType.STRING, this.uniqueId.toString());
        this.itemStack.setItemMeta(meta);

        this.clickAction = null;
        this.interactAction = null;
        this.interactAtEntityAction = null;
    }

    public SimpleItem(ItemStack itemStack) {
        this(itemStack, false);
    }

    public SimpleItem(ItemStack itemStack, boolean clone) {
        this.itemStack = itemStack;
        this.clone = clone;

        this.uniqueId = UUID.randomUUID();

        ItemMeta meta = this.itemStack.getItemMeta();
        meta.getCustomTagContainer().setCustomTag(SimpleItemProvider.ITEM_IDENTIFIER, ItemTagType.STRING, this.uniqueId.toString());
        this.itemStack.setItemMeta(meta);

        this.clickAction = null;
        this.interactAction = null;
        this.interactAtEntityAction = null;
    }

    public SimpleItem setCancelAction(boolean cancelAction) {
        this.cancelAction = cancelAction;

        return this;
    }

    public SimpleItem setClickAction(ItemEventAction<InventoryClickEvent> clickAction) {
        this.clickAction = clickAction;

        return this;
    }

    public SimpleItem setInteractAction(ItemEventAction<PlayerInteractEvent> interactAction) {
        this.interactAction = interactAction;

        return this;
    }

    public SimpleItem setInteractAtEntityAction(ItemEventAction<PlayerInteractAtEntityEvent> interactAtEntityAction) {
        this.interactAtEntityAction = interactAtEntityAction;

        return this;
    }

    public SimpleItem setDropAction(ItemEventAction<PlayerDropItemEvent> dropAction) {
        this.dropAction = dropAction;

        return this;
    }

    public SimpleItem setAmount(int amount) {
        this.itemStack.setAmount(amount);

        return this;
    }

    public SimpleItem setDisplay(String display) {
        ItemMeta meta = this.itemStack.getItemMeta();
        meta.setDisplayName(display);
        this.itemStack.setItemMeta(meta);

        return this;
    }

    public SimpleItem setLore(String... lore) {
        return this.setLore(Arrays.asList(lore));
    }

    public SimpleItem setLore(List<String> lore) {
        ItemMeta meta = this.itemStack.getItemMeta();
        meta.setLore(lore);
        this.itemStack.setItemMeta(meta);

        return this;
    }

    public SimpleItem setDurability(int durability) {
        return this.setAmount(durability);
    }

    public SimpleItem setUnbreakable() {
        ItemMeta meta = this.itemStack.getItemMeta();
        meta.setUnbreakable(true);
        this.itemStack.setItemMeta(meta);

        return this;
    }

    public SimpleItem setBreakable() {
        ItemMeta meta = this.itemStack.getItemMeta();
        meta.setUnbreakable(false);
        this.itemStack.setItemMeta(meta);

        return this;
    }

    public SimpleItem enchant(Enchantment enchantment, int strength) {
        this.itemStack.addEnchantment(enchantment, strength);

        return this;
    }

    public SimpleItem enchantUnsafe(Enchantment enchantment, int strength) {
        this.itemStack.addUnsafeEnchantment(enchantment, strength);

        return this;
    }

    public SimpleItem setSkullData(SkinData data) {
        return this.setSkullData(data.getValue());
    }

    public SimpleItem setSkullData(String value) {
        Bukkit.getUnsafe().modifyItemStack(
                this.itemStack,
                "{SkullOwner:{Id:\"" +
                        new UUID(value.hashCode(), value.hashCode()) +
                        "\",Properties:{textures:[{Value:\"" +
                        value +
                        "\"}]}}}");

        return this;
    }

    public SimpleItem setSkullOwner(String owner) {

        if(this.itemStack.getItemMeta() instanceof SkullMeta) {

            SkullMeta meta = (SkullMeta) this.itemStack.getItemMeta();
            meta.setOwner(owner);
            this.itemStack.setItemMeta(meta);

        }

        return this;
    }

    public SimpleItem setSkullOwner(UUID ownerUUID) {
        return this.setSkullOwner(Bukkit.getOfflinePlayer(ownerUUID));
    }

    public SimpleItem setSkullOwner(OfflinePlayer offlinePlayer) {

        if(this.itemStack.getItemMeta() instanceof SkullMeta) {

            SkullMeta meta = (SkullMeta) this.itemStack.getItemMeta();
            meta.setOwningPlayer(offlinePlayer);
            this.itemStack.setItemMeta(meta);

        }

        return this;
    }

    public SimpleItem setArmorColor(Color color) {

        if(this.itemStack.getItemMeta() instanceof LeatherArmorMeta) {

            LeatherArmorMeta meta = (LeatherArmorMeta) this.itemStack.getItemMeta();
            meta.setColor(color);
            this.itemStack.setItemMeta(meta);

        }

        return this;

    }

    public ItemStack getItemStack() {
        return this.clone ? this.itemStack.clone() : this.itemStack;
    }

}
