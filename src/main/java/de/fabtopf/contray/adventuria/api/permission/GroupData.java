package de.fabtopf.contray.adventuria.api.permission;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class GroupData {

    private PermissionGroup permissionGroup;

    public String getGroupName() {
        return permissionGroup.getName();
    }

    public String getGroupNameFormatted() {
        return ChatColor.translateAlternateColorCodes('&',
                permissionGroup.getDisplay()) + permissionGroup.getName();
    }

    public boolean isAvailable() {
        return this.isAvailable(AdviAPI.getInstance().getCloudUtilities().getGroup());
    }

    public boolean isAvailable(String serverGroup) {
        return permissionGroup.getAvailableOn().contains(serverGroup);
    }

    public boolean isManagedGroup() {
        return (boolean) permissionGroup.getOptions().getOrDefault("managed_group", false);
    }

    public boolean isTeamGroup() {
        return (boolean) permissionGroup.getOptions().getOrDefault("team_group", false);
    }

    public UUID getGroupLeader() {
        if(!this.isTeamGroup())
            return null;

        return UUID.fromString((String) permissionGroup.getOptions().get("team_leader"));
    }

}
