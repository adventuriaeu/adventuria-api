package de.fabtopf.contray.adventuria.api.utils.event.bungee;

import de.fabtopf.contray.adventuria.api.permission.PermissionGroup;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Event;

@Getter
@AllArgsConstructor
public class PermissionGroupUpdateEvent extends Event {

    private PermissionGroup permissionGroup;
    private String collection;

}
