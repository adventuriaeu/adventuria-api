package de.fabtopf.contray.adventuria.api.utils.cloud;

import de.dytanic.cloudnet.lib.utility.document.Document;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
public class ChannelMessageDocument {

    private Document document = new Document();

    public boolean has(String key)
    {
        return document.contains(key);
    }

    public boolean getBoolean(String key)
    {
        return document.getBoolean(key);
    }

    public ChannelMessageDocument add(String key, boolean value)
    {
        document.append(key, value);
        return this;
    }

    public int getInt(String key)
    {
        return document.getInt(key);
    }

    public ChannelMessageDocument add(String key, int value)
    {
        document.append(key, value);
        return this;
    }

    public double getDouble(String key)
    {
        return document.getDouble(key);
    }

    public ChannelMessageDocument add(String key, double value)
    {
        document.append(key, value);
        return this;
    }

    public float getFloat(String key)
    {
        return document.getFloat(key);
    }

    public ChannelMessageDocument add(String key, float value)
    {
        document.append(key, value);
        return this;
    }

    public long getLong(String key)
    {
        return document.getLong(key);
    }

    public String getString(String key)
    {
        return document.getString(key);
    }

    public ChannelMessageDocument add(String key, String value)
    {
        document.append(key, value);
        return this;
    }

    public UUID getUniqueId(String key)
    {
        return get(key, UUID.class);
    }

    public <T> T get(String key, Class<T> clazz)
    {
        return document.getObject(key, clazz);
    }

    public ChannelMessageDocument add(String key, Object value)
    {
        document.append(key, value);
        return this;
    }

    public Collection<String> keys()
    {
        return document.keys();
    }

    public Document export()
    {
        return document;
    }

}
