package de.fabtopf.contray.adventuria.api.utils.event.bukkit;

import de.fabtopf.contray.adventuria.api.permission.PermissionGroup;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Getter
@AllArgsConstructor
public class PermissionGroupUpdateEvent extends Event {

    @Getter
    private static final HandlerList handlerList = new HandlerList();

    private PermissionGroup permissionGroup;
    private String collection;

    @Override
    public HandlerList getHandlers() {
        return getHandlerList();
    }

}
