package de.fabtopf.contray.adventuria.api.utils.bukkit.command.alias;

import com.google.gson.GsonBuilder;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.*;

public class AliasManager {

    private Map<String, SimpleCommand> commands = new HashMap<>();
    private FileConfiguration config;

    public AliasManager() {
        this.config = YamlConfiguration.loadConfiguration(new File(BukkitCore.getInstance().getDataFolder(), "aliases.yml"));

        this.reloadAliases();
    }

    public void registerAlias(String name, String description, String usage, String permission, List<Map<String, Object>> commands) {

        CommandAlias alias;
        if((alias = new CommandAlias(name.toLowerCase(), description, usage, permission, commands)).register())
            this.commands.put(name.toLowerCase(), alias);
    }

    public void unregisterAlias(String name) {
        SimpleCommand alias;
        if((alias = commands.get(name)) == null)
            return;

        alias.unregister();
        commands.remove(name);
    }

    public void unregisterAll() {

        Iterator<Map.Entry<String, SimpleCommand>> iterator = commands.entrySet().iterator();
        while(iterator.hasNext()) {
            iterator.next().getValue().unregister();
            iterator.remove();
        }

    }

    public void reloadAliases() {

        this.unregisterAll();

        Map<String, Class> commandExecutors = new HashMap<>();
        commandExecutors.put("console", ConsoleCommandSender.class);
        commandExecutors.put("player", Player.class);
        commandExecutors.put("op", Player.class);

        for(String key : config.getKeys(false)) {
            String description = config.getString(key + ".description");
            String permission = config.getString(key + ".permission");
            String usage = config.getString(key + ".usage");

            List<Map<String, Object>> subCommands = new ArrayList<>();
            ConfigurationSection commands = config.getConfigurationSection(key + ".commands");
            if(commands != null)
                for(String cmdKey : commands.getKeys(false)) {
                    ConfigurationSection subCommand = commands.getConfigurationSection(cmdKey);
                    Map<String, Object> settings = new HashMap<>();

                    settings.put("permission", subCommand.getString("permission"));
                    settings.put("requiredArgs", subCommand.getStringList("requiredArgs").toArray(new String[0]));
                    settings.put("helpMessageParams", subCommand.getStringList("helpMessageParams").toArray(new String[0]));
                    settings.put("showInHelp", subCommand.getBoolean("showInHelp"));
                    settings.put("allowedSenders", subCommand.getStringList("allowedSenders").stream().map(String::toLowerCase).map(commandExecutors::get).filter(Objects::nonNull).toArray(Class[]::new));

                    List<Map<String, String>> executedCommands = new ArrayList<>();
                    for(String cmd : subCommand.getStringList("commands")) {
                        Map<String, String> subSettings = new HashMap<>();

                        subSettings.put("sender", cmd.split(" ")[0]);
                        subSettings.put("command", cmd.replace(cmd.split(" ")[0] + " ", ""));

                        executedCommands.add(subSettings);
                    }

                    settings.put("commands", executedCommands);
                    subCommands.add(settings);
                }

            System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(subCommands));
            this.registerAlias(key.toLowerCase(), description, usage, permission, subCommands);

        }

    }

}
