package de.fabtopf.contray.adventuria.api.permission;

import de.dytanic.cloudnet.api.CloudAPI;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissibleBase;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class AdviPermissible extends PermissibleBase {

    private Player player;
    private UUID uniqueId;

    public AdviPermissible(Player player) {
        super(player);
        this.player = player;
        this.uniqueId = player.getUniqueId();

        player.setOp(false);
    }

    @Override
    public void recalculatePermissions() {
        this.clearPermissions();

        try {
            Field attatchmentField = PermissibleBase.class.getDeclaredField("attachments");
            attatchmentField.setAccessible(true);

            Method childPermissionMethod = PermissibleBase.class.getDeclaredMethod("calculateChildPermissions", Map.class, boolean.class, PermissionAttachment.class);
            childPermissionMethod.setAccessible(true);

            for (PermissionAttachment attachment : (List<PermissionAttachment>) attatchmentField.get(this))
                childPermissionMethod.invoke(this, attachment.getPermissions(), false, attachment);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if(uniqueId == null)
            return;

        NetworkPlayer player;
        if((player = AdviAPI.getInstance().getPlayerProvider().get(uniqueId)) == null)
            return;

        player.getPermissionUser().clearCachedPermissions();
    }

    @Override
    public Set<PermissionAttachmentInfo> getEffectivePermissions() {
        NetworkPlayer netPlayer;
        if((netPlayer = AdviAPI.getInstance().getPlayerProvider().get(this.uniqueId)) == null)
            return new HashSet<>();

        final Map<String, Boolean> permissions = netPlayer.getPermissionUser().getCalculatedWorldPermissions(
                CloudAPI.getInstance().getGroup(), player.getLocation().getWorld().getName());
        Set<PermissionAttachmentInfo> set = new HashSet<>();

        for (Map.Entry<String, Boolean> entry : permissions.entrySet()) {
            PermissionAttachmentInfo permissionAttachmentInfo = new PermissionAttachmentInfo(this, entry.getKey(), null, entry.getValue());
            set.add(permissionAttachmentInfo);
        }

        return set;
    }

    @Override
    public boolean isPermissionSet(String perm) {

        NetworkPlayer player;
        if((player = AdviAPI.getInstance().getPlayerProvider().get(this.player.getUniqueId())) == null)
            return false;

        return player.getPermissionUser().getCalculatedWorldPermissions(
                AdviAPI.getInstance().getCloudUtilities().getGroup(),
                this.player.getWorld().getName()).containsKey(perm.toLowerCase());
    }

    @Override
    public boolean isPermissionSet(Permission perm)
    {
        return isPermissionSet(perm.getName());
    }

    @Override
    public boolean hasPermission(Permission perm)
    {
        return hasPermission(perm.getName());
    }

    @Override
    public boolean hasPermission(String perm) {
        if (perm == null || perm.equalsIgnoreCase("bukkit.broadcast.user"))
            return true;

        NetworkPlayer netPlayer;
        if((netPlayer = AdviAPI.getInstance().getPlayerProvider().get(this.uniqueId)) == null)
            return false;

        return netPlayer.getPermissionUser().hasPermission(perm, CloudAPI.getInstance().getGroup(), player.getWorld().getName());
    }

    @Override
    public boolean isOp()
    {
        return false;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

}
