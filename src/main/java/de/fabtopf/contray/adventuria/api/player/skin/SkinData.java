package de.fabtopf.contray.adventuria.api.player.skin;

import com.arangodb.entity.BaseDocument;
import com.mojang.authlib.properties.Property;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.player.PlayerProperty;
import de.fabtopf.contray.adventuria.api.utils.arango.ArangoWritable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter(AccessLevel.PACKAGE)
@AllArgsConstructor
public class SkinData implements ArangoWritable<UUID> {

    private UUID uniqueId;

    private String name;
    private String signature;
    private String value;

    private boolean blocked;

    private List<UUID> currentUsers;

    public SkinData(UUID uniqueId) {
        this.uniqueId = uniqueId;
        this.currentUsers = new ArrayList<>();

        for(NetworkPlayer player : AdviAPI.getInstance().getPlayerProvider().getOnlinePlayers())
            if(player.getProperty(PlayerProperty.SKIN).equals(this))
                this.addUser(player.getUniqueId());
    }

    @Override
    public UUID getKey() {
        return uniqueId;
    }

    @Override
    public void read(BaseDocument document) {
        this.name = (String) document.getAttribute("name");
        this.signature = (String) document.getAttribute("signature");
        this.value = (String) document.getAttribute("value");
        this.blocked = (boolean) document.getProperties().getOrDefault("blocked", false);
    }

    @Override
    public void save(BaseDocument document) {
        document.addAttribute("name", this.name);
        document.addAttribute("signature", this.signature);
        document.addAttribute("value", this.value);
        document.addAttribute("blocked", this.blocked);
    }

    public void addUser(UUID uniqueId) {
        currentUsers.add(uniqueId);
    }

    public void removeUser(UUID uniqueId) {
        currentUsers.remove(uniqueId);

        if(currentUsers.isEmpty())
            AdviAPI.getInstance().getPlayerProvider().getSkinDataProvider().uncache(this.uniqueId);
    }

    public Property getProperty() {
        return new Property(name, value, signature);
    }

}
