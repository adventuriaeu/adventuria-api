package de.fabtopf.contray.adventuria.api.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.Scanner;

public class FileUtilities {

    public static void copyFile(File file, File target) {
        if (file == null)
            throw new IllegalArgumentException("Unable to copy a file = null");

        if (file.isDirectory())
            throw new UnsupportedOperationException("Could not Copy directory");

        FileChannel inChannel = null;
        FileChannel outChannel = null;
        try {
            inChannel = new FileInputStream(file).getChannel();
            outChannel = new FileOutputStream(target).getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inChannel != null)
                    inChannel.close();
                if (outChannel != null)
                    outChannel.close();
            } catch (IOException e) {}
        }
    }

    public static String readFile(File file, String prefix) {
        try (Scanner reader = new Scanner(file)) {
            StringBuilder content = new StringBuilder(prefix);

            while (reader.hasNextLine())
                content.append(reader.nextLine()).append('\n');

            return content.toString();
        } catch (Exception exception) {
            return null;
        }
    }

    public static String pasteToHastebin(String text) {
        try {
            URL url = new URL("https://hastebin.com/documents");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0");
            connection.setRequestProperty("Accept-Language", "en-En,en;q=0.5");

            connection.setDoOutput(true);
            DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
            writer.writeUTF(text);
            writer.flush();
            writer.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);

            in.close();

            System.out.println(response.toString());

            JsonElement json = new JsonParser().parse(response.toString());

            if (!json.isJsonObject()) throw new IOException("Cannot parse JSON");

            return "https://hastebin.com/" + json.getAsJsonObject().get("key").getAsString();
        } catch (IOException e) {
            System.err.println("Error while uploading: " + e.getLocalizedMessage());
        }

        return null;
    }
}
