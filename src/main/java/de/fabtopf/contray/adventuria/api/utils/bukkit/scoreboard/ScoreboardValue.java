package de.fabtopf.contray.adventuria.api.utils.bukkit.scoreboard;

import org.bukkit.entity.Player;

public interface ScoreboardValue {

    String getDefaultValue();
    String getValue(Player player);
    String getName();

    default String getName(Player player) {
        return getName();
    }

}
