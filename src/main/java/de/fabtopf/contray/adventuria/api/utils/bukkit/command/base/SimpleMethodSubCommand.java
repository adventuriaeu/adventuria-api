package de.fabtopf.contray.adventuria.api.utils.bukkit.command.base;

import lombok.Getter;
import org.bukkit.command.CommandSender;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.UUID;
import java.util.regex.Pattern;

@Getter
class SimpleMethodSubCommand extends SimpleSubCommand {

    private Method method;

    SimpleMethodSubCommand(Method method) {

        try {
            SimpleSubCommandHandler handler = method.getAnnotation(SimpleSubCommandHandler.class);

            this.permission = handler.permission();
            this.requiredArgs = handler.requiredArgs();
            this.helpMessageParams = handler.helpMessageParams();
            this.showInHelp = handler.showInHelp();
            this.display = handler.display();
            this.allowedSenders = handler.allowedSenders();

            this.requiredArgPatterns = (Pattern[]) Arrays.stream(this.requiredArgs).map(s -> Pattern.compile(s, Pattern.CASE_INSENSITIVE)).toArray();

            this.method = method;

        } catch (Exception ignored) {}

    }

    public boolean trigger(CommandSender sender, SimpleCommand command, String label, String[] args) {
        if(!this.checkSender(sender))
            return false;

        if(!this.checkPermission(sender))
            return false;

        if(!this.checkArguments(args))
            return false;

        Object[] params = new Object[args.length + 2];
        params[0] = sender;
        params[1] = command;

        for(int i = 0; i < this.getRequiredArgs().length; i++)
            switch (this.getRequiredArgs()[i]) {
                case REGEX_FLOAT:
                    params[i + 2] = Float.parseFloat(args[i].replace(",", "."));
                    break;
                case REGEX_INT:
                    params[i + 2] = Integer.parseInt(args[i]);
                    break;
                case REGEX_UUID:
                    params[i + 2] = UUID.fromString(args[i]);
                    break;
                default:
                    params[i + 2] = args[i];
                    break;
            }

        try {

            this.getMethod().setAccessible(true);
            this.getMethod().invoke(command, params);

            return true;
        } catch (Exception e) {

            command.getExecutor().sendHelpMessage(sender, command);
            return true;
        }

    }

}
