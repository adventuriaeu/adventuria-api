package de.fabtopf.contray.adventuria.api.utils.arango.parser;

import java.util.Map;

public interface Serializable {

    void serialize(Map<String, Object> data);
    void deserialize(Map<String, Object> data);

}
