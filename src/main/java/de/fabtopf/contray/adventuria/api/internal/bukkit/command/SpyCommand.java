package de.fabtopf.contray.adventuria.api.internal.bukkit.command;

import com.google.common.collect.Lists;
import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.lib.player.CloudPlayer;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.player.PlayerProperty;
import de.fabtopf.contray.adventuria.api.utils.bukkit.chat.ChatChannel;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommand;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommandHandler;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleFunctionCommandExecutor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@SimpleCommandHandler(name = "spy", permission = "adventuria.api.command.spy")
public class SpyCommand implements SimpleFunctionCommandExecutor {

    @Override
    public void sendHelpMessage(CommandSender executor, SimpleCommand command) {

    }

    @Override
    public String getUsage() {
        return "/spy <Channel> [Spieler] [on|off]";
    }

    @Override
    public String getPermissionMessage() {
        return BukkitCore.getInstance().getLanguage().getPhraseContent("command-no-perm");
    }

    @Override
    public String getDescription() {
        return "Befehl zum ein-/ausschalten des Spychats für einzelne Channel";
    }

    @Override
    public void onExecute(CommandSender sender, SimpleCommand command, String label, String[] args) {

        NetworkPlayer player;
        ChatChannel channel;
        switch(args.length) {
            case 0:
                sender.sendMessage(getUsage());
                return;

            case 1:
                if(!(sender instanceof Player)) {
                    sender.sendMessage(getUsage());
                    return;
                }

                if((channel = AdviAPI.getInstance().getChatManager().getChannel(args[0])) == null) {
                    sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-not-existing"));
                    return;
                }

                player = AdviAPI.getInstance().getPlayerProvider().get(((Player) sender).getUniqueId());
                if(player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL).contains(channel)) {
                    List<ChatChannel> spyChannels = player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL);
                    spyChannels.removeIf(c -> c == channel);

                    player.updateProperty(PlayerProperty.SPY_CHAT_CHANNEL, spyChannels.stream().map(ChatChannel::getName).collect(Collectors.toList()));
                    player.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-now-inactive", channel.getName()));
                } else {
                    if(!sender.hasPermission(channel.getSpyPermission())) {
                        sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-no-permission"));
                        return;
                    }

                    List<ChatChannel> spyChannels = player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL);
                    spyChannels.add(channel);

                    player.updateProperty(PlayerProperty.SPY_CHAT_CHANNEL, spyChannels.stream().map(ChatChannel::getName).collect(Collectors.toList()));
                    player.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-now-active", channel.getName()));
                }

                return;

            case 2:
                if((channel = AdviAPI.getInstance().getChatManager().getChannel(args[0])) == null) {
                    sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-not-existing"));
                    return;
                }

                if(sender instanceof Player) {
                    player = AdviAPI.getInstance().getPlayerProvider().get(((Player) sender).getUniqueId());
                    if(args[1].equalsIgnoreCase("on")) {

                        if(!player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL).contains(channel)) {
                            if(!sender.hasPermission(channel.getSpyPermission())) {
                                sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-no-permission", channel.getName()));
                                return;
                            }

                            List<ChatChannel> spyChannels = player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL);
                            spyChannels.add(channel);

                            player.updateProperty(PlayerProperty.SPY_CHAT_CHANNEL, spyChannels.stream().map(ChatChannel::getName).collect(Collectors.toList()));
                            player.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-now-active", channel.getName()));
                        } else {

                            player.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-already-active", channel.getName()));
                        }

                        return;
                    } else if(args[1].equalsIgnoreCase("off")) {

                        if(player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL).contains(channel)) {
                            List<ChatChannel> spyChannels = player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL);
                            spyChannels.removeIf(c -> c == channel);

                            player.updateProperty(PlayerProperty.SPY_CHAT_CHANNEL, spyChannels.stream().map(ChatChannel::getName).collect(Collectors.toList()));
                            player.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-now-inactive", channel.getName()));
                        } else {

                            player.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-already-inactive", channel.getName()));
                        }

                        return;
                    }
                }

                if(!sender.hasPermission("adventuria.api.command.spy.others")) {
                    sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-no-permission", channel.getName()));
                    return;
                }

                if((player = AdviAPI.getInstance().getPlayerProvider().get(args[1])) == null) {
                    sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-player-does-not-exist", args[1]));
                    return;
                }

                if(player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL).contains(channel)) {
                    List<ChatChannel> spyChannels = player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL);
                    spyChannels.removeIf(c -> c == channel);

                    player.updateProperty(PlayerProperty.SPY_CHAT_CHANNEL, spyChannels.stream().map(ChatChannel::getName).collect(Collectors.toList()));
                    if(player.isOnline())
                        player.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-now-inactive", channel.getName()));

                    sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-now-inactive-others", channel.getName(), player.getName()));
                } else {
                    if(!sender.hasPermission(channel.getSpyPermission())) {
                        sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-no-permission", channel.getName()));
                        return;
                    }

                    List<ChatChannel> spyChannels = player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL);
                    spyChannels.add(channel);

                    player.updateProperty(PlayerProperty.SPY_CHAT_CHANNEL, spyChannels.stream().map(ChatChannel::getName).collect(Collectors.toList()));
                    if(player.isOnline())
                        player.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-now-active", channel.getName()));

                    sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-now-active-others", channel.getName(), player.getName()));
                }

                return;

            case 3:
                if((channel = AdviAPI.getInstance().getChatManager().getChannel(args[0])) == null) {
                    sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-not-existing"));
                    return;
                }

                if(!sender.hasPermission("adventuria.api.command.spy.others")) {
                    sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-no-permission", channel.getName()));
                    return;
                }

                if((player = AdviAPI.getInstance().getPlayerProvider().get(args[1])) == null) {
                    sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-player-does-not-exist", args[1]));
                    return;
                }

                if(args[2].equalsIgnoreCase("on")) {

                    List<ChatChannel> spyChannels = player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL);
                    if(!spyChannels.contains(channel)) {
                        if (!sender.hasPermission(channel.getSpyPermission())) {
                            sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-no-permission", channel.getName()));
                            return;
                        }

                        spyChannels.add(channel);

                        player.updateProperty(PlayerProperty.SPY_CHAT_CHANNEL, spyChannels.stream().map(ChatChannel::getName).collect(Collectors.toList()));
                        if (player.isOnline())
                            player.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-now-active", channel.getName()));

                        sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-now-active-others", channel.getName(), player.getName()));
                    } else {

                        sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-already-active-others", channel.getName(), player.getName()));
                    }

                    return;
                } else if(args[2].equalsIgnoreCase("off")) {

                    if (player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL).contains(channel)) {
                        List<ChatChannel> spyChannels = player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL);
                        spyChannels.removeIf(c -> c == channel);

                        player.updateProperty(PlayerProperty.SPY_CHAT_CHANNEL, spyChannels.stream().map(ChatChannel::getName).collect(Collectors.toList()));
                        if (player.isOnline())
                            player.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-now-inactive", channel.getName()));

                        sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-now-inactive-others", channel.getName(), player.getName()));
                    } else {

                        sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-spy-channel-already-inactive-others", channel.getName(), player.getName()));
                    }

                    return;
                } else {

                    sender.sendMessage(command.getUsage());
                    return;
                }

            default:
                sender.sendMessage(command.getUsage());
        }

    }

    @Override
    public List<String> onTabComplete(CommandSender sender, SimpleCommand command, String fallback, String[] args, Location location) {

        List<String> possibilities = Lists.newArrayList("on", "off");
        switch (args.length) {
            case 1:
                return AdviAPI.getInstance().getChatManager().getChannels().stream().map(ChatChannel::getName).collect(Collectors.toList());

            case 2:
                if(sender.hasPermission("adventuria.api.command.spy.others"))
                    CloudAPI.getInstance().getOnlinePlayers().stream().map(CloudPlayer::getName).collect(Collectors.toList());

                return possibilities;

            case 3:
                return sender.hasPermission("adventuria.api.command.spy.others") && !possibilities.contains(args[1].toLowerCase()) ? possibilities : Collections.EMPTY_LIST;

            default:
                return Collections.EMPTY_LIST;
        }
    }
}
