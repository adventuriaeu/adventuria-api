package de.fabtopf.contray.adventuria.api.internal;

import com.google.common.collect.Lists;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class PluginSupport {

    public static void loadFiles(File folder, ClassLoader parent) {
        List<File> toLoad = new ArrayList<>();

        if (!folder.exists())
            folder.mkdir();

        for (File file : folder.listFiles()) {
            if (file.getName().endsWith(".jar")) {
                toLoad.add(file);
                System.out.println("Dependency: " + file.getName());
            }
        }

        loadFiles(toLoad, parent);
    }

    private static void loadFiles(List<File> files, ClassLoader parent) {

        URL[] urls = Lists.newArrayList(files.stream().map(File::toURI).map(uri -> {
               try {
                   return uri.toURL();
               } catch (Exception exception) {
                   return null;
               }
           }).filter(Objects::nonNull).iterator()).toArray(new URL[] {});

        for (URL url : urls) {
            System.out.println(url.toString());
            addURLToClassLoader(url, parent);
        }

        for (File file : files) {
            try {
                JarFile jarFile = new JarFile(file);
                Enumeration<JarEntry> allEntries = jarFile.entries();

                while (allEntries.hasMoreElements()) {
                    JarEntry entry = allEntries.nextElement();
                    String name = entry.getName().replace('/', '.');

                    try {
                        if (name.endsWith(".class"))
                            parent.loadClass(name.substring(0, name.length() - 6));
                    } catch (Throwable exception) {
                        System.out.println("Could not load class " + name + " (" + name.substring(0, name.length() - 6) + ")");
                    }
                }
            } catch (IOException exception) {
                exception.printStackTrace();
                System.out.println("Could not load " + file.getName());
            }
        }
    }



    private static void addURLToClassLoader(URL url, ClassLoader classloader) {
        URLClassLoader systemClassLoader = (URLClassLoader) classloader;
        Class<URLClassLoader> classLoaderClass = URLClassLoader.class;

        try {
            Method method = classLoaderClass.getDeclaredMethod("addURL", new Class[]{URL.class});
            method.setAccessible(true);
            method.invoke(systemClassLoader, new Object[]{url});
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

}
