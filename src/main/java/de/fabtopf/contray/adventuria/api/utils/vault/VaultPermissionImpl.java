package de.fabtopf.contray.adventuria.api.utils.vault;

import de.dytanic.cloudnet.api.CloudAPI;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.permission.PermissionGroup;
import de.fabtopf.contray.adventuria.api.permission.PermissionGroupEntity;
import de.fabtopf.contray.adventuria.api.permission.PermissionPool;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import lombok.NoArgsConstructor;
import net.milkbowl.vault.permission.Permission;

@NoArgsConstructor
public class VaultPermissionImpl extends Permission {

    @Override
    public String getName() {
        return "AdventuriaAPI-Permissions";
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean hasSuperPermsCompat() {
        return true;
    }

    @Override
    public boolean playerHas(String world, String playerName, String permission) {
        NetworkPlayer player;
        if((player = AdviAPI.getInstance().getPlayerProvider().get(playerName)) == null)
            return false;

        if(world != null)
            return player.getPermissionUser().hasPermission(permission, getServerGroup(), world);

        return player.getPermissionUser().hasPermission(permission, getServerGroup());
    }

    @Override
    public boolean playerAdd(String world, String playerName, String permission) {
        NetworkPlayer player;
        if((player = AdviAPI.getInstance().getPlayerProvider().get(playerName)) == null)
            return false;

        if(!player.getPermissionUser().addPermission(permission, getServerGroup()))
            return false;

        player.getPermissionUser().sendUpdate();
        return true;
    }

    @Override
    public boolean playerRemove(String world, String playerName, String permission) {
        NetworkPlayer player;
        if((player = AdviAPI.getInstance().getPlayerProvider().get(playerName)) == null)
            return false;

        if(!player.getPermissionUser().removePermission(permission, getServerGroup()))
            return false;

        player.getPermissionUser().sendUpdate();
        return true;
    }

    @Override
    public boolean groupHas(String world, String groupName, String permission) {
        PermissionPool pool = AdviAPI.getInstance().getPermissionPool();

        PermissionGroup group;
        if((group = pool.get(groupName)) == null)
            return false;

        if(world != null)
            group.hasPermission(permission, getServerGroup(), world);

        return group.hasPermission(permission, getServerGroup());
    }

    @Override
    public boolean groupAdd(String world, String groupName, String permission) {
        PermissionPool pool = AdviAPI.getInstance().getPermissionPool();

        PermissionGroup group;
        if((group = pool.get(groupName)) == null)
            return false;

        if(!pool.addPermission(group, permission, getServerGroup()))
            return false;

        pool.sendGroupUpdate(group);
        return true;
    }

    @Override
    public boolean groupRemove(String world, String serverGroup, String permission) {
        PermissionPool pool = AdviAPI.getInstance().getPermissionPool();

        PermissionGroup group;
        if((group = pool.get(serverGroup)) == null)
            return false;

        if(!pool.removePermission(group, permission, getServerGroup()))
            return false;

        pool.sendGroupUpdate(group);
        return true;
    }

    @Override
    public boolean playerInGroup(String world, String playerName, String groupName) {

        NetworkPlayer player;
        if((player = AdviAPI.getInstance().getPlayerProvider().get(playerName)) == null)
            return false;

        return player.getPermissionUser().getCurrentGroups(CloudAPI.getInstance().getGroup()).stream().anyMatch(g -> g.getName().equalsIgnoreCase(groupName));
    }

    @Override
    public boolean playerAddGroup(String world, String playerName, String groupName) {
        NetworkPlayer player;
        if((player = AdviAPI.getInstance().getPlayerProvider().get(playerName)) == null)
            return false;

        if(!player.getPermissionUser().addGroup(new PermissionGroupEntity(groupName, System.currentTimeMillis(), 0, null)))
            return false;

        player.getPermissionUser().sendUpdate();
        return true;
    }

    @Override
    public boolean playerRemoveGroup(String world, String playerName, String groupName) {
        NetworkPlayer player;
        if((player = AdviAPI.getInstance().getPlayerProvider().get(playerName)) == null)
            return false;

        PermissionGroup group;
        if((group = AdviAPI.getInstance().getPermissionPool().get(groupName)) == null)
            return false;

        if(!player.getPermissionUser().removeGroup(group))
            return false;

        player.getPermissionUser().sendUpdate();
        return true;
    }

    @Override
    public String[] getPlayerGroups(String world, String playerName) {
        NetworkPlayer player;
        if((player = AdviAPI.getInstance().getPlayerProvider().get(playerName)) == null)
            return new String[0];

        return player.getPermissionUser().getCurrentGroups(getServerGroup()).stream().map(PermissionGroup::getName).toArray(String[]::new);
    }

    @Override
    public String getPrimaryGroup(String world, String playerName) {
        NetworkPlayer player;
        if((player = AdviAPI.getInstance().getPlayerProvider().get(playerName)) == null)
            return null;

        return player.getHighestGroup();
    }

    @Override
    public String[] getGroups() {
        return (String[]) AdviAPI.getInstance().getPermissionPool().getGroups().toArray();
    }

    @Override
    public boolean hasGroupSupport() {
        return true;
    }

    private String getServerGroup() {
        return CloudAPI.getInstance().getGroup();
    }
}
