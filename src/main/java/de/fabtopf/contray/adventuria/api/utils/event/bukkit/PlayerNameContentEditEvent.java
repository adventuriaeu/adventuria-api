package de.fabtopf.contray.adventuria.api.utils.event.bukkit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Getter
@AllArgsConstructor
public class PlayerNameContentEditEvent extends Event {

    @Getter
    private static final HandlerList handlerList = new HandlerList();

    private Player player;

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

}
