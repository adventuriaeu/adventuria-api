package de.fabtopf.contray.adventuria.api.utils.vault;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.permission.PermissionGroup;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;

public class VaultChatImpl extends Chat {

    public VaultChatImpl(Permission perms) {
        super(perms);
    }

    @Override
    public String getName() {
        return "AdventuriaAPI-Chat";
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getPlayerPrefix(String world, String playerName) {

        NetworkPlayer player;
        if((player = getPlayer(playerName)) == null)
            return null;

        return player.getPermissionUser().getPrefix();
    }

    @Override
    public void setPlayerPrefix(String world, String playerName, String prefix) {

        NetworkPlayer player;
        if((player = getPlayer(playerName)) == null)
            return;

        player.getPermissionUser().setPrefix(prefix);
    }

    @Override
    public String getPlayerSuffix(String world, String playerName) {

        NetworkPlayer player;
        if((player = getPlayer(playerName)) == null)
            return null;

        return player.getPermissionUser().getSuffix();
    }

    @Override
    public void setPlayerSuffix(String world, String playerName, String suffix) {

        NetworkPlayer player;
        if((player = getPlayer(playerName)) == null)
            return;

        player.getPermissionUser().setSuffix(suffix);
    }

    @Override
    public String getGroupPrefix(String world, String group) {

        PermissionGroup permissionGroup;
        if((permissionGroup = getGroup(group)) == null)
            return null;

        return permissionGroup.getPrefix();
    }

    @Override
    public void setGroupPrefix(String world, String group, String prefix) {

        PermissionGroup permissionGroup;
        if((permissionGroup = getGroup(group)) == null)
            return;

        AdviAPI.getInstance().getPermissionPool().setGroupPrefix(permissionGroup, prefix);
    }

    @Override
    public String getGroupSuffix(String world, String group) {

        PermissionGroup permissionGroup;
        if((permissionGroup = getGroup(group)) == null)
            return null;

        return permissionGroup.getPrefix();
    }

    @Override
    public void setGroupSuffix(String world, String group, String suffix) {

        PermissionGroup permissionGroup;
        if((permissionGroup = getGroup(group)) == null)
            return;

        AdviAPI.getInstance().getPermissionPool().setGroupSuffix(permissionGroup, suffix);
    }

    @Override
    public int getPlayerInfoInteger(String world, String playerName, String node, int defaultValue) {
        return defaultValue;
    }

    @Override
    public void setPlayerInfoInteger(String world, String playerName, String node, int value) {

    }

    @Override
    public int getGroupInfoInteger(String world, String groupName, String node, int defaultValue) {

        PermissionGroup permissionGroup;
        if((permissionGroup = getGroup(groupName)) == null)
            return defaultValue;

        if(!permissionGroup.getOptions().containsKey(node.toLowerCase()))
            return defaultValue;

        try {

            return ((Long) permissionGroup.getOptions().get(node.toLowerCase())).intValue();
        } catch (Exception ignored) {

            return defaultValue;
        }
    }

    @Override
    public void setGroupInfoInteger(String world, String groupName, String node, int value) {

        PermissionGroup permissionGroup;
        if((permissionGroup = getGroup(groupName)) == null)
            return;

        permissionGroup.getOptions().put(node, value);
    }

    @Override
    public double getPlayerInfoDouble(String world, String playerName, String node, double defaultValue) {
        return defaultValue;
    }

    @Override
    public void setPlayerInfoDouble(String world, String playerName, String node, double value) {

    }

    @Override
    public double getGroupInfoDouble(String world, String groupName, String node, double defaultValue) {

        PermissionGroup permissionGroup;
        if((permissionGroup = getGroup(groupName)) == null)
            return defaultValue;

        if(!permissionGroup.getOptions().containsKey(node.toLowerCase()))
            return defaultValue;

        try {

            return (double) permissionGroup.getOptions().get(node.toLowerCase());
        } catch (Exception ignored) {

            return defaultValue;
        }
    }

    @Override
    public void setGroupInfoDouble(String world, String groupName, String node, double value) {

        PermissionGroup permissionGroup;
        if((permissionGroup = getGroup(groupName)) == null)
            return;

        permissionGroup.getOptions().put(node, value);
    }

    @Override
    public boolean getPlayerInfoBoolean(String world, String playerName, String node, boolean defaultValue) {
        return defaultValue;
    }

    @Override
    public void setPlayerInfoBoolean(String world, String playerName, String node, boolean value) {

    }

    @Override
    public boolean getGroupInfoBoolean(String world, String groupName, String node, boolean defaultValue) {

        PermissionGroup permissionGroup;
        if((permissionGroup = getGroup(groupName)) == null)
            return defaultValue;

        if(!permissionGroup.getOptions().containsKey(node.toLowerCase()))
            return defaultValue;

        try {

            return (boolean) permissionGroup.getOptions().get(node.toLowerCase());
        } catch (Exception ignored) {

            return defaultValue;
        }
    }

    @Override
    public void setGroupInfoBoolean(String world, String groupName, String node, boolean value) {

        PermissionGroup permissionGroup;
        if((permissionGroup = getGroup(groupName)) == null)
            return;

        permissionGroup.getOptions().put(node, value);
    }

    @Override
    public String getPlayerInfoString(String world, String playerName, String node, String defaultValue) {
        return defaultValue;
    }

    @Override
    public void setPlayerInfoString(String world, String playerName, String node, String value) {

    }

    @Override
    public String getGroupInfoString(String world, String groupName, String node, String defaultValue) {

        PermissionGroup permissionGroup;
        if((permissionGroup = getGroup(groupName)) == null)
            return defaultValue;

        if(!permissionGroup.getOptions().containsKey(node.toLowerCase()))
            return defaultValue;

        try {

            return (String) permissionGroup.getOptions().get(node.toLowerCase());
        } catch (Exception ignored) {

            return defaultValue;
        }
    }

    @Override
    public void setGroupInfoString(String world, String groupName, String node, String value) {

        PermissionGroup permissionGroup;
        if((permissionGroup = getGroup(groupName)) == null)
            return;

        permissionGroup.getOptions().put(node, value);
    }

    private NetworkPlayer getPlayer(String name) {
        return AdviAPI.getInstance().getPlayerProvider().get(name);
    }

    private PermissionGroup getGroup(String name) {
        return AdviAPI.getInstance().getPermissionPool().get(name);
    }
}
