package de.fabtopf.contray.adventuria.api.internal.bukkit.listener;

import de.dytanic.cloudnet.bridge.event.bukkit.BukkitCustomChannelMessageReceiveEvent;
import de.dytanic.cloudnet.bridge.event.bukkit.BukkitMobInitEvent;
import de.dytanic.cloudnet.bridge.event.bukkit.BukkitSubChannelMessageEvent;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.permission.PermissionGroup;
import de.fabtopf.contray.adventuria.api.permission.PermissionUser;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.utils.bukkit.chat.ChatChannel;
import de.fabtopf.contray.adventuria.api.utils.cloud.ChannelMessageDocument;
import de.fabtopf.contray.adventuria.api.utils.economy.EconomyProvider;
import de.fabtopf.contray.adventuria.api.utils.event.bukkit.ChannelMessageEvent;
import de.fabtopf.contray.adventuria.api.utils.event.bukkit.PermissionGroupUpdateEvent;
import de.fabtopf.contray.adventuria.api.utils.event.bukkit.PlayerUpdateEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class CloudNetListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onMessage(BukkitSubChannelMessageEvent event) {
        if(event.getChannel().equals("advi-player")) {
            UUID uniqueId = event.getDocument().getObject("uniqueId", UUID.class);

            switch(event.getMessage()) {

                case "update":
                    AdviAPI.getInstance().getPlayerProvider().uncache(uniqueId);
                    if(AdviAPI.getInstance().isOnline(uniqueId))
                        Bukkit.getPluginManager().callEvent(new PlayerUpdateEvent(Bukkit.getPlayer(uniqueId)));
                    break;

                case "update-skin":
                    AdviAPI.getInstance().bukkit().getPlayerProvider().updateSkin(uniqueId, event.getDocument().getString("skinName"));
                    break;

                default:
                    break;

            }

        } else if(event.getChannel().equals("advi-group")) {
            PermissionGroup group = AdviAPI.getInstance().getPermissionPool().get(event.getDocument().getString("group"));
            String collection = event.getDocument().getString("collection");

            if(group == null)
                return;

            switch(event.getMessage()) {

                case "update":
                    AdviAPI.getInstance().getPermissionPool().uncache(group.getKey());
                    group = AdviAPI.getInstance().getPermissionPool().get(event.getDocument().getString("group"));

                    PermissionGroup group1 = group;
                    AdviAPI.getInstance().getPermissionPool().getGroups().stream().filter(p -> p.getImplementedGroups().contains(group1)).forEach(g -> {
                        g.clearCachedPermissions();
                        AdviAPI.getInstance().getPlayerProvider().getOnlinePlayers().stream().map(NetworkPlayer::getPermissionUser).filter(u -> u.getCurrentGroups().contains(g)).forEach(PermissionUser::clearCachedPermissions);
                    });

                    if(group != null)
                        Bukkit.getPluginManager().callEvent(new PermissionGroupUpdateEvent(group, collection));
                    break;

                default:
                    break;

            }

        } else if(event.getChannel().equalsIgnoreCase("advi-chat")) {

            AdviAPI.getInstance().getChatManager().getChannels().forEach(channel -> {
                if(!event.getMessage().equalsIgnoreCase(channel.getCommunicationChannel()))
                    return;

                String ch = event.getDocument().getString("channel");

                Optional<ChatChannel> chatChannelOptional;
                if(!(chatChannelOptional = AdviAPI.getInstance().getChatManager().getChannels().stream().filter(c -> c.getCommunicationChannel() != null && c.getCommunicationChannel().equalsIgnoreCase(ch)).findAny()).isPresent())
                    return;

                String uuid;
                NetworkPlayer invoker = null;
                if((uuid = event.getDocument().getString("invoker")) != null)
                    invoker = AdviAPI.getInstance().getPlayerProvider().get(UUID.fromString(uuid));

                String message = event.getDocument().getString("message");
                String from = event.getDocument().getString("from");
                boolean command = event.getDocument().getBoolean("command");
                Map<String, Object> info = event.getDocument().getObject("info", HashMap.class);

                if(from == null || !from.equals(AdviAPI.getInstance().getServerName()))
                    AdviAPI.getInstance().getChatManager().invokeChatMessage(invoker, message, chatChannelOptional.get().getName(), command,true, info);
            });

        } else if(event.getChannel().equals("advi-economy")) {

            switch(event.getMessage()) {

                case "update-currency":
                    EconomyProvider.getCurrencyProvider().uncache(event.getDocument().getObject("uniqueId", UUID.class));
                    break;

                default:
                    break;

            }

        } else {
            Bukkit.getPluginManager().callEvent(new ChannelMessageEvent(event.getChannel(), event.getMessage(), new ChannelMessageDocument(event.getDocument())));
        }
    }

    @EventHandler
    public void onMessage(BukkitCustomChannelMessageReceiveEvent event) {
        Bukkit.getPluginManager().callEvent(new ChannelMessageEvent(event.getChannel(), event.getMessage(), new ChannelMessageDocument(event.getDocument())));
    }

    @EventHandler
    public void onServerMobCreation(BukkitMobInitEvent event) {
        int entityId = event.getMob().getEntity().getEntityId();
        Location location = event.getMob().getEntity().getLocation();

        World world;
        if((world = location.getWorld()) != null)
            world.getNearbyEntities(location, 0.1, 0.1, 0.1).stream().filter(m -> m.getEntityId() != entityId).forEach(Entity::remove);
    }

}
