package de.fabtopf.contray.adventuria.api.utils.bukkit.command.base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpleCommandHandler {

    String name();
    String prefix() default "%plugin%";
    String permission() default "";

    String[] aliases() default {};

}
