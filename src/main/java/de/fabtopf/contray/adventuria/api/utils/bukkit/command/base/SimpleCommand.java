package de.fabtopf.contray.adventuria.api.utils.bukkit.command.base;

import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.SimplePluginManager;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

@Getter
public class SimpleCommand extends Command {

    @SafeVarargs
    public static List<SimpleCommand> analyze(Plugin plugin, Class<? extends ICommandExecutor>... commands) {

        List<SimpleCommand> registeredCommands = new ArrayList<>();
        for(Class<?> clazz : commands) {

            SimpleCommandHandler handler;
            if(!ICommandExecutor.class.isAssignableFrom(clazz) ||
                    (handler = clazz.getDeclaredAnnotation(SimpleCommandHandler.class)) == null)
                continue;

            try {

                ICommandExecutor executor = (ICommandExecutor) clazz.getDeclaredConstructor().newInstance();
                SimpleCommand command = SimpleCommand.class.getDeclaredConstructor(SimpleCommandHandler.class, ICommandExecutor.class, Plugin.class)
                        .newInstance(handler, executor, plugin);

                if(executor instanceof SimpleSubCommandExecutor)
                    for(Method method : executor.getClass().getMethods())
                        if(method.isAnnotationPresent(SimpleSubCommandHandler.class))
                            command.getSubCommandList().add(new SimpleMethodSubCommand(method));

                command.getSubCommandList().sort(Comparator.comparingInt(ISimpleSubCommand::getDisplay));

                if(command.register())
                    registeredCommands.add(command);

            } catch (Exception ignored) {}

        }

        return registeredCommands;
    }

    private static SimpleCommandMap getCommandMap(Plugin plugin) {
        if(!(plugin.getServer().getPluginManager() instanceof SimplePluginManager))
            return null;

        SimplePluginManager manager = (SimplePluginManager) plugin.getServer().getPluginManager();

        try {
            Field field = SimplePluginManager.class.getDeclaredField("commandMap");
            field.setAccessible(true);

            return (SimpleCommandMap) field.get(manager);
        } catch (Exception e) {

            return null;
        }

    }

    private SimpleSubCommandExecutor executor;

    private Plugin plugin;

    private String prefix;

    private ICommandExecutor commandExecutor;

    private final List<ISimpleSubCommand> subCommandList = new ArrayList<>();
    private final List<String> usableCommands = new ArrayList<>();

    public SimpleCommand(SimpleCommandHandler handler, ICommandExecutor executor, Plugin plugin) {
        this(handler.name(), handler.prefix(), executor.getDescription(), executor.getUsage(), Arrays.asList(handler.aliases()),
                handler.permission(), executor.getPermissionMessage(), plugin, executor);
    }

    public SimpleCommand(String name, String prefix, String description, String usageMessage, List<String> aliases, String permission,
                         String permissionMessage, Plugin plugin, ICommandExecutor executor) {
        super(name.toLowerCase().trim(), description, usageMessage, aliases);

        super.setPermission(permission);
        super.setPermissionMessage(permissionMessage);

        this.commandExecutor = executor;
        this.plugin = plugin;
        this.prefix = prefix.replace("%plugin%", plugin.getName()).toLowerCase().trim();
    }

    public boolean register() {
        if(this.isRegistered())
            return false;

        try {

            List<String> registeredAliases = new ArrayList<>();
            SimpleCommandMap commandMap = SimpleCommand.getCommandMap(this.getPlugin());
            Method registerCommand = SimpleCommandMap.class.getDeclaredMethod("register", String.class, Command.class, boolean.class, String.class);
            registerCommand.setAccessible(true);

            String label = this.getName();
            String fallbackPrefix = this.getPrefix();

            registeredAliases.add(fallbackPrefix + ":" + label);
            for (String alias : this.getAliases())
                registeredAliases.add(fallbackPrefix + ":" + alias);

            boolean registered;
            if (registered = (boolean) registerCommand.invoke(commandMap, label, this, false, fallbackPrefix))
                registeredAliases.add(label);

            Iterator<String> aliasIterator = this.getAliases().iterator();
            while (aliasIterator.hasNext()) if (!((boolean) registerCommand.invoke(commandMap, aliasIterator.next(), this, true, fallbackPrefix)))
                aliasIterator.remove();

            this.setLabel((registered ? (fallbackPrefix + ":") : "") + label);
            this.getUsableCommands().addAll(registeredAliases);

            return this.register(commandMap);
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

        return false;
    }

    public boolean unregister() {
        if(!this.isRegistered())
            return false;

        SimpleCommandMap commandMap = SimpleCommand.getCommandMap(this.getPlugin());
        if(!super.unregister(commandMap))
            return false;

        try {
            Field field = SimpleCommandMap.class.getDeclaredField("knownCommands");
            field.setAccessible(true);

            Iterator<Map.Entry<String, Command>> commandMapIterator = ((Map<String, Command>) field.get(commandMap)).entrySet().iterator();
            while(commandMapIterator.hasNext())
                if(commandMapIterator.next().getValue() == this)
                    commandMapIterator.remove();

            this.getUsableCommands().clear();

            return true;
        } catch (Exception e) {

            return false;
        }

    }

    public void forceUnregister() {
        if(!this.isRegistered())
            return;

        try {

            Field commandMapField = Command.class.getDeclaredField("commandMap");
            commandMapField.setAccessible(true);

            CommandMap commandMap;
            if((commandMap = (CommandMap) commandMapField.get(this)) == null)
                return;

            if(!super.unregister(commandMap))
                return;

            Field field = SimpleCommandMap.class.getDeclaredField("knownCommands");
            field.setAccessible(true);

            Iterator<Map.Entry<String, Command>> commandMapIterator = ((Map<String, Command>) field.get(commandMap)).entrySet().iterator();
            while(commandMapIterator.hasNext())
                if(commandMapIterator.next().getValue() == this)
                    commandMapIterator.remove();

            this.getUsableCommands().clear();

        } catch (Exception ignored) {}

    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {

        if(!commandSender.hasPermission(this.getPermission())) {
            commandSender.sendMessage(this.getPermissionMessage());
            return true;
        }

        try {

            this.getCommandExecutor().onExecute(commandSender, this, s, strings);

        } catch (IllegalFormatException exception) {

            commandSender.sendMessage(exception.getMessage());
        } catch (IllegalArgumentException exception) {

            this.getCommandExecutor().sendHelpMessage(commandSender, this);
        }

        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        return this.tabComplete(sender, alias, args, null);
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args, Location location) throws IllegalArgumentException {
        return this.getCommandExecutor().onTabComplete(sender, this, alias, args, location);
    }

}
