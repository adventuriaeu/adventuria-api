package de.fabtopf.contray.adventuria.api.player.skin;

import com.google.gson.*;
import de.fabtopf.contray.adventuria.api.AdviAPI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

class SkinFetcher {

    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private static final String SKIN_URL = "https://sessionserver.mojang.com/session/minecraft/profile/";
    private static final String UUID_URL = "https://api.mojang.com/users/profiles/minecraft/";

    public static Map<String, String> getSkinDataSave(String name) {

        try {
            UUID uniqueId;
            if((uniqueId = getUUID(name)) == null)
                return null;

            return getSkinData(uniqueId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static Map<String, String> getSkinDataSave(UUID uniqueId) {

        try {
            return getSkinData(uniqueId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static UUID getUUID(String name) {

        String redisResult;
        if((redisResult = AdviAPI.getInstance().saveRedisRequest(redis -> redis.get("api-player-uuid-cache-" + name.toLowerCase()), null)) != null) {
            Map<String, Object> map = GSON.fromJson(redisResult, HashMap.class);
            if(((Double) map.get("expires")).longValue() > System.currentTimeMillis())
                return UUID.fromString(((String) map.get("uuid")).replaceFirst("(\\p{XDigit}{8})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}+)", "$1-$2-$3-$4-$5"));

            AdviAPI.getInstance().saveRedisRequest(redis -> redis.del("api-player-uuid-cache-" + name.toLowerCase()), false);
        }

        try {
            HttpURLConnection connection = (HttpURLConnection) setupConnection(new URL(UUID_URL + name));

            if (connection.getResponseCode() == 429)
                throw new IllegalStateException("Rate limit reached");

            try (InputStream is = connection.getInputStream()) {
                String result = convertInputStreamToString(is);
                Map map = GSON.fromJson(result, HashMap.class);

                String uuidString;
                if ((uuidString = (String) map.get("id")) == null)
                    return null;

                Map<String, Object> cache = new HashMap<>();
                cache.put("expires", System.currentTimeMillis() + (1000 * 60 * 60 * 24));
                cache.put("uuid", uuidString);

                AdviAPI.getInstance().saveRedisRequest(redis -> redis.set("api-player-uuid-cache-" + name.toLowerCase(), GSON.toJson(cache)), null);
                return UUID.fromString(uuidString.replaceFirst("(\\p{XDigit}{8})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}+)", "$1-$2-$3-$4-$5"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Map<String, String> getSkinData(UUID uniqueId) throws IOException, JsonSyntaxException, IllegalStateException {
        HttpURLConnection connection = (HttpURLConnection) setupConnection(new URL(SKIN_URL +
                uniqueId.toString().replace("-", "") + "?unsigned=false"));

        if (connection.getResponseCode() == 429)
            throw new IllegalStateException("Rate limit reached");

        try (InputStream is = connection.getInputStream()) {
            String result = convertInputStreamToString(is);
            JsonObject obj = GSON.fromJson(result, JsonObject.class);

            JsonArray properties = obj.get("properties").getAsJsonArray();
            for (int i = 0; i < properties.size(); i++) {
                JsonObject property = properties.get(i).getAsJsonObject();

                String name = property.get("name").getAsString();
                String value = property.get("value").getAsString();
                String signature = property.get("signature").getAsString();

                if (!name.equals("textures"))
                    continue;

                Map<String, String> content = new HashMap<>();
                content.put("value", value);
                content.put("signature", signature);

                return content;
            }

            return null;
        }
    }

    private static URLConnection setupConnection(URL url) throws IOException {
        URLConnection connection = url.openConnection();

        connection.setConnectTimeout(10000);
        connection.setReadTimeout(10000);
        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);

        return connection;
    }

    private static String convertInputStreamToString(InputStream is) {
        return new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8)).lines().collect(Collectors.joining("\n"));
    }

}
