package de.fabtopf.contray.adventuria.api.utils.bukkit.command.timed;

import de.fabtopf.contray.adventuria.api.utils.feature.TimedCommand;
import org.bukkit.Bukkit;

public class BukkitTimedCommand extends TimedCommand {

    public BukkitTimedCommand(String command, long interval, long delay) {
        super(command, interval, delay);
    }

    @Override
    public void execute(String command) {
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
    }

}
