package de.fabtopf.contray.adventuria.api.internal.bukkit.listener;

import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.bridge.internal.util.ReflectionUtil;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.permission.AdviPermissible;
import de.fabtopf.contray.adventuria.api.permission.PermissionUser;
import de.fabtopf.contray.adventuria.api.player.LocalPlayer;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.player.PlayerProperty;
import de.fabtopf.contray.adventuria.api.utils.bukkit.chat.ChatChannel;
import de.fabtopf.contray.adventuria.api.utils.bukkit.chat.ChatManager;
import de.fabtopf.contray.adventuria.api.utils.cloud.ChannelMessageDocument;
import de.fabtopf.contray.adventuria.api.utils.event.bukkit.AdviChatEvent;
import de.fabtopf.contray.adventuria.api.utils.event.bukkit.PlayerNameContentEditEvent;
import de.fabtopf.contray.adventuria.api.utils.event.bukkit.PlayerUpdateEvent;
import fr.xephi.authme.api.v3.AuthMeApi;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

public class PlayerListener implements Listener {

    private Map<UUID, Long> connects = new HashMap<>();

    @EventHandler(priority = EventPriority.LOWEST)
    public void onLogin(PlayerLoginEvent event) {
        NetworkPlayer player = AdviAPI.getInstance().getPlayerProvider().get(event.getPlayer().getUniqueId());
        PermissionUser user = player.getPermissionUser();

        try {
            Field field;
            Class<?> clazz = ReflectionUtil.reflectCraftClazz(".entity.CraftHumanEntity");

            if (clazz != null) field = clazz.getDeclaredField("perm");
            else field = Class.forName("net.glowstone.entity.GlowHumanEntity").getDeclaredField("permissions");

            field.setAccessible(true);
            final AdviPermissible adviPermissible = new AdviPermissible(event.getPlayer());
            field.set(event.getPlayer(), adviPermissible);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        switch(AdviAPI.getInstance().bukkit().getState()) {
            case ONLINE:
                if(user.hasPermission("adventuria.api.general.fulljoin", AdviAPI.getInstance().getCloudUtilities().getGroup())) {
                    if (Bukkit.getOnlinePlayers().size() >= Bukkit.getMaxPlayers())
                        for(Player all : Bukkit.getOnlinePlayers())
                            if(!all.hasPermission("adventuria.api.general.fulljoin")) {
                                event.allow();
                                all.kickPlayer(BukkitCore.getInstance().getLanguage().getPhraseContent("kick-fulljoin"));
                                break;
                            }
                }

                break;

            case STATIC:
                if(user.hasPermission("adventuria.api.general.fulljoin", AdviAPI.getInstance().getCloudUtilities().getGroup()))
                    event.allow();
                break;

            case INGAME:
                event.allow();
                break;

            default:
                break;

        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent event) {
        connects.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());

        NetworkPlayer np;
        if((np = AdviAPI.getInstance().getPlayerProvider().handleJoin(event.getPlayer().getUniqueId())) == null)
            np = AdviAPI.getInstance().getPlayerProvider().get(event.getPlayer().getUniqueId());

        NetworkPlayer player = np;
        Bukkit.getScheduler().runTaskAsynchronously(BukkitCore.getInstance(), () -> {
            try {
                if(Bukkit.getPluginManager().isPluginEnabled("AuthMe")) {
                    if (AdviAPI.getInstance().saveRedisRequest(redis -> redis.exists("adviapi-authmebridge-" + player.getUniqueId().toString()), false)) {

                        String authmeInfo = AdviAPI.getInstance().saveRedisRequest(redis ->
                                AdviAPI.getInstance().getRedisClient().get("adviapi-authmebridge-" + player.getUniqueId().toString()), null);
                        Map<String, Object> authInfo = (Map<String, Object>) BukkitCore.getInstance().getGson().fromJson(authmeInfo, Map.class);

                        if(CloudAPI.getInstance().getOnlinePlayer(event.getPlayer().getUniqueId()).getPlayerConnection().getHost().equals(authInfo.get("address")) &&
                                Double.doubleToLongBits((double) authInfo.get("timestamp")) > System.currentTimeMillis() - 86400L) {
                            AuthMeApi.getInstance().forceLogin(event.getPlayer());
                            event.getPlayer().sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("system-integration-authme-autologin"));
                        }
                    }
                }
            } catch (Exception ignored) {

            }
        });

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onQuit(PlayerQuitEvent event) {
        handleDisconnect(event.getPlayer().getUniqueId());
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerUpdate(PlayerUpdateEvent event) {
        NetworkPlayer player = AdviAPI.getInstance().getPlayerProvider().get(event.getPlayer().getUniqueId());
        PermissionUser user = player.getPermissionUser();

        LocalPlayer local = AdviAPI.getInstance().getPlayerProvider().getLocal(player.getUniqueId());
        if(local == null || user == null)
            return;

        if(!user.getPrefix().equals(local.getPrefix()) ||
            !user.getSuffix().equals(local.getSuffix()) ||
            !user.getDisplay().equals(local.getDisplay())) {

            local.setPrefix(user.getPrefix());
            local.setSuffix(user.getSuffix());
            local.setDisplay(user.getDisplay());

            Bukkit.getPluginManager().callEvent(new PlayerNameContentEditEvent(event.getPlayer()));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onChatTrigger(AsyncPlayerChatEvent event) {
        if(event.isCancelled())
            return;

        if(AdviAPI.getInstance().getChatManager() == null)
            return;

        ChatManager chatManager = AdviAPI.getInstance().getChatManager();

        NetworkPlayer player = AdviAPI.getInstance().getPlayerProvider().get(event.getPlayer().getUniqueId());
        ChatChannel channel = chatManager.getChannel(player.getProperty(PlayerProperty.CHAT_CHANNEL)
                .getOrDefault(AdviAPI.getInstance().getServerName(), chatManager.getDefaultChannel().getName()));

        for(ChatChannel ch : chatManager.getChannels())
            if(ch.getPrefix() != null)
                if(event.getMessage().startsWith(ch.getPrefix()) && (ch.getWritePermission() == null || event.getPlayer().hasPermission(ch.getWritePermission())))
                    channel = ch;

        if(channel.getWritePermission() != null &&
                !event.getPlayer().hasPermission(channel.getWritePermission()) &&
                !channel.isDefault())
            channel = chatManager.getDefaultChannel();

        Map<String, Object> info = new HashMap<>();

        info.put("server", AdviAPI.getInstance().getServerName());

        if(channel.getWritePermission() == null || event.getPlayer().hasPermission(channel.getWritePermission()))
            event.setCancelled(chatManager.invokeChatMessage(player, event.getMessage(), channel.getName(), false, info));
        else event.setCancelled(true);

        if(event.isCancelled())
            return;

        event.setCancelled(true);
        Bukkit.broadcastMessage(chatManager.convertChannelFormat(null, player, event.getMessage(), channel, false, info));
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onAdviChat(AdviChatEvent event) {
        if(event.isCancelled())
            return;

        ChatChannel channel;
        if((channel = AdviAPI.getInstance().getChatManager().getChannel(event.getChannel())) == null) {

            if(event.getInvoker() == null)
                Bukkit.getLogger().warning(BukkitCore.getInstance().getLanguage().getPhraseContent("system-chat-channel-doesnt-exist-console"));
            else if(!event.isExternalTrigger())
                Bukkit.getPlayer(event.getInvoker().getUniqueId()).sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("system-chat-channel-doesnt-exist-player"));

            event.setCancelled(true);
            return;
        }

        String message = event.getMessage().trim().replaceFirst(Pattern.quote(channel.getPrefix()), "");
        if(!event.isCommand() && !event.isExternalTrigger() && message.equals("")) {

            if(event.getInvoker() == null)
                Bukkit.getLogger().warning(BukkitCore.getInstance().getLanguage().getPhraseContent("system-chat-no-args"));
            else if(!event.isExternalTrigger())
                Bukkit.getPlayer(event.getInvoker().getUniqueId()).sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("system-chat-no-args"));

            event.setCancelled(true);
            return;
        }

        if(!event.isExternalTrigger() &&
                ((channel.getType() == ChatChannel.Type.GLOBAL || channel.getType() == ChatChannel.Type.CUSTOM) &&
                        channel.getCommunicationChannel() != null)) {

            AdviAPI.getInstance().getCloudUtilities().sendServerChannelMessage("advi-chat", channel.getCommunicationChannel(),
                    new ChannelMessageDocument(new Document()
                            .append("channel", channel.getCommunicationChannel())
                            .append("invoker", event.getInvoker() == null ? null : event.getInvoker().getUniqueId().toString())
                            .append("message", message)
                            .append("from", AdviAPI.getInstance().getServerName())
                            .append("command", event.isCommand())
                            .append("info", event.getInfo())));
            AdviAPI.getInstance().getCloudUtilities().sendProxyChannelMessage("advi-chat", channel.getCommunicationChannel(),
                    new ChannelMessageDocument(new Document()
                            .append("channel", channel.getCommunicationChannel())
                            .append("invoker", event.getInvoker() == null ? null : event.getInvoker().getUniqueId().toString())
                            .append("message", message)
                            .append("from", AdviAPI.getInstance().getServerName())
                            .append("command", event.isCommand())
                            .append("info", event.getInfo())));

        }

        switch(channel.getType()) {

            case LOCAL:
                Bukkit.getLogger().info("[Chat-" + event.getChannel() + "] " +
                        (event.getInvoker() == null ? "CONSOLE" : event.getInvoker().getName()) + " » " +
                        event.getMessage());

                event.setCancelled(true);
                Bukkit.getOnlinePlayers().forEach(p -> {

                    NetworkPlayer player = AdviAPI.getInstance().getPlayerProvider().get(p.getUniqueId());
                    boolean receivedMessage = false;
                    if (((!player.getProperty(PlayerProperty.IGNORED_CHAT_CHANNEL).contains(channel) || channel.isDefault()) &&
                            (channel.getReadPermission() == null || p.hasPermission(channel.getReadPermission()))) || (event.getInvoker() != null && p.getName().equals(event.getInvoker().getName()))) {

                        if(event.getInvoker() == null || (event.getInvoker().isOnline() && (Bukkit.getPlayer(event.getInvoker().getUniqueId()).getLocation().getWorld().equals(p.getLocation().getWorld()) &&
                            Bukkit.getPlayer(event.getInvoker().getUniqueId()).getLocation().distance(p.getLocation()) <= channel.getRange()))) {
                            AdviAPI.getInstance().getChatManager().sendChatMessage(p.getUniqueId(), event.getInvoker(), event.getMessage(), channel, event.isCommand(), event.getInfo());
                            receivedMessage = true;
                        }
                    }

                    if (player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL).contains(channel) &&
                            channel.getSpyPermission() != null &&
                            p.hasPermission(channel.getSpyPermission()) &&
                            !receivedMessage) {
                        AdviAPI.getInstance().getChatManager().sendSpyMessage(p.getUniqueId(), event.getInvoker(), event.getMessage(), channel, event.isCommand(), event.getInfo());
                    }

                });
                break;

            case GLOBAL:
                Bukkit.getLogger().info("[Chat-" + event.getChannel() + "] " +
                        (event.getInvoker() == null ? "CONSOLE" : event.getInvoker().getName()) + " » " +
                        event.getMessage());

                event.setCancelled(true);
                Bukkit.getOnlinePlayers().forEach(p -> {

                    NetworkPlayer player = AdviAPI.getInstance().getPlayerProvider().get(p.getUniqueId());
                    boolean receivedMessage = false;
                    if ((!player.getProperty(PlayerProperty.IGNORED_CHAT_CHANNEL).contains(channel) || channel.isDefault()) &&
                            (channel.getReadPermission() == null || p.hasPermission(channel.getReadPermission())) ||
                            (event.getInvoker() != null && p.getName().equals(event.getInvoker().getName()))) {
                        AdviAPI.getInstance().getChatManager().sendChatMessage(p.getUniqueId(), event.getInvoker(), event.getMessage(), channel, event.isCommand(), event.getInfo());
                        receivedMessage = true;
                    }

                    if (player.getProperty(PlayerProperty.SPY_CHAT_CHANNEL).contains(channel) &&
                            channel.getSpyPermission() != null &&
                            p.hasPermission(channel.getSpyPermission()) &&
                            !receivedMessage) {
                        AdviAPI.getInstance().getChatManager().sendSpyMessage(p.getUniqueId(), event.getInvoker(), event.getMessage(), channel, event.isCommand(), event.getInfo());
                    }

                });
                break;

            case CUSTOM:
                event.setCancelled(true);
                break;

        }

    }

    public void handleDisconnect(UUID uniqueId) {
        NetworkPlayer player = AdviAPI.getInstance().getPlayerProvider().getNoCached(uniqueId);
        Map<String, Long> onlineTimeSpent = new HashMap<>(player.getProperty(PlayerProperty.ONTIME));

        onlineTimeSpent.put(AdviAPI.getInstance().getCloudUtilities().getGroup(),
                System.currentTimeMillis()
                        - connects.getOrDefault(uniqueId, System.currentTimeMillis())
                        + onlineTimeSpent.getOrDefault(
                        AdviAPI.getInstance().getCloudUtilities().getGroup(),
                        0L));

        player.updateProperty(PlayerProperty.ONTIME, onlineTimeSpent);
        player.updateProperty(PlayerProperty.TOTAL_ONTIME, player.getProperty(PlayerProperty.TOTAL_ONTIME) +
                System.currentTimeMillis() - connects.getOrDefault(uniqueId, System.currentTimeMillis()));

        connects.remove(uniqueId);

        AdviAPI.getInstance().getPlayerProvider().getLocalPlayerMap().remove(uniqueId);
        AdviAPI.getInstance().getPlayerProvider().uncache(uniqueId);
    }

}
