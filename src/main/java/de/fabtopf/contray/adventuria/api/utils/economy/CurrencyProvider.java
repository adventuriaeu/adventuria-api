package de.fabtopf.contray.adventuria.api.utils.economy;

import com.arangodb.ArangoCursor;
import com.arangodb.util.MapBuilder;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.utils.arango.CollectionManager;

import java.io.IOException;
import java.util.UUID;

public class CurrencyProvider extends CollectionManager<Currency, UUID> {

    CurrencyProvider() {
        super("economy_currencies", Currency::new, true, (key, obj) -> !AdviAPI.getInstance().isProxy());
    }

    public Currency get(String name) {

        UUID uniqueId;
        if((uniqueId = getUniqueId(name)) != null)
            return get(uniqueId);

        return null;
    }

    public UUID getUniqueId(String name) {

        ArangoCursor<String> cursor = AdviAPI.getInstance().getArangoDatabase()
                .query("FOR currency IN " + this.collection.name() + " FILTER LIKE(currency.name, @currency, true) RETURN currency._key",
                        new MapBuilder().put("currency", name.toLowerCase()).get(),
                        String.class);

        UUID uniqueId = null;
        if(cursor.hasNext())
            uniqueId = UUID.fromString(cursor.next());

        try {
            cursor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return uniqueId;
    }

}
