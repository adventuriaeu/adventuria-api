package de.fabtopf.contray.adventuria.api.internal.bukkit;

import com.arangodb.util.MapBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.fabtopf.contray.adventuria.api.AdviAPIBukkit;
import de.fabtopf.contray.adventuria.api.internal.AdviConfiguration;
import de.fabtopf.contray.adventuria.api.internal.PluginSupport;
import de.fabtopf.contray.adventuria.api.internal.bukkit.command.*;
import de.fabtopf.contray.adventuria.api.internal.bukkit.listener.AuthMeListener;
import de.fabtopf.contray.adventuria.api.internal.bukkit.listener.CloudNetListener;
import de.fabtopf.contray.adventuria.api.internal.bukkit.listener.PlayerListener;
import de.fabtopf.contray.adventuria.api.utils.Language;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.alias.AliasManager;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommand;
import de.fabtopf.contray.adventuria.api.utils.bukkit.inventory.SimpleInventoryProvider;
import de.fabtopf.contray.adventuria.api.utils.bukkit.inventory.SimpleItemProvider;
import de.fabtopf.contray.adventuria.api.utils.vault.VaultChatImpl;
import de.fabtopf.contray.adventuria.api.utils.vault.VaultEconomyImpl;
import de.fabtopf.contray.adventuria.api.utils.vault.VaultPermissionImpl;
import fr.xephi.authme.AuthMe;
import lombok.Getter;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

@Getter
public class BukkitCore extends JavaPlugin {

    @Getter
    private static BukkitCore instance;

    private Language language;
    private AdviAPIBukkit api;

    private Gson gson;

    private AliasManager aliasManager;

    private PlayerListener playerListener;

    @Override
    public void onLoad() {
        try {
            PluginSupport.loadFiles(new File("libraries"), this.getClassLoader());
        } catch (Exception e) {
            System.err.println("[AdviAPI-Bukkit] Beim laden externer Bibliotheken ist ein Fehler aufgetreten!");
        }

        try {
            Class.forName("net.milkbowl.vault.permission.Permission");

            VaultPermissionImpl vaultPermission;
            this.getServer().getServicesManager().register(Permission.class, vaultPermission = new VaultPermissionImpl(), this, ServicePriority.Highest);
            System.out.println("[AdviAPI-Bukkit] Vault Permission-Hook enabled!");

            this.getServer().getServicesManager().register(Chat.class, new VaultChatImpl(vaultPermission), this, ServicePriority.Highest);
            System.out.println("[AdviAPI-Bukkit] Vault Chat-Hook enabled!");

            this.getServer().getServicesManager().register(Economy.class, new VaultEconomyImpl(), this, ServicePriority.Highest);
            System.out.println("[AdviAPI-Bukkit] Vault Economy-Hook enabled!");

        } catch (Exception ignored) {}
    }

    @Override
    public void onEnable() {
        instance = this;

        this.language = new Language("api-messages.properties", this.getClass());
        this.api = new AdviAPIBukkit(this.loadConfiguration());
        this.gson = new GsonBuilder().setPrettyPrinting().create();

        try {

            if(AuthMe.getPluginName() != null)
                Bukkit.getPluginManager().registerEvents(new AuthMeListener(), this);

        } catch (Exception ignored) {}

        SimpleCommand.analyze(this,
                PerformanceCommand.class,
                LogCommand.class,
                MessageCommand.class,
                MoneyCommand.class,
                SeatCommand.class,
                SpyCommand.class,
                SkinCommand.class);

        //this.aliasManager = new AliasManager();

        this.getServer().getPluginManager().registerEvents(new CloudNetListener(), this);
        this.getServer().getPluginManager().registerEvents((playerListener = new PlayerListener()), this);
        this.getServer().getPluginManager().registerEvents(new SimpleInventoryProvider.ActionListener(), this);
        this.getServer().getPluginManager().registerEvents(new SimpleItemProvider.ActionListener(), this);
    }

    @Override
    public void onDisable() {
        Bukkit.getOnlinePlayers().stream().map(OfflinePlayer::getUniqueId).forEach(playerListener::handleDisconnect);

        this.api.cleanup();
    }

    public File getLogFile() {
        File latestLog = new File("logs/spigot.log.0");

        if(!latestLog.exists())
            latestLog = new File("logs/latest.log");

        return latestLog;
    }

    public AdviConfiguration loadConfiguration() {

        this.reloadConfig();

        this.getConfig().options().copyDefaults(true);

        this.getConfig().addDefault("database.arango.host", "127.0.0.1");
        this.getConfig().addDefault("database.arango.port", 8529);
        this.getConfig().addDefault("database.arango.user", "adventuria");
        this.getConfig().addDefault("database.arango.password", "*******");
        this.getConfig().addDefault("database.arango.database", "adventuria");

        this.getConfig().addDefault("database.redis.host", "127.0.0.1");
        this.getConfig().addDefault("database.redis.port", 6379);
        this.getConfig().addDefault("database.redis.password", "*******");

        this.getConfig().addDefault("additional.isLobby", false);
        this.getConfig().addDefault("additional.isDevServer", false);

        this.saveConfig();
        this.reloadConfig();

        AdviConfiguration configuration = new AdviConfiguration();

        configuration.setArangoHost(this.getConfig().getString("database.arango.host"));
        configuration.setArangoPort(this.getConfig().getInt("database.arango.port"));
        configuration.setArangoUser(this.getConfig().getString("database.arango.user"));
        configuration.setArangoPassword(this.getConfig().getString("database.arango.password"));
        configuration.setArangoDatabase(this.getConfig().getString("database.arango.database"));

        configuration.setRedisHost(this.getConfig().getString("database.redis.host"));
        configuration.setRedisPort(this.getConfig().getInt("database.redis.port"));
        configuration.setRedisPassword(this.getConfig().getString("database.redis.password"));

        configuration.setAdditional(new MapBuilder()
                .put("lobby", this.getConfig().getBoolean("additional.isLobby"))
                .put("devServer", this.getConfig().getBoolean("additional.isDevServer"))
                .get());

        return configuration;
    }

}
