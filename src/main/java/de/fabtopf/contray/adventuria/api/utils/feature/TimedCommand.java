package de.fabtopf.contray.adventuria.api.utils.feature;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public abstract class TimedCommand {

    private String command;

    private long interval;
    private long delay;

    public abstract void execute(String command);

}
