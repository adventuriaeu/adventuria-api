package de.fabtopf.contray.adventuria.api.utils.arango.parser;

public class ObjectParser<F, T> implements ArangoParser.FieldModifier<F, T> {
    private String field;
    private ArangoParser.ParseMethod<F, T> parseMethod;

    public ObjectParser(String field, ArangoParser.ParseMethod<F, T> parseMethod) {
        this.field = field;
        this.parseMethod = parseMethod;
    }

    public String getField() {
        return field;
    }

    @Override
    public T getValue(F input) {
        return this.parseMethod.parse(input);
    }

    public ArangoParser.ParseMethod<F, T> getParseMethod() {
        return parseMethod;
    }

}
