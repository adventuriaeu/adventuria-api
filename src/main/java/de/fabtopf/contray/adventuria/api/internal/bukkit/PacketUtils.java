package de.fabtopf.contray.adventuria.api.internal.bukkit;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class PacketUtils {

    private String version;

    private Class<?> packetClass;

    private Class<?> packetPlayOutCustomPayloadClass;
    private Constructor<?> customPayloadConstructor;
    private boolean customPayloadHasBytes;

    private Class<?> packetDataSerializerClass;
    private Constructor<?> packetDataSerializerConstructor;

    private Method getHandleMethod;
    private Field playerConnectionField;
    private Field networkManagerField;

    public PacketUtils() {
        this.version = Bukkit.getServer().getClass().getPackage().getName().replace( ".", "," ).split( "," )[3];

        try {
            this.packetClass = getNmsClass( "Packet" );
            this.packetPlayOutCustomPayloadClass = getNmsClass( "PacketPlayOutCustomPayload" );
            this.networkManagerField = getNmsClass( "PlayerConnection" ).getDeclaredField( "networkManager" );
        } catch ( ClassNotFoundException | NoSuchFieldException e ) {
            e.printStackTrace();
        }

        if ( this.packetPlayOutCustomPayloadClass != null ) {
            for ( Constructor<?> constructors : packetPlayOutCustomPayloadClass.getDeclaredConstructors() ) {
                if ( constructors.getParameterTypes().length == 2 && constructors.getParameterTypes()[1] == byte[].class ) {
                    customPayloadHasBytes = true;
                    customPayloadConstructor = constructors;
                } else if ( constructors.getParameterTypes().length == 2 && constructors.getParameterTypes()[1].getSimpleName().equals( "PacketDataSerializer" ) ) {
                    customPayloadConstructor = constructors;
                }
            }

            if ( !customPayloadHasBytes ) {
                try {
                    packetDataSerializerClass = getNmsClass( "PacketDataSerializer" );
                    packetDataSerializerConstructor = packetDataSerializerClass.getDeclaredConstructor( ByteBuf.class );
                } catch ( Exception ex ) {
                    ex.printStackTrace();
                    Bukkit.getLogger().severe( "Couldn't find a valid constructor for PacketPlayOutCustomPayload. Disabling the plugin." );
                }
            }
        }
    }

    public Object getPlayerHandle( Player player ) {
        try {
            if ( getHandleMethod == null )
                getHandleMethod = player.getClass().getMethod( "getHandle" );

            // Getting the player's nms-handle
            return getHandleMethod.invoke( player );
        } catch ( Exception ex ) {
            ex.printStackTrace();
        }

        return null;
    }

    public Object getPlayerConnection( Object nmsPlayer ) {
        try {
            if ( playerConnectionField == null )
                playerConnectionField = nmsPlayer.getClass().getField( "playerConnection" );

            // Getting the player's connection
            return playerConnectionField.get( nmsPlayer );
        } catch ( IllegalAccessException | NoSuchFieldException e ) {
            e.printStackTrace();
        }

        return null;
    }

    public void sendPacket( Player player, Object packet ) {
        try {
            Object nmsPlayer = getPlayerHandle( player );

            Object playerConnection = getPlayerConnection( nmsPlayer );

            playerConnection.getClass().getMethod( "sendPacket", packetClass ).invoke( playerConnection, packet );
        } catch ( Exception ex ) {
            ex.printStackTrace();
        }
    }

    public Object getPluginMessagePacket( String channel, byte[] bytes ) {
        try {
            return customPayloadConstructor.newInstance( channel, customPayloadHasBytes ? bytes : packetDataSerializerConstructor.newInstance( Unpooled.wrappedBuffer( bytes ) ) );
        } catch ( NullPointerException | InstantiationException | IllegalAccessException | InvocationTargetException e ) {
            Bukkit.getLogger().severe( "Couldn't construct a custom-payload packet (Channel: " + channel + "):" );
            e.printStackTrace();
        }

        return null;
    }

    public Class<?> getNmsClass( String nmsClassName ) throws ClassNotFoundException {
        return Class.forName( "net.minecraft.server." + version + "." + nmsClassName );
    }

    public void setField( Object targetObject, String fieldName, Object value ) {
        try {
            Field field = targetObject.getClass().getDeclaredField( fieldName );
            field.setAccessible( true );

            field.set( targetObject, value );
        } catch ( Exception ex ) {
            ex.printStackTrace();
        }
    }

    public Field getNetworkManagerField() {
        return networkManagerField;
    }

}
