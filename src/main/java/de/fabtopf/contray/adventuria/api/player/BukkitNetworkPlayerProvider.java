package de.fabtopf.contray.adventuria.api.player;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.player.skin.SkinData;
import de.fabtopf.contray.adventuria.api.utils.arango.CollectionManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.*;

public class BukkitNetworkPlayerProvider extends NetworkPlayerProvider {

    public BukkitNetworkPlayerProvider() {
        super(new CollectionManager.CacheCondition<NetworkPlayer, UUID>() {

           @Override
           public boolean checkCache(UUID uniqueId, NetworkPlayer networkPlayer) {
               return !AdviAPI.getInstance().isOnline(uniqueId);
           }

        });
    }

    public NetworkPlayer get(Player player) {
        return super.get(player.getUniqueId());
    }

    @Override
    public void updateSkin(UUID uniqueId, String skinName) {

        Bukkit.getScheduler().scheduleSyncDelayedTask(BukkitCore.getInstance(), () -> {
            try {

                Player player;
                if((player = Bukkit.getPlayer(uniqueId)) == null)
                    return;

                SkinData data;
                if((data = AdviAPI.getInstance().bukkit().getSkinProvider().get(skinName)) == null)
                    return;

                Object entityPlayer = player.getClass().getMethod("getHandle").invoke(player);
                Object profile = entityPlayer.getClass().getMethod("getProfile").invoke(entityPlayer);
                Object propMap = profile.getClass().getMethod("getProperties").invoke(profile);

                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                System.out.println(gson.toJson(data));
                System.out.println(gson.toJson(propMap));
                propMap.getClass().getMethod("clear").invoke(propMap);
                propMap.getClass().getMethod("put", Object.class, Object.class).invoke(propMap, "textures", data.getProperty());
                System.out.println(gson.toJson(propMap));

                Bukkit.getScheduler().runTaskAsynchronously(BukkitCore.getInstance(), () -> {

                    Location location = player.getLocation();

                    try {
                        String serverPrefix = "net.minecraft.server." + AdviAPI.getInstance().bukkit().getPackageVersion() + ".";
                        String craftServerPrefix = "org.bukkit.craftbukkit." + AdviAPI.getInstance().bukkit().getPackageVersion() + ".";

                        Class<?> craftPlayerClass = Class.forName(craftServerPrefix + "entity.CraftPlayer");
                        Class<?> entityPlayerClass = Class.forName(serverPrefix + "EntityPlayer");
                        Class<?> entityHumanClass = Class.forName(serverPrefix + "EntityHuman");
                        Class<?> dimensionManagerClass = Class.forName(serverPrefix + "DimensionManager");
                        Class<?> craftItemStackClass = Class.forName(craftServerPrefix + "inventory.CraftItemStack");
                        Class<?> itemStackClass = Class.forName(serverPrefix + "ItemStack");
                        Class<?> enumItemSlotClass = Class.forName(serverPrefix + "EnumItemSlot");

                        Class<?> packetPlayOutPlayerInfoClass = Class.forName(serverPrefix + "PacketPlayOutPlayerInfo");
                        Class<?> packetPlayOutEntityDestroyClass = Class.forName(serverPrefix + "PacketPlayOutEntityDestroy");
                        Class<?> packetPlayOutNamedEntitySpawnClass = Class.forName(serverPrefix + "PacketPlayOutNamedEntitySpawn");
                        Class<?> playerInfoActionClass = Class.forName(serverPrefix + "PacketPlayOutPlayerInfo$EnumPlayerInfoAction");
                        Class<?> packetPlayOutRespawnClass = Class.forName(serverPrefix + "PacketPlayOutRespawn");
                        Class<?> packetPlayOutPositionClass = Class.forName(serverPrefix + "PacketPlayOutPosition");
                        Class<?> packetPlayOutEntityEquipmentClass = Class.forName(serverPrefix + "PacketPlayOutEntityEquipment");
                        Class<?> packetPlayOutHeldItemSlotClass = Class.forName(serverPrefix + "PacketPlayOutHeldItemSlot");

                        Enum<?> playerInfoActionRemovePlayer = (Enum<?>) Arrays.stream(playerInfoActionClass.getEnumConstants()).filter(e -> ((Enum<?>) e).name().equals("REMOVE_PLAYER")).findAny().get();
                        Enum<?> playerInfoActionAddPlayer = (Enum<?>) Arrays.stream(playerInfoActionClass.getEnumConstants()).filter(e -> ((Enum<?>) e).name().equals("ADD_PLAYER")).findAny().get();
                        Enum<?> mainHand = (Enum<?>) Arrays.stream(enumItemSlotClass.getEnumConstants()).filter(e -> ((Enum<?>) e).name().equals("MAINHAND")).findAny().get();
                        Enum<?> offHand = (Enum<?>) Arrays.stream(enumItemSlotClass.getEnumConstants()).filter(e -> ((Enum<?>) e).name().equals("OFFHAND")).findAny().get();
                        Enum<?> head = (Enum<?>) Arrays.stream(enumItemSlotClass.getEnumConstants()).filter(e -> ((Enum<?>) e).name().equals("HEAD")).findAny().get();
                        Enum<?> chest = (Enum<?>) Arrays.stream(enumItemSlotClass.getEnumConstants()).filter(e -> ((Enum<?>) e).name().equals("CHEST")).findAny().get();
                        Enum<?> feet = (Enum<?>) Arrays.stream(enumItemSlotClass.getEnumConstants()).filter(e -> ((Enum<?>) e).name().equals("FEET")).findAny().get();
                        Enum<?> legs = (Enum<?>) Arrays.stream(enumItemSlotClass.getEnumConstants()).filter(e -> ((Enum<?>) e).name().equals("LEGS")).findAny().get();

                        Method getHandleMethod = craftPlayerClass.getMethod("getHandle");
                        Method getWorldMethod = entityPlayerClass.getMethod("getWorld");
                        Object world = getWorldMethod.invoke(entityPlayer);

                        Method getDifficultyMethod = world.getClass().getMethod("getDifficulty");
                        Object difficulty = getDifficultyMethod.invoke(world);

                        Object worldData = world.getClass().getField("worldData").get(world);

                        Method getWorldType = worldData.getClass().getMethod("getType");
                        Object worldType = getWorldType.invoke(worldData);

                        World.Environment environment = player.getWorld().getEnvironment();

                        Object playerInteractionManager = entityPlayerClass.getField("playerInteractManager").get(entityPlayer);

                        Method getGameMode = playerInteractionManager.getClass().getMethod("getGameMode");
                        Enum<?> gameMode = (Enum<?>) getGameMode.invoke(playerInteractionManager);

                        Method getDimensionManager = dimensionManagerClass.getDeclaredMethod("a", Integer.TYPE);
                        Object dimensionManager = getDimensionManager.invoke(null, environment.getId());

                        List<Object> set = new ArrayList<>();
                        set.add(entityPlayer);

                        Constructor<?> playOutEntityEquipmentConstructor = packetPlayOutEntityEquipmentClass.getConstructor(int.class, enumItemSlotClass, itemStackClass);
                        Method getNmsCopy = craftItemStackClass.getDeclaredMethod("asNMSCopy", ItemStack.class);
                        Object removeInfoPacket = packetPlayOutPlayerInfoClass.getConstructor(playerInfoActionClass, Iterable.class).newInstance(playerInfoActionRemovePlayer, set);
                        Object removeEntityPacket = packetPlayOutEntityDestroyClass.getConstructor(int[].class).newInstance(new int[]{player.getEntityId()});
                        Object addNamedPacket = packetPlayOutNamedEntitySpawnClass.getConstructor(entityHumanClass).newInstance(entityPlayer);
                        Object addInfoPacket = packetPlayOutPlayerInfoClass.getConstructor(playerInfoActionClass, Iterable.class).newInstance(playerInfoActionAddPlayer, set);
                        Object respawnPacket = packetPlayOutRespawnClass.getConstructor(dimensionManagerClass, difficulty.getClass(), worldType.getClass(), gameMode.getClass())
                                .newInstance(dimensionManager, difficulty, worldType, gameMode);
                        Object positionPacket = packetPlayOutPositionClass.getConstructor(double.class, double.class, double.class, float.class, float.class, Set.class, int.class)
                                .newInstance(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch(), new HashSet<Enum<?>>(), 0);
                        Object mainHandPacket = playOutEntityEquipmentConstructor.newInstance(player.getEntityId(), mainHand, getNmsCopy.invoke(null, player.getInventory().getItemInMainHand()));
                        Object offHandPacket = playOutEntityEquipmentConstructor.newInstance(player.getEntityId(), offHand, getNmsCopy.invoke(null, player.getInventory().getItemInOffHand()));
                        Object headPacket = playOutEntityEquipmentConstructor.newInstance(player.getEntityId(), head, getNmsCopy.invoke(null, player.getInventory().getHelmet()));
                        Object chestPacket = playOutEntityEquipmentConstructor.newInstance(player.getEntityId(), chest, getNmsCopy.invoke(null, player.getInventory().getChestplate()));
                        Object feetPacket = playOutEntityEquipmentConstructor.newInstance(player.getEntityId(), feet, getNmsCopy.invoke(null, player.getInventory().getBoots()));
                        Object legsPacket = playOutEntityEquipmentConstructor.newInstance(player.getEntityId(), legs, getNmsCopy.invoke(null, player.getInventory().getLeggings()));
                        Object slotPacket = packetPlayOutHeldItemSlotClass.getConstructor(int.class).newInstance(player.getInventory().getHeldItemSlot());

                        for(Player target : Bukkit.getOnlinePlayers()) {
                            Object targetHandle = getHandleMethod.invoke(target);
                            Object connection =  entityPlayerClass.getField("playerConnection").get(targetHandle);

                            if(target == player) {

                                sendPacket(connection, removeInfoPacket);
                                //sendPacket(connection, addInfoPacket);
                                sendPacket(connection, respawnPacket);

                                Bukkit.getScheduler().scheduleSyncDelayedTask(BukkitCore.getInstance(), () -> {
                                    try {
                                        entityPlayerClass.getMethod("updateAbilities").invoke(targetHandle);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                });

                                sendPacket(connection, positionPacket);
                                sendPacket(connection, slotPacket);

                                craftPlayerClass.getMethod("updateScaledHealth").invoke(target);
                                craftPlayerClass.getMethod("updateInventory").invoke(target);
                                entityPlayerClass.getMethod("triggerHealthUpdate").invoke(targetHandle);

                                continue;
                            }

                            if(target.getWorld().equals(player.getWorld()) && target.canSee(player)) {

                                sendPacket(connection, removeEntityPacket);
                                sendPacket(connection, removeInfoPacket);
                                sendPacket(connection, addInfoPacket);
                                sendPacket(connection, addNamedPacket);

                                sendPacket(connection, mainHandPacket);
                                sendPacket(connection, offHandPacket);
                                sendPacket(connection, headPacket);
                                sendPacket(connection, chestPacket);
                                sendPacket(connection, legsPacket);
                                sendPacket(connection, feetPacket);

                            } else {

                                sendPacket(connection, removeInfoPacket);
                                sendPacket(connection, addInfoPacket);

                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    private void sendPacket(Object playerConnection, Object packet) throws Exception {
        playerConnection.getClass().getMethod("sendPacket", Class.forName("net.minecraft.server." + AdviAPI.getInstance().bukkit().getPackageVersion() + ".Packet")).invoke(playerConnection, packet);
    }
}
