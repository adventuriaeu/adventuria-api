package de.fabtopf.contray.adventuria.api.utils;

import de.fabtopf.contray.adventuria.api.player.skin.SkinData;
import net.minecraft.server.v1_13_R2.NBTTagCompound;
import net.minecraft.server.v1_13_R2.NBTTagList;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ItemBuilder {

    private ItemStack item;

    public ItemBuilder(Material material)
    {
        this(material, 1);
    }

    public ItemBuilder(Material material, int amount)
    {
        this(material, amount, (short) 0);
    }

    public ItemBuilder(Material material, short subId)
    {
        this (material, 1, subId);
    }

    public ItemBuilder(Material material, int amount, short subId)
    {
        this(new ItemStack(material, amount, subId));
    }

    public ItemBuilder(ItemStack item)
    {
        this.item = item;
    }

    public ItemBuilder amount(int amount) {
        this.item.setAmount(amount);
        return this;
    }

    public ItemBuilder displayname(String name) {
        ItemMeta meta = this.item.getItemMeta();
        meta.setDisplayName(name);
        this.item.setItemMeta(meta);

        return this;
    }

    public ItemBuilder lore(String... lore)
    {
        return this.lore(Arrays.asList(lore));
    }

    public ItemBuilder lore(List<String> lore) {
        ItemMeta meta = this.item.getItemMeta();
        meta.setLore(lore);

        this.item.setItemMeta(meta);

        return this;
    }

    public ItemBuilder durability(short durability) {
        this.item.setDurability(durability);

        return this;
    }

    public ItemBuilder breakable() {
        this.item.getItemMeta().spigot().setUnbreakable(false);

        return this;
    }

    public ItemBuilder unbreakable() {
        this.item.getItemMeta().spigot().setUnbreakable(true);

        return this;
    }

    public ItemBuilder enchant(Enchantment enchantment, int strength) {
        this.item.addEnchantment(enchantment, strength);

        return this;
    }

    public ItemBuilder enchantUnsafe(Enchantment enchantment, int strength) {
        this.item.addUnsafeEnchantment(enchantment, strength);

        return this;
    }

    public ItemBuilder skullData(SkinData data)
    {
        return this.skullData(data.getValue());
    }

    public ItemBuilder skullData(String value) {
        Bukkit.getUnsafe().modifyItemStack(
                this.item,
                "{SkullOwner:{Id:\"" +
                        new UUID(value.hashCode(), value.hashCode()) +
                        "\",Properties:{textures:[{Value:\"" +
                        value +
                        "\"}]}}}");

        return this;
    }

    public ItemBuilder skullOwner(String owner) {
        SkullMeta meta = (SkullMeta) this.item.getItemMeta();
        meta.setOwner(owner);
        this.item.setItemMeta(meta);

        return this;
    }

    public ItemBuilder armorColor(Color color) {
        LeatherArmorMeta meta = (LeatherArmorMeta) this.item.getItemMeta();
        meta.setColor(color);
        this.item.setItemMeta(meta);

        return this;
    }

    public ItemBuilder glow() {
        net.minecraft.server.v1_13_R2.ItemStack nmsStack = CraftItemStack.asNMSCopy(this.item);
        NBTTagCompound nbtTag;

        if (!nmsStack.hasTag())
            nmsStack.setTag(new NBTTagCompound());

        nbtTag = nmsStack.getTag();
        nbtTag.set("ench", new NBTTagList());
        nmsStack.setTag(nbtTag);

        this.item = CraftItemStack.asBukkitCopy(nmsStack);

        return this;
    }

    public ItemStack build()
    {
        return this.item;
    }

    @Override
    public ItemBuilder clone()
    {
        return new ItemBuilder(this.item.clone());
    }

    public static ItemBuilder skullBuilder() {
        return new ItemBuilder(Material.PLAYER_HEAD);
    }

}
