package de.fabtopf.contray.adventuria.api.utils.bukkit.inventory;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import lombok.Getter;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.meta.tags.ItemTagType;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Getter
public class SimpleInventoryProvider {

    private final List<SimpleInventory> inventories = new ArrayList<>();

    public void activateInventoryListeners(SimpleInventory inventory) {
        if(!this.inventories.contains(inventory))
            this.inventories.add(inventory);
    }

    public void deactivateInventoryListeners(SimpleInventory inventory) {
        this.inventories.remove(inventory);
    }

    public static class ActionListener implements Listener {

        @EventHandler
        public void onClick(InventoryClickEvent event) {
            if(event.getClickedInventory() == null)
                return;

            SimpleInventory inventory = null;
            for(SimpleInventory inv : AdviAPI.getInstance().bukkit().getInventoryProvider().getInventories())
                if(event.getClickedInventory().equals(inv.getInstance()))
                    inventory = inv;

            if(inventory == null)
                return;

            if(inventory.getClickAction() != null)
                inventory.getClickAction().call(event, inventory);

            UUID uniqueId = null;
            if(event.getCurrentItem() != null && event.getCurrentItem().getItemMeta() != null && event.getCurrentItem().getItemMeta().getCustomTagContainer().hasCustomTag(SimpleItemProvider.ITEM_IDENTIFIER, ItemTagType.STRING))
                uniqueId = UUID.fromString(event.getCurrentItem().getItemMeta().getCustomTagContainer().getCustomTag(SimpleItemProvider.ITEM_IDENTIFIER, ItemTagType.STRING));

            if(uniqueId == null)
                return;

            List<SimpleItem> itemList = new ArrayList<>();
            itemList.addAll(inventory.getVariableContent());
            itemList.addAll(inventory.getContent().values());
            itemList.add(inventory.getLayout().getEmptySlotFiller());

            if(inventory.getLayout() instanceof MultiPageInventoryLayout) {
                itemList.add(((MultiPageInventoryLayout) inventory.getLayout()).getVariableSlotFiller());
                itemList.add(((MultiPageInventoryLayout) inventory.getLayout()).getCurrentPageDisplay());
                itemList.add(((MultiPageInventoryLayout) inventory.getLayout()).getNextPageItem());
                itemList.add(((MultiPageInventoryLayout) inventory.getLayout()).getPreviousInventoryItem());
                itemList.add(((MultiPageInventoryLayout) inventory.getLayout()).getPreviousPageItem());
            }

            itemList.removeIf(Objects::isNull);
            for(SimpleItem item : itemList)
                if(item.getUniqueId().equals(uniqueId)) {
                    if(item.isCancelAction())
                        event.setCancelled(true);

                    if (item.getClickAction() != null)
                        item.getClickAction().call(event, inventory, item);
                }

        }

        @EventHandler
        public void onOpen(InventoryOpenEvent event) {

            SimpleInventory inventory = null;
            for(SimpleInventory inv : AdviAPI.getInstance().bukkit().getInventoryProvider().getInventories())
                if(event.getInventory().equals(inv.getInstance()))
                    inventory = inv;

            if(inventory == null)
                return;

            if(inventory.getOpenAction() != null)
                inventory.getOpenAction().call(event, inventory);

        }

        @EventHandler
        public void onClose(InventoryCloseEvent event) {

            SimpleInventory inventory = null;
            for(SimpleInventory inv : AdviAPI.getInstance().bukkit().getInventoryProvider().getInventories())
                if(event.getInventory().equals(inv.getInstance()))
                    inventory = inv;

            if(inventory == null)
                return;

            if(inventory.getCloseAction() != null)
                inventory.getCloseAction().call(event, inventory);

            if(inventory.isTemporary() && event.getInventory().getViewers().stream().filter(e -> !e.equals(event.getPlayer())).count() == 0)
                AdviAPI.getInstance().bukkit().getInventoryProvider().deactivateInventoryListeners(inventory);

        }

    }

}
