package de.fabtopf.contray.adventuria.api;

import de.fabtopf.contray.adventuria.api.internal.AdviConfiguration;
import de.fabtopf.contray.adventuria.api.internal.bungee.BungeeCore;
import de.fabtopf.contray.adventuria.api.player.BungeeNetworkPlayerProvider;
import de.fabtopf.contray.adventuria.api.utils.bukkit.chat.BungeeChatManager;
import lombok.Getter;

import java.io.File;
import java.util.UUID;

@Getter
public class AdviAPIBungee extends AdviAPI {

    private BungeeNetworkPlayerProvider playerProvider;

    public AdviAPIBungee(AdviConfiguration configuration) {
        super(configuration);

        this.playerProvider = new BungeeNetworkPlayerProvider();
        this.chatManager = new BungeeChatManager(new File(BungeeCore.getInstance().getDataFolder(), "chat_settings.yml"));
    }

    @Override
    public boolean isOnline(UUID uniqueId) {
        return cloudUtilities.isOnline(uniqueId);
    }

    @Override
    public void cleanup() {
        super.cleanup();
    }

}
