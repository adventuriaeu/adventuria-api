package de.fabtopf.contray.adventuria.api.utils.cloud;

import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.bridge.CloudServer;
import de.dytanic.cloudnet.lib.server.ServerGroupMode;
import de.dytanic.cloudnet.lib.server.ServerState;
import de.dytanic.cloudnet.lib.server.info.ServerInfo;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.AdviAPIBukkit;

import java.util.*;

public class CloudUtilities {

    public Collection<ServerData> getServers(String group) {
        Collection<ServerInfo> infos = CloudAPI.getInstance().getServers(group);
        List<ServerData> servers = new ArrayList<>(infos.size());

        for(ServerInfo info : infos)
            servers.add(new ServerData(info));

        Collections.sort(servers);

        return servers;
    }

    public Collection<ServerData> getServers() {
        Collection<ServerInfo> infos = CloudAPI.getInstance().getServers();
        List<ServerData> servers = new ArrayList<>(infos.size());

        for(ServerInfo info : infos)
            servers.add(new ServerData(info));

        Collections.sort(servers);

        return servers;
    }

    public int getGlobalPlayerCount() {
        return CloudAPI.getInstance().getOnlineCount();
    }

    public int getMaxPlayerCount() {
        return CloudAPI.getInstance().getProxyGroupData("Proxy").getProxyConfig().getMaxPlayers();
    }

    public ServerData getServerData(String server) {
        ServerInfo data = CloudAPI.getInstance().getServerInfo(server);

        if(data == null)
            return null;

        return new ServerData(data);
    }

    public ServerData getData() {
        if(AdviAPI.getInstance() instanceof AdviAPIBukkit)
            return getServerData(AdviAPI.getInstance().getServerName());

        return null;
    }

    public String getGroup() {
        return CloudAPI.getInstance().getGroup();
    }

    public String getServerName() {
        return CloudAPI.getInstance().getServerId();
    }

    public void sendProxyChannelMessage(String channel, String message, ChannelMessageDocument document) {
        CloudAPI.getInstance().sendCustomSubProxyMessage(channel, message, document.export());
    }

    public void sendProxyChannelMessage(String channel, String message, ChannelMessageDocument document, String proxy) {
        CloudAPI.getInstance().sendCustomSubProxyMessage(channel, message, document.export(), proxy);
    }

    public void sendServerChannelMessage(String channel, String message, ChannelMessageDocument document) {
        CloudAPI.getInstance().sendCustomSubServerMessage(channel, message, document.export());
    }

    public void sendServerChannelMessage(String channel, String message, ChannelMessageDocument document, String server) {
        CloudAPI.getInstance().sendCustomSubServerMessage(channel, message, document.export(), server);
    }

    public boolean isOnline(UUID unqiueId) {
        return CloudAPI.getInstance().getOnlinePlayer(unqiueId) != null;
    }

    public boolean isDynamic() {
        return CloudAPI.getInstance().getServerGroup(getGroup()).getGroupMode() == ServerGroupMode.DYNAMIC ||
            CloudAPI.getInstance().getServerGroup(getGroup()).getGroupMode() == ServerGroupMode.LOBBY;
    }

    public void setMotd(String motd) {
        if(AdviAPI.getInstance() instanceof AdviAPIBukkit)
            CloudServer.getInstance().setMotd(motd);
    }

    public void setServerState(ServerState state) {
        if(AdviAPI.getInstance() instanceof AdviAPIBukkit)
            CloudServer.getInstance().setServerState(state);
    }

    public void setMaxPlayers(int maxPlayers) {
        if(AdviAPI.getInstance() instanceof AdviAPIBukkit)
            CloudServer.getInstance().setMaxPlayers(maxPlayers);
    }

    public void update() {
        if(AdviAPI.getInstance() instanceof AdviAPIBukkit)
            CloudServer.getInstance().update();
    }

}
