package de.fabtopf.contray.adventuria.api.utils.arango;

public interface WritableGenerator<Writable extends ArangoWritable<KeyType>, KeyType> {
    Writable generate(KeyType key);
}
