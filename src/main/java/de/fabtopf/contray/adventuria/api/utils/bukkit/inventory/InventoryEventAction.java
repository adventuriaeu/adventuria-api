package de.fabtopf.contray.adventuria.api.utils.bukkit.inventory;

import org.bukkit.event.Event;

public interface InventoryEventAction<E extends Event> {

    void call(E event, SimpleInventory inventory);

}
