package de.fabtopf.contray.adventuria.api.utils.bukkit.command.timed;

import de.fabtopf.contray.adventuria.api.utils.feature.TimedCommandManager;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;

public class BukkitTimedCommandManager extends TimedCommandManager {

    private BukkitTask task = null;
    private FileConfiguration configuration;

    public BukkitTimedCommandManager(File config, Plugin plugin) {
        super(config, plugin);

        this.configuration = YamlConfiguration.loadConfiguration(this.file);
    }

    public BukkitTimedCommandManager(Plugin plugin) {
        super(plugin);
    }

    @Override
    protected void runScheduler() {
        if(this.task == null)
            this.task = Bukkit.getScheduler()
                    .runTaskTimerAsynchronously(this.plugin, new CommandRunnable(), 0L, 20L);
    }

    @Override
    protected void stopScheduler() {
        if(this.task == null)
            return;

        Bukkit.getScheduler().cancelTask(this.task.getTaskId());
        this.task = null;
    }

    @Override
    protected String replaceCommandValues(String command) {
        return null;
    }

    @Override
    protected void readCommands() {

        this.commands.clear();
        for(String key : configuration.getKeys(false)) {

            //TODO


        }

    }

}
