package de.fabtopf.contray.adventuria.api.internal.bukkit.command;

import com.sun.management.OperatingSystemMXBean;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommand;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommandHandler;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleFunctionCommandExecutor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.management.ManagementFactory;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SimpleCommandHandler(name = "performance", permission = "adventuria.api.command.performance", aliases = { "perf" })
public class PerformanceCommand implements Runnable, SimpleFunctionCommandExecutor {

    private final DecimalFormat format = new DecimalFormat("#0.000");
    private List<UUID> subscribed = new ArrayList<>();

    private int count = 0;
    private int taskId = -1;

    private long avgRam = 0;
    private long minRam = 0;
    private long maxRam = 0;

    private double avgCpu = 0;
    private double maxCpu = 0;
    private double minCpu = 0;

    @Override
    public void run() {

        if (++count <= 0 || count > 600) {

            Bukkit.getScheduler().cancelTask(taskId);
            taskId = -1;

            return;
        }

        long currentRam = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed() / 1048576L;

        if (minRam > currentRam || minRam <= 0) minRam = currentRam;
        if (maxRam < currentRam) maxRam = currentRam;

        avgRam = ((avgRam * (count - 1)) + currentRam) / count;

        double currentCpu = ((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getProcessCpuLoad() * 100.0D;

        if (minCpu > currentCpu || minCpu <= 0) minCpu = currentCpu;
        if (maxCpu < currentCpu) maxCpu = currentCpu;

        avgCpu = ((avgCpu * (count - 1)) + currentCpu) / count;

        for (UUID uuid : subscribed) {

            if (Bukkit.getPlayer(uuid) != null) {

                Bukkit.getPlayer(uuid).sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent(
                        "command-performance-output",
                        AdviAPI.getInstance().getServerName(),
                        minRam, avgRam, maxRam,
                        format.format(minCpu), format.format(avgCpu), format.format(maxCpu),
                        currentRam, format.format(currentCpu)));
            }

        }

    }

    @Override
    public void sendHelpMessage(CommandSender executor, SimpleCommand command) {

    }

    @Override
    public String getUsage() {
        return "/performance";
    }

    @Override
    public String getPermissionMessage() {
        return BukkitCore.getInstance().getLanguage().getPhraseContent("no-permission");
    }

    @Override
    public String getDescription() {
        return "Zeigt Performance-Statistiken des aktuellen Servers an.";
    }

    @Override
    public void onExecute(CommandSender sender, SimpleCommand command, String s, String[] strings) {
        if (!(sender instanceof Player))
            return;

        Player player = (Player) sender;

        if (this.subscribed.contains(player.getUniqueId())) {
            this.subscribed.remove(player.getUniqueId());
            sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-performance-unsubscribed"));
        } else {
            this.subscribed.add(player.getUniqueId());
            sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-performance-subscribed"));
        }

        if (this.taskId != -1)
            return;


        count = 1;
        minRam = maxRam = avgRam = 0;
        minCpu = maxCpu = avgCpu = 0;
        this.taskId = Bukkit.getScheduler().runTaskTimerAsynchronously(BukkitCore.getInstance(), this, 20, 20).getTaskId();

        sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-performance-started"));

        return;
    }
}
