package de.fabtopf.contray.adventuria.api.utils;

import java.io.*;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashSet;
import java.util.Properties;
import java.util.UUID;

public class Language {

    private static Language instance;

    private Properties properties;
    private Collection<String> requested;

    public Language(String filename, Class<?> clazz) {
        Language.instance = this;

        this.requested = new HashSet<>();
        this.properties = new Properties();

        try {
            properties.load(
                    new InputStreamReader(
                            clazz.getClassLoader().getResourceAsStream(filename),
                            "UTF-8"));

            System.out.println("Loaded PhraseFile " + filename);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Could not load PhraseFile " + filename);
        }
    }

    public Language(File file) {
        Language.instance = this;

        this.requested = new HashSet<>();
        this.properties = new Properties();

        try {
            properties.load(new InputStreamReader(new FileInputStream(file), "UTF-8"));

            System.out.println("Loaded PhraseFile " + file.getName());
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Could not load PhraseFile " + file.getName());
        }
    }

    public String getPhraseContent(String message, Object... objects) {
        if(!requested.contains(message) && !properties.containsKey(message))
            requested.add(message);

        return MessageFormat.format(
                this.properties.getProperty(message, "§cUnbekannte Nachricht: " + message),
                objects);
    }

    public static String getPhrase(String message, Object... objects) {
        if(Language.instance == null)
            return "§cEs wurde bisher keine Datei geladen!";

        return Language.instance.getPhraseContent(message, objects);
    }

    public static void dumpMissing() {
        for(String key : Language.instance.requested)
            Language.instance.properties.setProperty(key, UUID.randomUUID().toString());

        try (OutputStream stream = new FileOutputStream(new File("missingMessages.properties"))) {
            Language.instance.properties.store(stream, "Lorem ipsum dolor");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
