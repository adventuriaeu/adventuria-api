package de.fabtopf.contray.adventuria.api.internal.bukkit.command;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import de.fabtopf.contray.adventuria.api.utils.FileUtilities;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommand;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleSubCommandExecutor;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommandHandler;
import org.bukkit.command.CommandSender;

import java.io.File;

@SimpleCommandHandler(name = "log", permission = "adventuria.api.command.log")
public class LogCommand implements SimpleSubCommandExecutor {

    @Override
    public String getUsage() {
        return "/<command>";
    }

    @Override
    public String getPermissionMessage() {
        return BukkitCore.getInstance().getLanguage().getPhraseContent("no-permission");
    }

    @Override
    public String getDescription() {
        return "Erstellt eine Kopie des Serverlogs.";
    }

    @Override
    public void onExecute(CommandSender sender, SimpleCommand command, String label, String[] args) {
        if (!sender.hasPermission("adventuria.api.commands.log")) {
            sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("no-permission"));
            return;
        }

        File latestLog = BukkitCore.getInstance().getLogFile();
        if (latestLog.exists()) {
            String content = FileUtilities.readFile(
                    latestLog, "###############################################################################"
                            + "\n# CREATED BY AdviAPI-Bukkit"
                            + "\n# ONLY FOR USER RELATED TO Adventuria.eu\n# "
                            + "\n# AdviAPI-Version: " + BukkitCore.getInstance().getDescription().getVersion()
                            + "\n# Server: " + AdviAPI.getInstance().getServerName()
                            + "\n# Template: " + AdviAPI.getInstance().getCloudUtilities().getData().getTemplate()
                            + "\n###############################################################################\n\n\n");

            if (content != null)
                sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent(
                        "command-log-success",
                        FileUtilities.pasteToHastebin(content) + ".md"));
            else
                sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent(
                        "command-log-logReadError"));

        } else
            sender.sendMessage(BukkitCore.getInstance().getLanguage().getPhraseContent("command-log-noLogFound"));

    }

}
