package de.fabtopf.contray.adventuria.api.internal.bungee.listener;

import de.dytanic.cloudnet.bridge.event.proxied.ProxiedCustomChannelMessageReceiveEvent;
import de.dytanic.cloudnet.bridge.event.proxied.ProxiedPlayerUpdateEvent;
import de.dytanic.cloudnet.bridge.event.proxied.ProxiedSubChannelMessageEvent;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.permission.PermissionGroup;
import de.fabtopf.contray.adventuria.api.permission.PermissionUser;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.utils.cloud.ChannelMessageDocument;
import de.fabtopf.contray.adventuria.api.utils.economy.EconomyProvider;
import de.fabtopf.contray.adventuria.api.utils.event.bungee.ChannelMessageEvent;
import de.fabtopf.contray.adventuria.api.utils.event.bungee.PermissionGroupUpdateEvent;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;

public class CloudNetListener implements Listener {

    @EventHandler
    public void onMessage(ProxiedSubChannelMessageEvent event) {
        if(event.getChannel().equals("advi-player")) {
            UUID uniqueId = event.getDocument().getObject("uniqueId", UUID.class);
            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uniqueId);

            switch(event.getMessage()) {

                case "update":
                    AdviAPI.getInstance().getPlayerProvider().uncache(uniqueId);
                    break;

                case "send":
                    if(player != null)
                        player.connect(ProxyServer.getInstance().getServerInfo(
                                event.getDocument().getString("server")));
                    break;

                case "message":
                    if(player != null)
                        player.sendMessage(TextComponent.fromLegacyText(event.getDocument().getString("message")));
                    break;

                default:
                    break;

            }

        } else if(event.getChannel().equals("advi-group")) {
            String group = event.getDocument().getString("group");
            String collection = event.getDocument().getString("collection");

            if(group == null)
                return;

            switch(event.getMessage()) {

                case "update":
                    AdviAPI.getInstance().getPermissionPool().uncache(group);

                    PermissionGroup group1 = AdviAPI.getInstance().getPermissionPool().get(group);
                    AdviAPI.getInstance().getPermissionPool().getGroups().stream().filter(p -> p.getImplementedGroups().contains(group1)).forEach(g -> {
                        g.clearCachedPermissions();
                        AdviAPI.getInstance().getPlayerProvider().getOnlinePlayers().stream().map(NetworkPlayer::getPermissionUser).filter(u -> u.getCurrentGroups().contains(g)).forEach(PermissionUser::clearCachedPermissions);
                    });

                    BungeeCord.getInstance().getPluginManager().callEvent(new PermissionGroupUpdateEvent(
                            group1, collection));
                    break;

                default:
                    break;

            }

        } else if(event.getChannel().equals("advi-economy")) {

            switch(event.getMessage()) {

                case "update-currency":
                    EconomyProvider.getCurrencyProvider().uncache(event.getDocument().getObject("uniqueId", UUID.class));
                    break;

                default:
                    break;

            }

        } else {
            ProxyServer.getInstance().getPluginManager().callEvent(new ChannelMessageEvent(event.getChannel(), event.getMessage(), new ChannelMessageDocument(event.getDocument())));
        }
    }

    @EventHandler
    public void onMessage(ProxiedCustomChannelMessageReceiveEvent event) {
        ProxyServer.getInstance().getPluginManager().callEvent(new ChannelMessageEvent(event.getChannel(), event.getMessage(), new ChannelMessageDocument(event.getDocument())));
    }

    @EventHandler
    public void onPlayerUpdate(ProxiedPlayerUpdateEvent event) {
        if(BungeeCord.getInstance().getPlayer(event.getCloudPlayer().getUniqueId()) != null)
            AdviAPI.getInstance().getPlayerProvider().get(event.getCloudPlayer().getUniqueId()).sendPropertyUpdate();
    }

}
