package de.fabtopf.contray.adventuria.api.utils.economy;

import com.arangodb.entity.BaseDocument;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.utils.arango.ArangoWritable;
import de.fabtopf.contray.adventuria.api.utils.cloud.ChannelMessageDocument;
import lombok.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Getter
@Setter
public class Currency implements ArangoWritable<UUID> {

    @Setter(AccessLevel.NONE)
    private UUID uniqueId;

    private String name;
    private String sign;

    private float tax;
    private float transactionTax;

    private boolean allowMain;
    private boolean tradeable;
    private boolean offlineTax;

    @Setter(AccessLevel.NONE)
    private Map<UUID, Translator> translatorMap;

    public Currency(UUID uniqueId) {
        this.uniqueId = uniqueId;

        this.name = "No name set!";
        this.sign = "$";
        this.tax = 0;
        this.offlineTax = false;
        this.transactionTax = 0;
        this.allowMain = true;
        this.tradeable = true;

        this.translatorMap = new HashMap<>();
    }

    public UUID getKey() {
        return uniqueId;
    }

    @Override
    public void read(BaseDocument document) {
        this.name = (String) document.getAttribute("name");
        this.sign = (String) document.getAttribute("sign");

        this.tax = ((Double) document.getAttribute("tax")).floatValue();
        this.transactionTax = ((Double) document.getAttribute("transactionTax")).floatValue();
        this.offlineTax = (boolean) document.getAttribute("offlineTax");
        this.tradeable = (boolean) document.getAttribute("tradeable");
        this.allowMain = (boolean) document.getAttribute("allowMain");

        for(Map<String, Object> entries : (List<Map<String, Object>>) document.getAttribute("translators"))
            translatorMap.put(UUID.fromString((String) entries.get("translateTo")),
                    new Translator(
                            UUID.fromString((String) entries.get("translateTo")),
                            (long) entries.get("baseCost"),
                            (long) entries.get("minTranslation"),
                            ((Double) entries.get("tax")).floatValue()));
    }

    @Override
    public void save(BaseDocument document) {
        document.addAttribute("name", name);
        document.addAttribute("sign", sign);
        document.addAttribute("tax", tax);
        document.addAttribute("offlineTax", offlineTax);
        document.addAttribute("transactionTax", transactionTax);
        document.addAttribute("tradeable", tradeable);
        document.addAttribute("allowMain", allowMain);
        document.addAttribute("translators", translatorMap.values());
    }

    public Translator updateTranslator(UUID currency, long baseCost, long minTranslation, float tax) {

        Translator translator;
        if((translator = this.translatorMap.get(currency)) == null)
            this.translatorMap.put(currency, translator = new Translator(currency));

        translator.setBaseCost(baseCost);
        translator.setMinTranslation(minTranslation);
        translator.setTax(tax);
        save();

        return translator;
    }

    public boolean removeTranslator(UUID currency) {
        return this.translatorMap.remove(currency) != null;
    }

    private void save() {
        EconomyProvider.getCurrencyProvider().save(this);
        EconomyProvider.getCurrencyProvider().uncache(this.uniqueId);

        AdviAPI.getInstance().getCloudUtilities().sendServerChannelMessage("advi-economy", "update-currency",
                new ChannelMessageDocument(new Document().append("uniqueId", this.uniqueId)));
        AdviAPI.getInstance().getCloudUtilities().sendProxyChannelMessage("advi-economy", "update-currency",
                new ChannelMessageDocument(new Document().append("uniqueId", this.uniqueId)));
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @RequiredArgsConstructor
    private class Translator {

        @NonNull
        @Setter(AccessLevel.NONE)
        private UUID translateTo;

        private long baseCost;
        private long minTranslation;
        private float tax;

    }

}
