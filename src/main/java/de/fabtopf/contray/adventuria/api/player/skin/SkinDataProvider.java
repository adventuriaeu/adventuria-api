package de.fabtopf.contray.adventuria.api.player.skin;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.utils.arango.CollectionManager;
import de.fabtopf.contray.adventuria.api.utils.arango.WritableGenerator;

import java.util.Map;
import java.util.UUID;

public class SkinDataProvider extends CollectionManager<SkinData, UUID> {

    public SkinDataProvider() {
        super("skin", new WritableGenerator<SkinData, UUID>() {

            @Override
            public SkinData generate(UUID key) {
                return new SkinData(key);
            }

        }, true, new CacheCondition<SkinData, UUID>() {

            @Override
            public boolean checkCache(UUID key, SkinData obj) {
                return obj.getValue() != null &&
                        !obj.getCurrentUsers().isEmpty();
            }

        });

    }

    public boolean isFetchable(UUID uniqueId) {
        return this.getFetchTimeout(uniqueId) == 0;
    }

    public long getFetchTimeout(UUID uniqueId) {
        String result;
        if((result = AdviAPI.getInstance().saveRedisRequest(redis -> redis.get("skindata-fetcher-delay-" + uniqueId.toString()), null)) == null)
            return 0;

        long timeout;
        if((timeout = Long.parseLong(result) - System.currentTimeMillis()) > 0)
            return timeout;

        AdviAPI.getInstance().saveRedisRequest(redis -> AdviAPI.getInstance().getRedisClient().del("skindata-fetcher-delay-" + uniqueId.toString()), false);

        return 0;
    }

    private void updateFetchTimeout(UUID uniqueId) {
        AdviAPI.getInstance().saveRedisRequest(redis ->
                redis.set("skindata-fetcher-delay-" + uniqueId.toString(), Long.toString(System.currentTimeMillis() + 600000L)), null);
    }

    public SkinData get(String name) {

        UUID uniqueId;
        if((uniqueId = SkinFetcher.getUUID(name)) == null)
            return null;

        return this.get(uniqueId);
    }

    @Override
    public SkinData get(UUID uniqueId) {

        SkinData data;
        if((data = super.get(uniqueId)) != null)
            return data;

        if(!this.isFetchable(uniqueId))
            return null;

        this.updateFetchTimeout(uniqueId);

        Map<String, String> content;
        if((content = SkinFetcher.getSkinDataSave(uniqueId)) == null)
            return null;

        this.register(uniqueId);
        data = this.getNoCached(uniqueId);

        data.setSignature(content.get("signature"));
        data.setValue(content.get("value"));

        this.save(data);

        if(cacheCondition.checkCache(uniqueId, data))
            cache.put(uniqueId, data);

        return data;
    }

    public void update(UUID uniqueId) {
        if(!isFetchable(uniqueId))
            return;

        updateFetchTimeout(uniqueId);

        Map<String, String> content;
        if((content = SkinFetcher.getSkinDataSave(uniqueId)) == null)
            return;

        SkinData data = this.get(uniqueId);

        data.setSignature(content.get("signature"));
        data.setValue(content.get("value"));

        this.save(data);

    }

}
