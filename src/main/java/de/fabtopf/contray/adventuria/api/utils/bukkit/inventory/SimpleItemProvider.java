package de.fabtopf.contray.adventuria.api.utils.bukkit.inventory;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.internal.bukkit.BukkitCore;
import lombok.Getter;
import org.bukkit.NamespacedKey;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.meta.tags.ItemTagType;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
public class SimpleItemProvider {

    static final NamespacedKey ITEM_IDENTIFIER = new NamespacedKey(BukkitCore.getInstance(), "item-identifier");

    private final List<SimpleItem> items = new ArrayList<>();

    public void activateItemListeners(SimpleItem item) {
        if(!this.items.contains(item))
            this.items.add(item);
    }

    public void deactivateItemListeners(SimpleItem item) {
        this.items.remove(item);
    }

    public static class ActionListener implements Listener {

        @EventHandler
        public void onInventoryClick(InventoryClickEvent event) {
            if(event.getCurrentItem() == null)
                return;

            if(event.getCurrentItem().getItemMeta() == null || !event.getCurrentItem().getItemMeta().getCustomTagContainer().hasCustomTag(ITEM_IDENTIFIER, ItemTagType.STRING))
                return;

            SimpleItem simpleItem = null;
            UUID uniqueId = UUID.fromString(event.getCurrentItem().getItemMeta().getCustomTagContainer().getCustomTag(ITEM_IDENTIFIER, ItemTagType.STRING));
            for(SimpleItem item : AdviAPI.getInstance().bukkit().getItemProvider().getItems())
                if(item.getUniqueId().equals(uniqueId))
                    simpleItem = item;

            if(simpleItem == null)
                return;

            if(simpleItem.isCancelAction())
                event.setCancelled(true);

            if(simpleItem.getClickAction() != null)
                simpleItem.getClickAction().call(event, null, simpleItem);

        }

        @EventHandler
        public void onInteract(PlayerInteractEvent event) {
            if(event.getItem() == null || event.getItem().getItemMeta() == null)
                return;

            if(!event.getItem().getItemMeta().getCustomTagContainer().hasCustomTag(ITEM_IDENTIFIER, ItemTagType.STRING))
                return;

            SimpleItem simpleItem = null;
            UUID uniqueId = UUID.fromString(event.getItem().getItemMeta().getCustomTagContainer().getCustomTag(ITEM_IDENTIFIER, ItemTagType.STRING));
            for(SimpleItem item : AdviAPI.getInstance().bukkit().getItemProvider().getItems())
                if(item.getUniqueId().equals(uniqueId))
                    simpleItem = item;

            if(simpleItem == null)
                return;

            if(simpleItem.isCancelAction())
                event.setCancelled(true);

            if(simpleItem.getInteractAction() != null)
                simpleItem.getInteractAction().call(event, null, simpleItem);

        }

        @EventHandler
        public void onInteractAtEntity(PlayerInteractAtEntityEvent event) {
            if(event.getPlayer().getItemOnCursor().getItemMeta() == null)
                return;

            if(!event.getPlayer().getItemOnCursor().getItemMeta().getCustomTagContainer().hasCustomTag(ITEM_IDENTIFIER, ItemTagType.STRING))
                return;

            SimpleItem simpleItem = null;
            UUID uniqueId = UUID.fromString(event.getPlayer().getItemOnCursor().getItemMeta().getCustomTagContainer().getCustomTag(ITEM_IDENTIFIER, ItemTagType.STRING));
            for(SimpleItem item : AdviAPI.getInstance().bukkit().getItemProvider().getItems())
                if(item.getUniqueId().equals(uniqueId))
                    simpleItem = item;

            if(simpleItem == null)
                return;

            if(simpleItem.isCancelAction())
                event.setCancelled(true);

            if(simpleItem.getInteractAtEntityAction() != null)
                simpleItem.getInteractAtEntityAction().call(event, null, simpleItem);

        }

        @EventHandler
        public void onDropItem(PlayerDropItemEvent event) {
            if(event.getItemDrop().getItemStack().getItemMeta() == null)
                return;

            if(!event.getItemDrop().getItemStack().getItemMeta().getCustomTagContainer().hasCustomTag(ITEM_IDENTIFIER, ItemTagType.STRING))
                return;

            SimpleItem simpleItem = null;
            UUID uniqueId = UUID.fromString(event.getItemDrop().getItemStack().getItemMeta().getCustomTagContainer().getCustomTag(ITEM_IDENTIFIER, ItemTagType.STRING));
            for(SimpleItem item : AdviAPI.getInstance().bukkit().getItemProvider().getItems())
                if(item.getUniqueId().equals(uniqueId))
                    simpleItem = item;

            if(simpleItem == null)
                return;

            if(simpleItem.isCancelAction())
                event.setCancelled(true);

            if(simpleItem.getDropAction() != null)
                simpleItem.getDropAction().call(event, null, simpleItem);

        }

    }

}
