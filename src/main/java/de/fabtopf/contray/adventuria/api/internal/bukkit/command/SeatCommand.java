package de.fabtopf.contray.adventuria.api.internal.bukkit.command;

import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommand;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleCommandHandler;
import de.fabtopf.contray.adventuria.api.utils.bukkit.command.base.SimpleFunctionCommandExecutor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@SimpleCommandHandler(name = "trojaner", permission = "adventuria.api.command.trojaner")
public class SeatCommand implements SimpleFunctionCommandExecutor {

    @Override
    public void sendHelpMessage(CommandSender executor, SimpleCommand command) {

    }

    @Override
    public String getUsage() {
        return "no usage available";
    }

    @Override
    public String getPermissionMessage() {
        return "§cBist du dir §lwirklich §csicher?";
    }

    @Override
    public String getDescription() {
        return "no description available";
    }

    @Override
    public void onExecute(CommandSender sender, SimpleCommand command, String label, String[] args) {
        if(!(sender instanceof Player))
            return;

        Player player = (Player) sender;

        if(args.length == 1) {

            Player target;
            if((target = Bukkit.getPlayer(args[0])) != null && target != player)
                target.addPassenger(player);

        }
    }

}
