package de.fabtopf.contray.adventuria.api.utils.bukkit.inventory;

import org.bukkit.event.inventory.InventoryType;

public interface MultiPageInventoryLayout extends InventoryLayout {

    int[] getVariableSlots();

    boolean fillEmptyVariableSlots();

    SimpleItem getCurrentPageDisplay();
    SimpleItem getNextPageItem();
    SimpleItem getPreviousPageItem();
    SimpleItem getVariableSlotFiller();
    SimpleItem getPreviousInventoryItem();

    void setCurrentPageDisplayPosition(SimpleInventory inventory, SimpleItem item, int page);
    void setNextPageItemPosition(SimpleInventory inventory, SimpleItem item, int page);
    void setPreviousPageItemPosition(SimpleInventory inventory, SimpleItem item, int page);
    void setPreviousInventoryItemPosition(SimpleInventory inventory, SimpleItem item);

    @Override
    default InventoryType getType() {
        return InventoryType.CHEST;
    }

}
