package de.fabtopf.contray.adventuria.api.utils.bukkit.scoreboard;

import de.fabtopf.contray.adventuria.api.AdviAPI;
import de.fabtopf.contray.adventuria.api.player.NetworkPlayer;
import de.fabtopf.contray.adventuria.api.player.PlayerProperty;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

@Getter
public class AdviScoreboard {

    private final transient ScoreboardManager manager;

    private final String name;

    private String[] title;

    private int interval;

    @Setter
    private boolean defaultScoreboard;

    private final ArrayList<ScoreboardValue> values = new ArrayList<>();

    public AdviScoreboard(ScoreboardManager manager, String name, boolean isDefault, String... title) {
        this(manager, name, isDefault, title, 10);
    }

    public AdviScoreboard(ScoreboardManager manager, String name, boolean isDefault, String[] title, int interval) {
        this.manager = manager;

        this.name = name;
        this.title = title;
        this.defaultScoreboard = isDefault;
        this.interval = interval;
    }

    public void forceUpdate() {
        Bukkit.getOnlinePlayers().forEach(this::forceUpdate);
    }

    public void forceUpdate(List<Player> players) {
        players.forEach(this::forceUpdate);
    }

    public void forceUpdate(Player... players) {
        for(Player player : players)
            forceUpdate(player);
    }

    private void forceUpdate(Player player) {
        if(getValues().size() > 4)
            throw new IllegalStateException("too many values");

        NetworkPlayer netPlayer;
        if((netPlayer = AdviAPI.getInstance().getPlayerProvider().get(player.getUniqueId())) == null)
            return;

        if((netPlayer.getProperty(PlayerProperty.SCOREBOARD).get(AdviAPI.getInstance().getCloudUtilities().getServerName()) == null && defaultScoreboard) ||
                netPlayer.getProperty(PlayerProperty.SCOREBOARD).get(AdviAPI.getInstance().getCloudUtilities().getServerName()).equals(this.name))
            this.manager.updateScoreboard(player, this);
    }

}

