package de.fabtopf.contray.adventuria.api.utils.arango.parser;

public class FieldReplacer<F, T> implements ArangoParser.FieldModifier<F, T> {
    private String field;
    private T toReplace;

    public FieldReplacer(String field, T toReplace) {
        this.field = field;
        this.toReplace = toReplace;
    }

    public String getField() {
        return field;
    }

    @Override
    public T getValue(F input) {
        return this.toReplace;
    }

}
